<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>Upload Sample Image</title>
		<?php include_once dirname(__DIR__).'/templates/include_css.php';?>
	</head>
	<body>
		<div id="wrapper">
			<?php include_once dirname(__DIR__).'/templates/sidebar.php'; ?>
			<div id="page-wrapper" class="gray-bg">
				<?php include_once dirname(__DIR__).'/templates/header.php'; ?>
				<div class="row wrapper border-bottom white-bg page-heading">
					<div class="col-sm-8">
						<h2>Upload Sample Image</h2>
						<ol class="breadcrumb">
							<li>
								Home
							</li>
							<li class="active">
								<strong>Upload Sample Image</strong>
							</li>
						</ol>
					</div>
				</div>
				<div class="wrapper wrapper-content animated fadeInRight">
					<div class="row">
						<div class="col-lg-12">
							<div class="ibox">
								<div class="ibox-title">
									<h5>Upload Sample Image</h5>
								</div>
								<div class="ibox-content">
									<form id="frmAdd" action="<?php echo base_url()."samples/uploadsampleimages"?>" class="form-horizontal" method="post" enctype="multipart/form-data">
										
										<div class="form-group">
											<label class="col-sm-3 control-label">Select Sample Image <span class="clsRequiredFieldLable">*</span></label>
											<div class="col-sm-9">
												<div class="control">
													<input id="image" name="image" type="file">
												</div>
											</div>
										</div>
										<div class="form-group">
											<label class="col-sm-3 control-label"></label>
											<div class="col-sm-9">
												<div class="control">
													<img id="imgMemberSelect" src="" alt="" height="100px" width="100px"/>
												</div>
											</div>
										</div>
										
										<div class="clsSubmitButtons">
											<input type="hidden" id="hdn_id" name="hdn_id" value="<?php echo $sample_data->unique_code;?>">
											<button id="btnAdd" class="btn btn-sm btn-success" name="btnAdd" type="submit">Submit</button>
											<a class="btn btn-white" href="javascript:history.back()">Cancel</a>
										</div>
										
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
		<?php include_once dirname(__DIR__).'/templates/footer.php'; ?>

		</div>
		</div>

	<?php include_once dirname(__DIR__).'/templates/include_js.php'; ?>

	<script>
		$(document).ready(function(){
			$("#image").change(function(){
				readURL(this);
			});
			
			$("#frmAdd").validate({
				rules:
				{
					image:{required: true, extension: "jpg|jpeg|png|gif|pdf|docx|doc",},
				},
				messages:
				{
					image:{required: "Please upload slip image.", extension: "Please upload only jpg, jpeg, gif, png, pdf, doc or docx files.",},
				},
				highlight: function(element) {
				   //$(element).attr('class', 'filde error-border');
				}, unhighlight: function(element) {
				   //$(element).removeClass('error-border');
				},
				onfocusout: function(element) {
					this.element(element);
				},
				submitHandler: function(form) {
					 form.submit();
				}
			});

		});

		function readURL(input) {
			if (input.files && input.files[0]) {
				var reader = new FileReader();

				reader.onload = function (e) {
					$('#imgMemberSelect').attr('src', e.target.result);
				}

				reader.readAsDataURL(input.files[0]);
			}
		}
	</script>
</body>

</html>