<!DOCTYPE html>
<html>

<head>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<title>Samples Under Act</title>

	<?php include_once dirname(__DIR__).'/templates/include_css.php';?>

</head>

<body>

	<div id="wrapper">

		<?php include_once dirname(__DIR__).'/templates/sidebar.php'; ?>

		<div id="page-wrapper" class="gray-bg">
			<?php include_once dirname(__DIR__).'/templates/header.php'; ?>
			<div class="row wrapper border-bottom white-bg page-heading">
				<div class="col-sm-4">
					<h2>Samples Under Act</h2>
					<ol class="breadcrumb">
						<li>
							Home
						</li>
						<li class="active">
							<strong>Samples Under Act</strong>
						</li>
					</ol>
				</div>
			</div>
			<div class="wrapper wrapper-content animated fadeInRight">
				<div class="row">
					<div class="col-lg-12">
						<div class="ibox float-e-margins">
							<div class="ibox-title">
								<h5>Samples Under Act</h5>
								<div class="ibox-tools">
									<a id="btnBack" name="btnBack" class="btnBackcls btn btn-primary btn-xs"><i class="fa fa-arrow-left"></i>&nbsp;Back</a>
								</div>
							</div>
							<div class="ibox-content">
								<!-- Search Panel Start  -->
								<div class="panel-body panel panel-info">
									<div class="filter">
										<div class="col-sm-6 col-lg-6">
											<div class="form-group cls-form-group">
												<label class=" control-label">Year</label>
												<select id="year" name="year" class="chosen-select">
													<?php 
															$current_year = date('Y');
															$selected = "";
																for($i = $current_year; $i >= START_YEAR ;$i--){
																	if(($i == $current_year && @$year == "")  || @$year == $i)
																		$selected = "selected='true'";
																	else	
																		$selected = "";
																	?>
													<option value="<?php echo $i;?>" <?php echo $selected;?> >
														<?php echo $i;?>
													</option>
													<?php 
																}
															?>
													<?php 
													$selected = "";
															if(@$year == "All")	$selected = "selected='true'";
															?>
													<option value="All" <?php echo $selected;?> >All</option>
												</select>
											</div>
										</div>
										<div class="col-sm-6 col-lg-6">
											<div class="form-group cls-form-group">
												<label class=" control-label">Search</label>
												<input id="search_text" name="search_text" class="form-control" maxlength="250" type="text" placeholder="Search" value="<?php echo $search; ?>">
											</div>
										</div>
									</div>
									<div class="col-sm-12 col-lg-12">
										<div class="text-center">
											<button class="btn btn-primary btn-sm" onClick="window.location='<?php echo $base_url_for_search;?>'">Clear</button>
											<button class="btn btn-sm btn-success" onClick="searchFilter();">Submit</button>
										</div>
									</div>

								</div>

								<!-- Search Panel End -->
								<table id="example" class="table table-striped table-hover dt-responsive table-bordered" cellspacing="0" width="100%">
									<thead>
										<tr>
											<th width="25px">#</th>
											<th>Sample Name</th>
											<th>Sample Code</th>
											<th>Category Name</th>
											<th>FBO Name</th>
											<th>Mode</th>
											<th>Status</th>
											<th>Created Date</th>
											<th>Send Date</th>
											<th>Action</th>
										</tr>
									</thead>

									<tbody>
										<?php 
										if(!empty($sample_data[0])){
											$start_count = $start_index + 1;
											foreach ($sample_data as $key => $_sample_data){
									?>
										<tr>
											<td>
												<?php echo $start_count?>
											</td>
											<td>
												<?php echo $this->m_util->truncate($_sample_data->name,35);?>
											</td>
											<td>
												<?php echo $_sample_data->code;?>
											</td>
											<td><?php echo $_sample_data->food_category_no . " - ".$this->m_util->truncate($_sample_data->food_category_name,35);?></td>
											<td>
												<?php echo $this->m_util->truncate($_sample_data->fbo_name,35);?>
											</td>
											<td>
												<?php 
												if(!empty($_sample_data->is_app)):
											?>
												<span class="label label-success"><i class="fa fa-mobile"></i>&nbsp;App</span>
												<?php 
												else:
											?>
												<span class="label label-primary"><i class="fa fa-desktop"></i>&nbsp;Admin</span>
												<?php 
												endif;
											?>
											</td>
											<td>
												<?php
												if($_sample_data->lab_is_prepare_form_b == 1){
													echo $this->m_util->getStatusByReportStatusNumber($_sample_data->lab_report_status);
												}else{
													echo "Pending";
												}
												?>
											</td>
											<td>
												<?php echo !empty($_sample_data->sample_created_date) ? $this->m_util->date_format($_sample_data->sample_created_date) : '-';?>
											</td>
											<td>
												<?php 
												if(!empty($_sample_data->send_date))
												{
													$array_send_date = explode(' ', $_sample_data->send_date);
													echo $this->m_util->date_format($array_send_date[0]);
												}
												else 
												{
													echo "-";
												}
											?>
											</td>
											<td>
												<?php 
												if(($_sample_data->is_app == 0 || $_sample_data->is_app == 2) && !empty($_sample_data->transport_type_id)):
											?>
												<div class="btn-group">
													<button data-toggle="dropdown" class="btn btn-primary btn-xs dropdown-toggle"><i class="fa fa-print"></i>&nbsp;Print Forms <span class="caret"></span></button>
													<ul class="dropdown-menu">
														<li><a href="<?php echo base_url()."printforms/printallforms/".$_sample_data->unique_code;?>" target="_blank">PRINT-ALL-FORMS</a>
														</li>
														<li class="divider"></li>
														<li><a href="<?php echo base_url()."printforms/printformva/".$_sample_data->unique_code;?>" target="_blank">FORM-VA</a>
														</li>
														<li class="divider"></li>
														<li><a href="<?php echo base_url()."printforms/printformlabel/".$_sample_data->unique_code;?>" target="_blank">FORM-LABEL</a>
														</li>
														<li class="divider"></li>
														<li><a href="<?php echo base_url()."printforms/printformcashmemo/".$_sample_data->unique_code;?>" target="_blank">CASH-MEMO</a>
														</li>
														<li class="divider"></li>
														<li><a href="<?php echo base_url()."printforms/printformvi/".$_sample_data->unique_code;?>" target="_blank">FORM-VI</a>
														</li>
														<li class="divider"></li>
														<li><a href="<?php echo base_url()."printforms/printformsampledeposit/".$_sample_data->unique_code;?>" target="_blank">OFFICE-COPY</a>
														</li>
														<li class="divider"></li>
														<li><a href="<?php echo base_url()."printforms/printformsealform/".$_sample_data->unique_code;?>" target="_blank">SEAL-FORM</a>
														</li>
														<!-- <li><a href="#">Another action</a></li>
					                                <li><a href="#">Something else here</a></li>
					                                <li class="divider"></li>
					                                <li><a href="#">Separated link</a></li> -->
													</ul>
												</div>
												<?php 
					                        	endif;
					                        ?>

												<a class="btn btn-success btn-xs" href="<?php echo base_url()."samples/viewsamplearchives/".$_sample_data->unique_code;?>"><i class="fa fa-eye"></i>&nbsp;View</a>
												<!-- 
					                        <?php 
												if($_sample_data->is_app != 1 && $_sample_data->edit_request_flag == 1):
											?>
					                        	<a class="btn btn-warning btn-xs" href="<?php echo base_url()."samples/editrequest/".$_sample_data->unique_code;?>"><i class="fa fa-edit"></i>&nbsp;Edit Request</a>
					                        <?php 
												endif;
											?>
											
											<?php 
												if($_sample_data->is_app != 1 && $_sample_data->edit_request_flag == 0):
											?>
					                        	<a class="btn btn-warning btn-xs" href="<?php echo base_url()."samples/editsample/".$_sample_data->unique_code;?>"><i class="fa fa-edit"></i>&nbsp;Edit</a>
					                        <?php 
												endif;
											?>
											
											<?php 
					                        	if(empty($_sample_data->transport_type_id)):
					                        ?>
					                        	<a class="btn btn-info btn-xs" href="<?php echo base_url()."samples/gettransportlabdetails/".$_sample_data->unique_code;?>"><i class="fa fa-bus"></i>&nbsp;Transport Type</a>
					                        <?php 
					                        	endif;
					                        ?>
					                        
					                        <?php 
					                        	if(empty($_sample_data->slip_no) && !empty($_sample_data->transport_type_id)):
					                        ?>
					                        	<a class="btn btn-info btn-xs" href="<?php echo base_url()."samples/getsenddetails/".$_sample_data->unique_code;?>"><i class="fa fa-paper-plane"></i>&nbsp;Send</a>
					                        <?php 
					                        	endif;
					                        ?>
					                        
											<?php 
												if($_sample_data->is_app == 1):
											?>
												<a class="btn btn-warning btn-xs" href="<?php echo base_url()."samples/edit/".$_sample_data->unique_code;?>"><i class="fa fa-edit"></i>&nbsp;Edit</a>
											<?php 
												endif;
											?>
											 -->
											</td>
										</tr>
										<?php 
									$start_count++;
										}} 
									?>
									</tbody>
								</table>
								<?php if(isset($links)):?>
								<div class="row">
									<div class="col-sm-6">Showing
										<?php echo @$start_index + 1;?> to
										<?php echo $start_count - 1;?> of
										<?php echo $total_records;?> entries</div>
									<div class="col-sm-6">
										<?php echo $links;?>
									</div>
								</div>
								<?php endif;?>
							</div>
						</div>
					</div>
				</div>

			</div>
			<?php include_once dirname(__DIR__).'/templates/footer.php'; ?>
		</div>


	</div>


	<?php include_once dirname(__DIR__).'/templates/include_js.php'; ?>

	<script>
		$( document ).ready( function () {
			$( '#example' ).dataTable( {
				"searching": false,
				"ordering": false,
				"paging": false,
				"info": false,
				"columns": [ {
						"searchable": false
					},
					null,
					null,
					null,
					null,
					null,
					null,
					null, {
						"searchable": false
					},
				],
				"aoColumnDefs": [ {
					'bSortable': false,
					'aTargets': [ 8 ]
				} ],
			} );

			/*$('#btnBack').click(function () {
		    	window.history.back();
            	window.location.href = "<?php echo base_url()."samples";?>";
            }); */
		} );

		function searchFilter() {
			var filter = "";

			if ( $.trim( $( '#search_text' ).val() ) != "" ) {
				filter += "&search=" + $.trim( $( '#search_text' ).val() );
			}
			if ( $( '#year' ).val() != "" && $( "#year" ).val() != '<?php echo $current_year; ?>' ) {
				filter += "&year=" + $( '#year' ).val();
			}
			window.location = '<?php echo $base_url_for_search;?>' + filter;
		}
	</script>
</body>

</html>