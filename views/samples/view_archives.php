<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Samples Under Act</title>

    <?php include_once dirname(__DIR__).'/templates/include_css.php';?>

</head>

<body>

    <div id="wrapper">

    <?php include_once dirname(__DIR__).'/templates/sidebar.php'; ?>

        <div id="page-wrapper" class="gray-bg">
        <?php include_once dirname(__DIR__).'/templates/header.php'; ?>
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-sm-8">
                    <h2>Samples Under Act</h2>
                    <ol class="breadcrumb">
                        <li>
                            Home
                        </li>
                        <li class="active">
                            <strong>Samples Under Act</strong>
                        </li>
                    </ol>
                </div>
            </div>
            <div class="wrapper wrapper-content">
				<div class="row">
	                <div class="col-lg-12">
	                    <div class="ibox">
	                        <div class="ibox-title">
	                            <h5>View Sample</h5>
	                            <div class="pull-right">
	                            	<?php //if($user_data->type == 1 || $user_data->type == 2):?>
	                            		<a id="btnBack" name="btnBack" class="btnBackcls btn btn-warning btn-xs"><i class="fa fa-arrow-left"></i>&nbsp;Back</a>
                                	<?php //else:?>
                                		<!-- <a id="btnBack1" name="btnBack1" class="btn btn-warning btn-xs"><i class="fa fa-arrow-left"></i>&nbsp;Back</a> -->
                                	<?php //endif;?>
	                            </div>
	                        </div>
	                        <div class="ibox-content">
	                        	<form id="frmView" action="" class="form-horizontal" method="post" enctype="multipart/form-data">                      
	                                <fieldset>
	                                    <div class="row">
	                                        <div class="col-lg-6">
	                                        	<div class="panel panel-info">
                                        			<div class="panel-heading">
                                            			FSO Details
                                        			</div>
                                        			<div class="panel-body">
                                        				<div class="form-group">
	                                                		<label class="col-sm-3 control-label">FSO Name</label>
	                                                		<div class="col-sm-9">
	                                                			<div class="clsSampleViewLabel">
	                                                				<?php echo (!empty($sample_data->created_by_full_name)) ? $sample_data->created_by_full_name : '-';?>
	                                                			</div>
	                                                		</div>
	                                            		</div>
	                                            		<div class="form-group">
	                                                		<label class="col-sm-3 control-label">Circle Name</label>
	                                                		<div class="col-sm-9">
	                                                			<div class="clsSampleViewLabel">
	                                                				<?php echo (!empty($sample_data->circle_name)) ? $sample_data->circle_name : '-';?>
                                                				</div>
	                                                		</div>
	                                            		</div>
	                                            		<div class="form-group">
	                                                		<label class="col-sm-3 control-label">Sample Collection Date</label>
	                                                		<div class="col-sm-9 clsSampleViewLabel">
	                                                			<?php echo (!empty($sample_data->sample_created_date)) ? $this->m_util->date_format($sample_data->sample_created_date) : '-';?>
	                                                		</div>
	                                            		</div>
	                                            		<div class="form-group">
	                                                		<label class="col-sm-3 control-label">Sample Collection Time</label>
	                                                		<div class="col-sm-9 clsSampleViewLabel">
	                                                			<?php echo (!empty($sample_data->sample_created_time)) ? $sample_data->sample_created_time : '-';?>
	                                                		</div>
	                                            		</div>
	                                            	</div>
                                    			</div>
                                    			<div class="panel panel-primary">
                                        			<div class="panel-heading">
                                            			FBO Details
                                        			</div>
                                        			<div class="panel-body">
                                            			<div class="form-group">
	                                                		<label class="col-sm-3 control-label">FBO Name</label>
	                                                		<div class="col-sm-9">
	                                                			<div class="clsSampleViewLabel">
	                                                				<?php echo (!empty($sample_data->fbo_name)) ? $sample_data->fbo_name : '-';?>
	                                                			</div>
	                                                		</div>
	                                            		</div>
	                                            		<div class="form-group">
	                                                		<label class="col-sm-3 control-label">Firm Name</label>
	                                                		<div class="col-sm-9">
	                                                			<div class="clsSampleViewLabel">
	                                                				<?php echo (!empty($sample_data->fbo_firm_name)) ? $sample_data->fbo_firm_name : '-';?>
	                                                			</div>
	                                                		</div>
	                                            		</div>
	                                            		<div class="form-group">
									                		<label class="col-sm-3 control-label">FBO Status</label>
									                		<div class="col-sm-9">
										                		<div class="clsSampleViewLabel">
										                			<?php echo (!empty($sample_data->fbo_status)) ? $sample_data->fbo_status : '-';?>
										                		</div>
									                		</div>
					                					</div>
					                					<div class="form-group">
									                		<label class="col-sm-3 control-label">Business Address</label>
				                                			<div class="col-sm-9">
				                                				<div class="clsSampleViewLabel">
				                                					<?php echo (!empty($sample_data->fbo_address_line1)) ? $sample_data->fbo_address_line1 : '-';?>
			                                					</div>
				                                			</div>
					                					</div>
					                					<div class="form-group">
									                		<label class="col-sm-3 control-label">Residential Address</label>
				                                			<div class="col-sm-9">
				                                				<div class="clsSampleViewLabel">
				                                					<?php echo (!empty($sample_data->fbo_address_line2)) ? $sample_data->fbo_address_line2 : '-';?>
				                                				</div>
			                                				</div>
					                					</div>
					                					<div class="form-group">
									                		<label class="col-sm-3 control-label">District</label>
									                		<div class="col-sm-9">
									                			<div class="clsSampleViewLabel">
										                			<?php echo (!empty($sample_data->fbo_district_name)) ? $sample_data->fbo_district_name : '-';?>
										                		</div>
										                	</div>
					                					</div>
					                					<div class="form-group">
									                		<label class="col-sm-3 control-label">Taluko</label>
									                		<div class="col-sm-9">
									                			<div class="clsSampleViewLabel">
										                			<?php echo (!empty($sample_data->fbo_taluka_name)) ? $sample_data->fbo_taluka_name : '-';?>
										                		</div>
										                	</div>
					                					</div>
					                					<div class="form-group">
	                                                		<label class="col-sm-3 control-label">Place</label>
	                                                		<div class="col-sm-9">
	                                                			<div class="clsSampleViewLabel">
	                                                				<?php echo (!empty($sample_data->fbo_place)) ? $sample_data->fbo_place : '-';?>
	                                                			</div>
	                                                		</div>
	                                            		</div>
	                                            		<div class="form-group">
	                                                		<label class="col-sm-3 control-label">Pincode</label>
	                                                		<div class="col-sm-9">
	                                                			<div class="clsSampleViewLabel">
	                                                				<?php echo (!empty($sample_data->fbo_pincode)) ? $sample_data->fbo_pincode : '-';?>
	                                                			</div>
	                                                		</div>
	                                            		</div>
	                                            		<div class="form-group">
									                		<label class="col-sm-3 control-label">Kind of Business</label>
									                		<div class="col-sm-9">
									                			<div class="clsSampleViewLabel">
										                			<?php echo (!empty($sample_data->fbo_business)) ? $sample_data->fbo_business : '-';?>
										                		</div>
									                		</div>
					                					</div>
					                					<div class="form-group">
									                		<label class="col-sm-3 control-label">Firm Constitution</label>
									                		<div class="col-sm-9">
									                			<div class="clsSampleViewLabel">
										                			<?php echo (!empty($sample_data->fbo_firm_type)) ? $sample_data->fbo_firm_type : '-';?>
										                		</div>
										                	</div>
					                					</div>
					                					<div class="form-group">
	                                                		<label class="col-sm-3 control-label">License Registration No.</label>
	                                                		<div class="col-sm-9">
	                                                			<div class="clsSampleViewLabel">
	                                                				<?php echo (!empty($sample_data->license_reg_no)) ? $sample_data->license_reg_no : '-';?>
	                                                			</div>
	                                                		</div>
	                                            		</div>
					                				</div>
                                    			</div>
                                    			<div class="panel panel-info">
                                        			<div class="panel-heading">
                                        				Sample Images
	                                            	</div>
                                        			<div class="panel-body">
	                                        			<div class="scroll_content">
	                                        				<div class="row">
												                <div class="col-lg-12">
												                <div class="ibox float-e-margins">
												                	<div class="lightBoxGallery">
											                        	<?php 
												                        	if(!@$sample_media[0]->Error_Message):
										                                		foreach($sample_media as $key => $_sample_media):
										                                			$arr_sample_image = explode('.', $_sample_media->image);
										                                			//print_r($arr_referral_lab_report_image);exit;
										                                			if($arr_sample_image[1] == 'jpg' || $arr_sample_image[1] == 'jpeg' || $arr_sample_image[1] == 'gif' || $arr_sample_image[1] == 'png'):
										                                ?>
															                            <span>(<?php echo $key+1;?>)</span>&nbsp;<a href="<?php echo base_url()."files/sample_images/".$_sample_media->image;?>" title="" data-gallery=""><img src="<?php echo base_url()."files/sample_images/".$_sample_media->image;?>" height="100px" width="100px"></a> <br/>
															                            
																						
																						<!-- The Gallery as lightbox dialog, should be a child element of the document body -->
															                            <div id="blueimp-gallery" class="blueimp-gallery">
															                                <div class="slides"></div>
															                                <h3 class="title"></h3>
															                                <a class="prev">&lt;</a>
															                                <a class="next">&gt;</a>
															                                <a class="close">X</a>
															                                <a class="play-pause"></a>
															                                <ol class="indicator"></ol>
															                            </div>
														                            <?php 
														                            else:
														                            ?>
														                            	<span>(<?php echo $key+1;?>)</span>&nbsp;<a href="<?php echo base_url()."files/sample_images/".$_sample_media->image;?>" target="_blank"><?php echo $_sample_media->image;?></a>	<br/>
											                            <?php
											                            			endif;
											                            		endforeach;
										                            		endif;
										                            	?>
											                        </div>
																</div>
												            	</div>
															</div>
		                                            	</div>
	                                            	</div>
                                    			</div>
                                    			<?php 
	                                    			if(!@$sample_edit_request_details[0]->Error_Message):
                                    			?>
                                    			<div class="panel panel-danger">
                                        			<div class="panel-heading">
                                            			Sample Edit Request Details
                                        			</div>
                                        			<div class="panel-body">
                                        				<div class="scroll_content">
                                        				<?php 
                                        					foreach ($sample_edit_request_details as $_sample_edit_request_details):
                                        				?>
                                        				<div class="timeline-item">
                            								<div class="row">
								                                <div class="col-xs-3 date">
								                                    <i class="fa fa-edit"></i>
								                                    <?php 
								                                    	$array_created_date = explode(' ', $_sample_edit_request_details->created_date);
								                                    	echo $this->m_util->date_format($array_created_date[0]);
								                                    ?>
								                                    <br/>
								                                    <!-- <small class="text-navy">3 hour ago</small> -->
								                                </div>
								                                <div class="col-xs-7 content">
								                                    <p class="m-b-xs"><strong>Food Safety Officer Note</strong></p>
								                                    <p><?php echo !empty($_sample_edit_request_details->fso_note) ? $_sample_edit_request_details->fso_note : '-'?></p>
								                                </div>
								                            </div>
								                        </div>
								                        <?php 
								                        	if(!empty($_sample_edit_request_details->updated_date)):
								                        ?>
								                        <div class="timeline-item">
								                            <div class="row">
								                                <div class="col-xs-3 date">
								                                    <i class="fa fa-check-square"></i>
								                                    <?php 
								                                    	$array_updated_date = explode(' ', $_sample_edit_request_details->updated_date);
								                                    	echo $this->m_util->date_format($array_updated_date[0]);
							                                    	?>
								                                    <br/>
								                                    <!-- <small class="text-navy">3 hour ago</small> -->
								                                </div>
								                                <div class="col-xs-7 content">
								                                    <p class="m-b-xs"><strong>Designated Officer Note</strong></p>
								                                    <p><p><?php echo !empty($_sample_edit_request_details->do_note) ? $_sample_edit_request_details->do_note : '-'?></p></p>
								                                </div>
								                            </div>
								                        </div>
									                    <?php 
									                    		endif;
							                        		endforeach;
							                        	?>
							                        	</div>
	                                            	</div>
                                    			</div>
                                    			<?php 
                                    				endif;
                                    				$current_date = $this->utils->get_date();
                                    				// if(!empty($sample_data->lab_dispatch_date) && !empty($sample_data->lab_view_to_do_date < $current_date)):
                                    			?>
                                    			<div class="panel panel-primary">
                                        			<div class="panel-heading">
                                            			Laboratory Report
                                        			</div>
                                        			<div class="panel-body">
                                            			<div class="form-group">
	                                                		<label class="col-sm-3 control-label">Laboratory Name</label>
	                                                		<div class="col-sm-9">
	                                                			<div class="clsSampleViewLabel">
	                                                				<?php echo (!empty($sample_data->lab_name)) ? $sample_data->lab_name : '-';?>
	                                                			</div>
	                                                		</div>
	                                            		</div>
	                                            		<div class="form-group">
									                		<label class="col-sm-3 control-label">Report Status</label>
									                		<div class="col-sm-9">
										                		<div class="clsSampleViewLabel">
										                			<?php echo (!empty($lab_report_status_data)) ? $lab_report_status_data : '-';?>
										                		</div>
									                		</div>
					                					</div>
					                				</div>
                                    			</div>
                                    			<?php 
                                    				// endif;
                                    				if(!empty($sample_data->is_accredited_lab_report_prepare_by_do)):
                                    			?>
                                    			<div class="panel panel-primary">
                                        			<div class="panel-heading">
                                            			Accredited Laboratory Report
                                        			</div>
                                        			<div class="panel-body">
                                            			<div class="form-group">
	                                                		<label class="col-sm-3 control-label">Laboratory Name</label>
	                                                		<div class="col-sm-9">
	                                                			<div class="clsSampleViewLabel">
	                                                				<?php echo (!empty($sample_data->accredited_lab_name)) ? $sample_data->accredited_lab_name : '-';?>
	                                                			</div>
	                                                		</div>
	                                            		</div>
	                                            		<div class="form-group">
									                		<label class="col-sm-3 control-label">Report Date</label>
									                		<div class="col-sm-9">
										                		<div class="clsSampleViewLabel">
										                			<?php echo (!empty($sample_data->accredited_lab_report_date)) ? $this->m_util->date_format($sample_data->accredited_lab_report_date) : '-';?>
										                		</div>
									                		</div>
					                					</div>
					                					<div class="form-group">
									                		<label class="col-sm-3 control-label">Report Status</label>
									                		<div class="col-sm-9">
										                		<div class="clsSampleViewLabel">
										                			<?php echo (!empty($accredited_lab_report_status_data)) ? $accredited_lab_report_status_data : '-';?>
										                		</div>
									                		</div>
					                					</div>
					                					<?php 
					                						$arr_accredited_lab_report_image = explode('.', $sample_data->accredited_lab_report_image);
					                						//print_r($arr_referral_lab_report_image);exit;
					                						if($arr_accredited_lab_report_image[1] == 'jpg' || $arr_accredited_lab_report_image[1] == 'jpeg' || $arr_accredited_lab_report_image[1] == 'gif' || $arr_accredited_lab_report_image[1] == 'png'):
					                					?>
						                					<div class="form-group">
		                                                		<label class="col-sm-3 control-label">Lab Report</label>
		                                                		<div class="col-sm-9 clsSampleViewLabel">
		                                                			<div class="ibox float-e-margins">
													                	<div class="lightBoxGallery">
												                        	<a href="<?php echo base_url()."files/accredited_lab_report_images/".$sample_data->accredited_lab_report_image;?>" title="" data-gallery=""><img src="<?php echo base_url()."files/accredited_lab_report_images/".$sample_data->accredited_lab_report_image;?>" height="100px" width="100px"></a>
												                            <!-- The Gallery as lightbox dialog, should be a child element of the document body -->
												                            <div id="blueimp-gallery" class="blueimp-gallery">
												                                <div class="slides"></div>
												                                <h3 class="title"></h3>
												                                <a class="prev">&lt;</a>
												                                <a class="next">&gt;</a>
												                                <a class="close">X</a>
												                                <a class="play-pause"></a>
												                                <ol class="indicator"></ol>
												                            </div>
																		</div>
		                                                			</div>
		                                            			</div>
		                                            		</div>
	                                            		<?php 
	                                            			else:
	                                            		?>
	                                            			<div class="form-group">
										                		<label class="col-sm-3 control-label">Lab Report</label>
										                		<div class="col-sm-9">
											                		<div class="clsSampleViewLabel">
											                			<a href="<?php echo base_url()."files/accredited_lab_report_images/".$sample_data->accredited_lab_report_image;?>" target="_blank"><?php echo $sample_data->accredited_lab_report_image;?></a>
											                		</div>
										                		</div>
						                					</div>
	                                            		<?php 
	                                            			endif;
	                                            		?>
					                				</div>
                                    			</div>
                                    			<?php 
                                    				endif;
                                    			?>
                                    		</div>
	                                    	<div class="col-lg-6">
	                                        	<div class="panel panel-success">
                                        			<div class="panel-heading">
                                            			Sample Details
                                        			</div>
                                        			<div class="panel-body">
                                        				<div class="form-group">
	                                                		<label class="col-sm-3 control-label">Laboratory Name</label>
	                                                		<div class="col-sm-9">
	                                                			<div class="clsSampleViewLabel">
	                                                				<?php
				                                        				echo !empty($sample_data->lab_name) ? $sample_data->lab_name : '-';
				                                        			?>
	                                                			</div>
	                                                		</div>
	                                            		</div>
                                            			<div class="form-group">
	                                                		<label class="col-sm-3 control-label">Sample Name</label>
	                                                		<div class="col-sm-9">
	                                                			<div class="clsSampleViewLabel">
	                                                				<?php echo (!empty($sample_data->name)) ? $sample_data->name : '-';?>
	                                                			</div>
	                                                		</div>
	                                            		</div>
	                                            		<div class="form-group">
	                                                		<label class="col-sm-3 control-label">Place of Collection</label>
	                                                		<div class="col-sm-9">
	                                                			<div class="clsSampleViewLabel">
	                                                				<?php echo (!empty($sample_data->short_name)) ? $sample_data->short_name : '-';?>
	                                                			</div>
	                                                		</div>
	                                            		</div>
	                                            		<div class="form-group">
	                                                		<label class="col-sm-3 control-label">Sample Code of DO Slip</label>
	                                                		<div class="col-sm-9">
	                                                			<div class="clsSampleViewLabel">
	                                                				<?php echo (!empty($sample_data->code)) ? $sample_data->code : '-';?>
	                                                			</div>
	                                                		</div>
	                                            		</div>
	                                            		<div class="form-group">
									                		<label class="col-sm-3 control-label">Food Category</label>
									                		<div class="col-sm-9">
									                			<div class="clsSampleViewLabel">
										                			<?php echo (!empty($sample_data->food_category_name)) ? $sample_data->food_category_no . '-'. $sample_data->food_category_name : '-';?>
										                		</div>
										                	</div>
					                					</div>
					                					<div class="form-group">
									                		<label class="col-sm-3 control-label">Manufacturer Address</label>
				                                			<div class="col-sm-9">
				                                				<div class="clsSampleViewLabel">
				                                					<?php echo (!empty($sample_data->manufacturer_address)) ? $sample_data->manufacturer_address : '-';?>
				                                				</div>
				                                			</div>
					                					</div>
					                					<div class="form-group">
									                		<label class="col-sm-3 control-label">Type of Sample</label>
									                		<div class="col-sm-9">
									                			<div class="clsSampleViewLabel">
										                			<?php echo (!empty($sample_data->pack_type)) ? $sample_data->pack_type : '-';?>
										                		</div>
										                	</div>
					                					</div>
					                					<?php 
					                						if($sample_data->pack_id == "1" || $sample_data->pack_id == "2"):
					                					?>
					                					<div class="form-group">
	                                                		<label class="col-sm-3 control-label">Lot/Code/Batch No.</label>
	                                                		<div class="col-sm-9 clsSampleViewLabel">
	                                                			<?php echo (!empty($sample_data->batch_no)) ? $sample_data->batch_no : '-';?>
	                                                		</div>
	                                            		</div>
					                					<div class="form-group">
	                                                		<label class="col-sm-3 control-label">Date of Manufacture or Packing</label>
	                                                		<div class="col-sm-9 clsSampleViewLabel">
	                                                			<?php echo (!empty($sample_data->packing_date)) ? $sample_data->packing_date : '-';?>
	                                                		</div>
	                                            		</div>
	                                            		<div class="form-group">
	                                                		<label class="col-sm-3 control-label">Best Before/Use By Date</label>
	                                                		<div class="col-sm-9 clsSampleViewLabel">
	                                                			<?php echo (!empty($sample_data->used_by_date)) ? $sample_data->used_by_date : '-';?>
	                                                		</div>
	                                            		</div>
	                                            		<?php 
	                                            			endif;
	                                            		?>
					                					<div class="form-group">
	                                                		<label class="col-sm-3 control-label">Quantity</label>
	                                                		<div class="col-sm-9">
	                                                			<div class="clsSampleViewLabel">
	                                                				<?php echo (!empty($sample_data->quantity)) ? $sample_data->quantity : '-';?><span>&nbsp;</span><?php echo (!empty($sample_data->quantity_type)) ? $sample_data->quantity_type : '';?>
	                                                			</div>
	                                                		</div>
	                                                	</div>
	                                                	<div class="form-group">
	                                                		<label class="col-sm-3 control-label">Price</label>
	                                                		<div class="col-sm-9">
	                                                			<div class="clsSampleViewLabel">
	                                                				<?php echo (!empty($sample_data->price)) ? $sample_data->price : '-';?><span>&nbsp;Per&nbsp;</span><?php echo (!empty($sample_data->price_type)) ? $sample_data->price_type : '';?>
	                                                			</div>
	                                                		</div>
	                                                	</div>
	                                                	<div class="form-group">
	                                                		<label class="col-sm-3 control-label">Payment made to FBO</label>
	                                                		<div class="col-sm-9">
	                                                			<div class="clsSampleViewLabel">
	                                                				<?php echo (!empty($sample_data->payment_to_fbo)) ? $sample_data->payment_to_fbo : '-';?>
	                                                			</div>
	                                                		</div>
	                                                	</div>
	                                                	<div class="form-group">
	                                                		<label class="col-sm-3 control-label">Cash Memo No.</label>
	                                                		<div class="col-sm-9">
	                                                			<div class="clsSampleViewLabel">
	                                                				<?php echo (!empty($sample_data->receipt_no)) ? $sample_data->receipt_no : '-';?>
	                                                			</div>
	                                                		</div>
	                                                	</div>
	                                                	<div class="form-group">
									                		<label class="col-sm-3 control-label">Kind of Sample</label>
									                		<div class="col-sm-9">
									                			<div class="clsSampleViewLabel">
										                			<?php echo (!empty($sample_data->fbo_business)) ? $sample_data->fbo_business : '-';?>
										                		</div>
										                	</div>
					                					</div>
					                					<div class="form-group">
									                		<label class="col-sm-3 control-label">Preservative Added</label>
				                                			<div class="col-sm-9">
				                                				<div class="clsSampleViewLabel">
				                                					<?php echo (!empty($sample_data->preservation_added)) ? $sample_data->preservation_added : '-';?>
				                                				</div>
				                                			</div>
					                					</div>
					                					<div class="form-group">
									                		<label class="col-sm-3 control-label">Disclosure of purchase of food by FBO</label>
				                                			<div class="col-sm-9">
				                                				<div class="clsSampleViewLabel">
				                                					<?php echo (!empty($sample_data->disclosure)) ? $sample_data->disclosure : '-';?>
				                                				</div>
				                                			</div>
					                					</div>
					                					<!-- <div class="form-group">
				                           					<label class="col-sm-3 control-label">Whether FBO demand for referral lab</label>
			                                        		<div class="col-sm-9">
			                                        			<div class="clsSampleViewLabel">
				                                        			<?php //echo (!empty($sample_data->is_demand_referral_lab)) ? 'Yes' : 'No';?>
				                                        		</div>
			                                        		</div>
														</div>
														<?php //if(!empty($sample_data->is_demand_referral_lab)):?>
														<div class="form-group">
				                           					<label class="col-sm-3 control-label">Referral Laboratory</label>
			                                        		<div class="col-sm-9">
			                                        			<div class="clsSampleViewLabel">
				                                        			<?php
				                                        				//$referral_lab_name = $this->m_util->getReferralLaboratoryName($sample_data->referral_lab_id);
				                                        				//echo !empty($referral_lab_name) ? $referral_lab_name : '-';
				                                        			?>
				                                        		</div>
			                                        		</div>
														</div> -->
														<?php //endif;?>
														<div class="form-group">
				                           					<label class="col-sm-3 control-label">Whether FBO demand for accredited lab</label>
			                                        		<div class="col-sm-9">
			                                        			<div class="clsSampleViewLabel">
				                                        			<?php echo (!empty($sample_data->is_demand_accredited_lab)) ? 'Yes' : 'No';?>
				                                        		</div>
			                                        		</div>
														</div>
														<?php if(!empty($sample_data->is_demand_accredited_lab)):?>
														<div class="form-group">
				                           					<label class="col-sm-3 control-label">Accredited Laboratory</label>
			                                        		<div class="col-sm-9">
			                                        			<div class="clsSampleViewLabel">
				                                        			<?php
				                                        				echo !empty($sample_data->accredited_lab_name) ? $sample_data->accredited_lab_name : '-';
				                                        			?>
				                                        		</div>
			                                        		</div>
														</div>
														<?php endif;?>
														<div class="form-group">
									                		<label class="col-sm-3 control-label">Witness Name</label>
									                		<div class="col-sm-9">
									                			<div class="clsSampleViewLabel">
									                				<?php echo (!empty($sample_data->witness_name)) ? $sample_data->witness_name : '-';?>
									                			</div>
									                		</div>
				                                		</div>
					                					<div class="form-group">
									                		<label class="col-sm-3 control-label">Witness Address</label>
				                                			<div class="col-sm-9">
				                                				<div class="clsSampleViewLabel">
				                                					<?php echo (!empty($sample_data->witness_address)) ? $sample_data->witness_address : '-';?>
				                                				</div>
				                                			</div>
					                					</div>
					                					<div class="form-group">
				                           					<label class="col-sm-3 control-label">Food Article Seized</label>
			                                        		<div class="col-sm-9">
			                                        			<div class="clsSampleViewLabel">
				                                        			<?php echo (!empty($sample_data->is_seizzer)) ? 'Yes' : 'No';?>
				                                        		</div>
			                                        		</div>
														</div>
														<div id="divQuantity" class="form-group">
	                                                		<label class="col-sm-3 control-label">Quantity</label>
	                                                		<div class="col-sm-9">
	                                                			<div class="clsSampleViewLabel">
		                                                			<?php
		                                                			if(!empty($sample_data->is_seizzer)):
		                                                				if(!empty($sample_data->seizzer_quantity)):
		                                                					if(!empty($sample_data->seizzer_quantity_type)):
		                                                						echo $sample_data->seizzer_quantity.' '.$sample_data->seizzer_quantity_type;
		                                                					else:
		                                                						echo $sample_data->seizzer_quantity;
		                                                					endif;
		                                                				elseif(!empty($sample_data->seizzer_quantity_ml_ltr)):
		                                                					if(!empty($sample_data->seizzer_quantity_type_ml_ltr)):
		                                                						echo $sample_data->seizzer_quantity_ml_ltr.' '.$sample_data->seizzer_quantity_type_ml_ltr;
		                                                					else:
		                                                						echo $sample_data->seizzer_quantity_ml_ltr;
		                                                					endif;
		                                                				else:
		                                                					echo '0';
		                                                				endif;
		                                                			else:
		                                                				echo '0';
		                                                			endif;
	                                                				?>
                                                				</div>
	                                                		</div>
	                                                	</div>
	                                                	<div id="divPrice" class="form-group">
	                                                		<label class="col-sm-3 control-label">Price</label>
	                                                		<div class="col-sm-9">
	                                                			<div class="clsSampleViewLabel">
		                                                			<?php
		                                                				if(!empty($sample_data->is_seizzer)):
		                                                					echo (!empty($sample_data->seizzer_price)) ? $sample_data->seizzer_price : '0';
	                                                					else:
	                                                						echo '0';
	                                                					endif;
	                                                				?>
                                                				</div>
	                                                		</div>
	                                                	</div>
                                        			</div>
                                    			</div> 
                                    			<?php 
                                    				if(!empty($sample_data->send_date)):
                                    			?>
                                    			<div class="panel panel-warning">
                                        			<div class="panel-heading">
                                            			Transportation Details
                                        			</div>
                                        			<div class="panel-body">
                                        				<div class="form-group">
	                                                		<label class="col-sm-3 control-label">Transportation Type</label>
	                                                		<div class="col-sm-9">
	                                                			<div class="clsSampleViewLabel">
	                                                				<?php echo (!empty($sample_data->transport_type)) ? $sample_data->transport_type : '-';?>
	                                                			</div>
	                                                		</div>
	                                            		</div>
	                                            		<div class="form-group">
	                                                		<label class="col-sm-3 control-label">Slip Number</label>
	                                                		<div class="col-sm-9">
	                                                			<div class="clsSampleViewLabel">
	                                                				<?php echo (!empty($sample_data->slip_no)) ? $sample_data->slip_no : '-';?>
                                                				</div>
	                                                		</div>
	                                            		</div>
	                                            		<div class="form-group">
	                                                		<label class="col-sm-3 control-label">Slip Date</label>
	                                                		<div class="col-sm-9 clsSampleViewLabel">
	                                                			<?php echo (!empty($sample_data->slip_date)) ? $this->m_util->date_format($sample_data->slip_date) : '-';?>
	                                                		</div>
	                                            		</div>
	                                            		<div class="form-group">
	                                                		<label class="col-sm-3 control-label">Sample Dispatch Date</label>
	                                                		<div class="col-sm-9 clsSampleViewLabel">
	                                                			<?php 
		                                                			if(!empty($sample_data->send_date))
		                                                			{
		                                                				$array_send_date = explode(' ', $sample_data->send_date);
		                                                				echo $this->m_util->date_format($array_send_date[0]);
		                                                			}
		                                                			else
		                                                			{
		                                                				echo "-";
		                                                			}
	                                                			?>
	                                                		</div>
	                                            		</div>
	                                            		<div class="form-group">
	                                                		<label class="col-sm-3 control-label">Slip Image</label>
	                                                		<div class="col-sm-9 clsSampleViewLabel">
	                                                			<div class="ibox float-e-margins">
												                	<div class="lightBoxGallery">
											                        	<a href="<?php echo base_url()."files/transport_slips/".$sample_data->slip_image;?>" title="" data-gallery=""><img src="<?php echo base_url()."files/transport_slips/thumb/".$sample_data->slip_image;?>" height="100px" width="100px"></a>
											                            <!-- The Gallery as lightbox dialog, should be a child element of the document body -->
											                            <div id="blueimp-gallery" class="blueimp-gallery">
											                                <div class="slides"></div>
											                                <h3 class="title"></h3>
											                                <a class="prev">&lt;</a>
											                                <a class="next">&gt;</a>
											                                <a class="close">X</a>
											                                <a class="play-pause"></a>
											                                <ol class="indicator"></ol>
											                            </div>
											                        </div>
																</div>
	                                                		</div>
	                                            		</div>
	                                            	</div>
                                    			</div>
                                    			<?php 
                                    				endif;
                                    				if(!empty($sample_data->is_referral_lab_report_prepare_by_do)):
                                    			?> 
	                                    		<div class="panel panel-primary">
													<div class="panel-heading">
														Referral Laboratory Report
													</div>
													<div class="panel-body">
														<div class="form-group">
															<label class="col-sm-3 control-label">Laboratory Name</label>
															<div class="col-sm-9">
																<div class="clsSampleViewLabel">
																	<?php echo (!empty($sample_data->referral_lab_name)) ? $$sample_data->referral_lab_name : '-';?>
																</div>
															</div>
														</div>
														<div class="form-group">
															<label class="col-sm-3 control-label">Report Date</label>
															<div class="col-sm-9">
																<div class="clsSampleViewLabel">
																	<?php echo (!empty($sample_data->referral_lab_report_date)) ? $this->m_util->date_format($sample_data->referral_lab_report_date) : '-';?>
																</div>
															</div>
														</div>
														<div class="form-group">
															<label class="col-sm-3 control-label">Report Status</label>
															<div class="col-sm-9">
																<div class="clsSampleViewLabel">
																	<?php echo (!empty($referral_lab_report_status_data)) ? $referral_lab_report_status_data : '-';?>
																</div>
															</div>
														</div>
														<?php 
															$arr_referral_lab_report_image = explode('.', $sample_data->referral_lab_report_image);
															//print_r($arr_referral_lab_report_image);exit;
															if($arr_referral_lab_report_image[1] == 'jpg' || $arr_referral_lab_report_image[1] == 'jpeg' || $arr_referral_lab_report_image[1] == 'gif' || $arr_referral_lab_report_image[1] == 'png'):
														?>
															<div class="form-group">
																<label class="col-sm-3 control-label">Lab Report</label>
																<div class="col-sm-9 clsSampleViewLabel">
																	<div class="ibox float-e-margins">
																		<div class="lightBoxGallery">
																			<a href="<?php echo base_url()."files/referral_lab_report_images/".$sample_data->referral_lab_report_image;?>" title="" data-gallery=""><img src="<?php echo base_url()."files/referral_lab_report_images/".$sample_data->referral_lab_report_image;?>" height="100px" width="100px"></a>
																			<!-- The Gallery as lightbox dialog, should be a child element of the document body -->
																			<div id="blueimp-gallery" class="blueimp-gallery">
																				<div class="slides"></div>
																				<h3 class="title"></h3>
																				<a class="prev">&lt;</a>
																				<a class="next">&gt;</a>
																				<a class="close">X</a>
																				<a class="play-pause"></a>
																				<ol class="indicator"></ol>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														<?php 
															else:
														?>
															<div class="form-group">
																<label class="col-sm-3 control-label">Lab Report</label>
																<div class="col-sm-9">
																	<div class="clsSampleViewLabel">
																		<a href="<?php echo base_url()."files/referral_lab_report_images/".$sample_data->referral_lab_report_image;?>" target="_blank"><?php echo $sample_data->referral_lab_report_image;?></a>
																	</div>
																</div>
															</div>
														<?php 
															endif;
														?>
													</div>
												</div>
												<?php 
													endif;
												?> 
	                                    	</div>
	                                    </div>
	                                </fieldset>
								</form>
	                        </div>
	                    </div>
					</div>
	        	</div>
			</div>
        <?php include_once dirname(__DIR__).'/templates/footer.php'; ?>

        </div>
        </div>

    <?php include_once dirname(__DIR__).'/templates/include_js.php'; ?>

    <script>
    	$(document).ready(function(){
    		$('.scroll_content').slimscroll({
	            height: '200px'
	        });
    		
    		/*$('#btnBack').click(function () {
            	window.location.href = "<?php echo base_url()."samples/archives";?>";
            });

    		$('#btnBack1').click(function () {
            	window.location.href = "<?php echo base_url()."sampleslisting";?>";
            });*/
    	});
    </script>
</body>

</html>