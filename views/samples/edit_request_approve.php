<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Samples Under Act Edit Request Approval</title>

    <?php include_once dirname(__DIR__).'/templates/include_css.php';?>

</head>

<body>

    <div id="wrapper">

    <?php include_once dirname(__DIR__).'/templates/sidebar.php'; ?>

        <div id="page-wrapper" class="gray-bg">
        <?php include_once dirname(__DIR__).'/templates/header.php'; ?>
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-sm-8">
                    <h2>Samples Under Act Edit Request Approval</h2>
                    <ol class="breadcrumb">
                        <li>
                            Home
                        </li>
                        <li class="active">
                            <strong>Samples Under Act Edit Request Approval</strong>
                        </li>
                    </ol>
                </div>
            </div>
        	<div class="wrapper wrapper-content animated fadeInRight">
				<div class="row">
	                <div class="col-lg-12">
	                    <div class="ibox">
	                        <div class="ibox-title">
	                            <h5>Samples Under Act Edit Request Approval</h5>
	                        </div>
	                        <div class="ibox-content">
	                        	<!-- <div class="alert alert-danger fade in error" id="error"> </div> -->
	                        	<?php 
				            		if(!empty($this->session->flashdata('mendatory_sample_edit_request_approval_data'))):
				            	?>
				            	<div class="alert alert-danger fade in error" id="error"> 
				            		<?php 
				            			echo $this->session->flashdata('mendatory_sample_edit_request_approval_data');
				            		?>
				            	</div>
				            	<?php 
				            		endif;
				            	?>
	                            <form id="frmAdd" action="<?php echo base_url()."requestapporvals/editsamplerequestapprove";?>" class="form-horizontal" method="post" enctype="multipart/form-data">                      
	                                <fieldset>
	                                    <div class="row">
	                                        <div class="col-lg-6">
	                                        	<div class="form-group">
													<label class="col-sm-3 control-label">Sample Code </label>
													<div class="col-sm-9">
														<div class="control">
															<input id="samplecode" name="samplecode" type="text" class="form-control" value="<?php echo (!empty($sample_data->code)) ? $sample_data->code : '';?>" readonly>
														</div>
													</div>
												</div>
												<div class="form-group">
													<label class="col-sm-3 control-label">Sample Name </label>
													<div class="col-sm-9">
														<div class="control">
															<input id="samplename" name="samplename" type="text" class="form-control" value="<?php echo (!empty($sample_data->name)) ? $sample_data->name : '';?>" readonly>
														</div>
													</div>
												</div>
												<div class="form-group">
													<label class="col-sm-3 control-label">FSO Edit Request Note </label>
													<div class="col-sm-9">
														<div class="control">
															<textarea id="edit_request_note" name="edit_request_note" class="form-control" rows="3" readonly><?php echo (!empty($sample_edit_request_detail->fso_note)) ? $sample_edit_request_detail->fso_note : '';?></textarea>
														</div>
													</div>
												</div>
												<div class="form-group">
													<label class="col-sm-3 control-label">Edit Request Approval Note <span class="clsRequiredFieldLable">*</span></label>
													<div class="col-sm-9">
														<div class="control">
															<textarea id="note" name="note" class="form-control" rows="3" maxlength="450"></textarea>
														</div>
													</div>
												</div>
	                                        </div>
                                    	</div>
	                                    <div class="clsSubmitButtons">
	                                    	<input type="hidden" id="hdn_id" name="hdn_id" value="<?php echo $sample_data->id;?>">
	                                    	<input type="hidden" id="hdn_fso_id" name="hdn_fso_id" value="<?php echo $sample_data->created_by;?>">
	                                    	<button id="btnAdd" class="btn btn-sm btn-success" name="btnAdd" type="submit">Approve</button>
											<a class="btn btn-white" href="javascript:history.back()">Cancel</a>
										</div>
	                                </fieldset>
								</form>
	                        </div>
	                    </div>
					</div>
	        	</div>
			</div>
        <?php include_once dirname(__DIR__).'/templates/footer.php'; ?>

        </div>
        </div>

    <?php include_once dirname(__DIR__).'/templates/include_js.php'; ?>

    <script>
    	$(document).ready(function(){
    		$.validator.addMethod("lettersonly", function (value, element) {
    			return this.optional(element) || /^[a-z]+$/i.test(value);
        	}, "Please enter only letters.");

    		$.validator.addMethod("lettersandspaceonly", function(value, element) {
    			return this.optional(element) || /^[a-z\s]+$/i.test(value);
    		}, "Accept only Letters and Space");
			
    		$.validator.addMethod("lettersnumbersonly", function(value, element) {
    	        return this.optional(element) || /^[a-z0-9]+$/i.test(value);
    	    }, "Accept only letters and numbers.");

    		$("#frmAdd").validate({
                rules:
                {
                	note:{required: true, maxlength: 450,},
                },
                messages:
                {
                	note:{required: "Please enter edit request approval note.",},
                },
                highlight: function(element) {
     	           //$(element).attr('class', 'filde error-border');
     	        }, unhighlight: function(element) {
     	           //$(element).removeClass('error-border');
     	        },
     			onfocusout: function(element) {
     				this.element(element);
     			},
     			submitHandler: function(form) {
     				var is_confirm =confirm('Please check all detail are correctly entered in this form. Details can not be edit after submit. Are you sure want to submit?');
					if(is_confirm)
					{
                    	form.submit();
					}
					else
					{
						return false;
					}
                }
	        });

    	});
    </script>
</body>

</html>