<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Samples Under Act</title>

    <?php include_once dirname(__DIR__).'/templates/include_css.php';?>

</head>

<body>

    <div id="wrapper">

    <?php include_once dirname(__DIR__).'/templates/sidebar.php'; ?>

        <div id="page-wrapper" class="gray-bg">
        <?php include_once dirname(__DIR__).'/templates/header.php'; ?>
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-sm-8">
                    <h2>Samples Under Act</h2>
                    <ol class="breadcrumb">
                        <li>
                            Home
                        </li>
                        <li class="active">
                            <strong>Samples Under Act</strong>
                        </li>
                    </ol>
                </div>
            </div>
        	<div class="wrapper wrapper-content animated fadeInRight">
        		<div class="row">
					<div class="col-lg-12">
						<div class="ibox float-e-margins">
							<div class="ibox-title">
								<h5>Samples Under Act</h5>
								<!-- <div class="ibox-tools">
									<a class="btn btn-primary btn-xs" href="<?php echo base_url();?>samples/add"><i class="fa fa-plus-square"></i>&nbsp;Add Sample</a>
								</div> -->
							</div>
							<div class="ibox-content">
								<div id="circlewise" class="panel panel-info">
									<div class="panel-body">
										<form id="frmAdd" name="frmAdd" class="form-horizontal" method="post" action="<?php echo base_url().'sampleslisting/circlewisefsosampledetails'; ?>">
											<fieldset>
											<?php if(!empty($user_data) && $user_data->type != 3):?>
												<div class="col-lg-4">
													<div class="form-group cls-form-group">
								                		<label class="col-sm-3 control-label">Circle <span class="clsRequiredFieldLable">*</span></label>
								                		<div class="col-sm-9">
								                			<select id="circles" name="circles" class="form-control">
								                				<option value="" disabled="disabled" selected="selected">Select Circle</option>
								                				<?php 
								                					if(!empty($circle_data)):
								                						foreach ($circle_data as $_circle_data):
								                							if(isset($selected_circle_id) && $selected_circle_id == $_circle_data->id):
							                					?>
							                								
																				<option value="<?php echo $_circle_data->id; ?>" selected="selected"><?php echo ucfirst($_circle_data->district_name.' - '.$_circle_data->name); ?></option>
																			<?php
																			else:
																			?>
																				<option value="<?php echo $_circle_data->id; ?>"><?php echo ucfirst($_circle_data->district_name.' - '.$_circle_data->name); ?></option>
																			<?php
																			endif;
							                							endforeach;
						                							endif;
							                					?>
								                			</select>
								                		</div>
								            		</div>
												</div>
											<?php endif;?>	
												<div class="col-lg-4">
													<div class="form-group cls-form-group">
								                		<label class="col-sm-3 control-label">FSO Name</label>
								                		<div class="col-sm-9">
								                			<select id="fsoname" name="fsoname" class="form-control">
								                				<option value="all">All FSO</option>
								                				<?php 
								                					if(!empty($fso_data)):
								                						foreach ($fso_data as $_fso_data):
								                							if(isset($selected_fso_id) && $selected_fso_id == $_fso_data->id):
							                					?>
							                									<option value="<?php echo $_fso_data->id; ?>" selected="selected"><?php echo ucfirst($_fso_data->full_name); ?></option>
							                								<?php
																			else:
																			?>
																				<option value="<?php echo $_fso_data->id; ?>"><?php echo ucfirst($_fso_data->full_name); ?></option>
							                					<?php 
							                								endif;
							                							endforeach;
						                							endif;
							                					?>
								                			</select>
								                		</div>
								            		</div>
												</div>
												
												<div class="col-lg-4">
													<div align="center">
														<button id="btnAdd1" class="btn btn-sm btn-success" name="btnAdd1" type="submit">Submit</button>
													</div>
												</div>
											</fieldset>
										</form>
							    	</div>
								</div>
								<table id="example" class="table table-striped table-hover dt-responsive table-bordered" cellspacing="0" width="100%">
									<thead>
										<tr>
											<th width="25px">#</th>
											<th>Sample Name</th>
											<th>Sample Code</th>
											<th>Category Name</th>
											<th>FSO Name</th>
											<th>FBO Name</th>
											<th>Mode</th>
											<th>Lab Report</th>
											<th>Created Date</th>
											<th>Send Date</th>
											<th>Action</th>
										</tr>
									</thead>

									<tbody>
									<?php 
										if(!empty($sample_data)){foreach ($sample_data as $key => $_sample_data){
									?>
									<tr>
										<td><?php echo $key+1?></td>
										<td><?php echo $this->m_util->truncate($_sample_data->name,35);?></td>
										<td><?php echo $_sample_data->code;?></td>
										<td>
											<?php 
												$food_category_name = $this->m_util->getFoodCategoryName($_sample_data->category_id);	
												
												if(!empty($food_category_name))
												{
													echo $this->m_util->truncate($food_category_name,35);
												}
												else 
												{
													echo "-";
												}
											?>
										</td>
										<td>
											<?php 
												$fso_name = $this->m_util->getFsoName($_sample_data->created_by);
												
												if(!empty($fso_name))
												{
													echo $this->m_util->truncate($fso_name,35);
												}
												else 
												{
													echo "-";
												}
											?>
										</td>
										<td>
											<?php 
												$fbo_name = $this->m_util->getFboName($_sample_data->id);
												
												if(!empty($fbo_name))
												{
													echo $this->m_util->truncate($fbo_name,35);
												}
												else 
												{
													echo "-";
												}
											?>
										</td>
										<td>
											
											<?php if($_sample_data->is_offline == 0){?>
													<span class="label label-primary"><i class="fa fa-desktop"></i>&nbsp;Admin</span>
											<?php }else{ ?>
											<span class="label label-primary"><i class="fa fa-desktop"></i>&nbsp;BackData</span>
											<?php } ?>
										</td>
										<td>
										    <?php if(!empty($_sample_data->lab_dispatch_date)){?>
											<?php 
												if($_sample_data->lab_report_status != null && $_sample_data->lab_report_status == 0):
											?>
													<span class="label label-primary"><?php echo "Confirm";?></span>
											<?php 
												elseif($_sample_data->lab_report_status != null && $_sample_data->lab_report_status == 1):
											?>
													<span class="label label-danger"><?php echo "Unsafe";?></span>
											<?php 
												elseif($_sample_data->lab_report_status != null && $_sample_data->lab_report_status == 2):
											?>
													<span class="label label-warning"><?php echo "Misbranded";?></span>
											<?php 
												else:
											?>
													<span class="label label-warning"><?php echo "SubStandard";?></span>
											<?php 
												endif;
											?>
										<?php	}else{?>
										    <span class="label label-default">Pendding</span>
									<?php	}
											?>
										</td>
										<td><?php echo !empty($_sample_data->sample_created_date) ? $this->m_util->date_format($_sample_data->sample_created_date) : '-';?></td>
										<td>
											<?php 
												if(!empty($_sample_data->send_date))
												{
													$array_send_date = explode(' ', $_sample_data->send_date);
													echo $this->m_util->date_format($array_send_date[0]);
												}
												else 
												{
													echo "-";
												}
											?>
										</td>
										<td>
										    <?php if($_sample_data->is_offline == 0 && !empty($_sample_data->lab_prepare_form_b_date)):?>
												<a class="btn btn-primary btn-xs" href="<?php echo base_url()."printforms/printformb/".$_sample_data->id;?>" target="_blank"><i class="fa fa-print"></i>&nbsp;Print Form B</a>
											<?php endif;?>
											<a class="btn btn-success btn-xs" href="<?php echo base_url()."sampleslisting/viewsample/".$_sample_data->id;?>"><i class="fa fa-eye"></i>&nbsp;View</a>
											
					                    </td>
									</tr>
									<?php 
										}} 
									?>
		                        </tbody>
								</table>
							</div>
						</div>
					</div>

				</div>
			</div>
        <?php include_once dirname(__DIR__).'/templates/footer.php'; ?>

        </div>
        </div>

    <?php include_once dirname(__DIR__).'/templates/include_js.php'; ?>

    <script>
	    $(document).ready(function()
	    {
	    	var fso_count="<?php echo $fso_count; ?>";
	    	if(fso_count==0)
	    	{
				var selectList = $("#fsoname");
				selectList.find("option:gt(0)").remove();
				selectList.attr("disabled","disabled");
			}
	    	else
	    	{
				var selectList = $("#fsoname");
				selectList.removeAttr("disabled","disabled");
			}

			$('select[name="circles"]').change(function(){
        		var id = $(this).val();
        		fnGetFsoByCircle(id);
        	});
			
		    $('#example').dataTable({
				"columns": [
					{ "searchable": false },
					null,
					null,
					null,
					null,
					null, 
					null,
					null,
					null,
					null,
					{ "searchable": false },
				],
				"aoColumnDefs" : [
	 				 {
	 				   'bSortable' : false,
	 				   'aTargets' : [ 10 ]
	 			}],
		    });

		    $("#frmAdd").validate({
                rules:
                {
                	circles:{required: true,},
                },
                messages:
                {
                	circles:{required: 'Please select circle.'},
                },
                highlight: function(element) {
     	           //$(element).attr('class', 'filde error-border');
     	        }, unhighlight: function(element) {
     	           //$(element).removeClass('error-border');
     	        },
     			onfocusout: function(element) {
     				this.element(element);
     			},
     			submitHandler: function(form) {
     			    $(".footer_modal.modal").show();
                     form.submit();
                }
	        });
	    });
	    
	    function fnGetFsoByCircle(id)
		{
			if(id != ''){
				$.ajax({
		            type: "POST",
                    url: "<?php echo base_url() . "sampleslisting/getFsoByCircle";?>",
                    data: {
		                circle_id : id,
		            },
		            success: function(response) {
		            	var obj = jQuery.parseJSON(response);
		                 try {
		                     if (obj['fso_details'] != "") {
		                    	 var selectList = $("#fsoname");
								 selectList.removeAttr("disabled","disabled");
								 
		                    	 $('#fsoname')
		                    	     .find('option')
		                    	     .remove()
		                    	     .end()
		                    	     .append('<option value="all">All FSO</option>'); 
		                      	 $.each(obj['fso_details'], function(idx, obj) {
		                      		$("#fsoname").append("<option value='"+obj.id+"'>"+obj.full_name+"</option>");
		                      	 });
		                     }
		                     else
		                     {
		                    	 $('#fsoname')
		                	     .find('option')
		                	     .remove()
		                	     .end()
		                	     .append('<option value="all">All FSO</option>');
		                     }
		                 }
		                 catch (e) {
		                     alert('Exception while request..');
		                 }
		            }
		        });
	        
	        }
		}
    </script>
</body>

</html>