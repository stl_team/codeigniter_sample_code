<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Samples Under Act</title>

    <?php include_once dirname(__DIR__).'/templates/include_css.php';?>

</head>

<body>

    <div id="wrapper">

    <?php include_once dirname(__DIR__).'/templates/sidebar.php'; ?>

        <div id="page-wrapper" class="gray-bg">
        <?php include_once dirname(__DIR__).'/templates/header.php'; ?>
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-sm-8">
                    <h2>Samples Under Act</h2>
                    <ol class="breadcrumb">
                        <li>
                            Home
                        </li>
                        <li class="active">
                            <strong>Samples Under Act</strong>
                        </li>
                    </ol>
                </div>
            </div>
        	<div class="wrapper wrapper-content animated fadeInRight">
        		<div class="row">
					<div class="col-lg-12">
						<div class="ibox float-e-margins">
							<div class="ibox-title">
								<h5>Samples Under Act</h5>
								<!-- <div class="ibox-tools">
									<a class="btn btn-primary btn-xs" href="<?php echo base_url();?>samples/add"><i class="fa fa-plus-square"></i>&nbsp;Add Sample</a>
								</div> -->
							</div>
							<div class="ibox-content">
								<div id="circlewise" class="panel panel-info">
									<div class="panel-body">
										<form id="frmAdd" class="filter" method="get">
											
											<?php if(((!empty($user_data) && $user_data->do_flag != 1) || ($user_data->type == 2 && $user_data->do_flag == 1)) && !empty($circle_data)):?>
												<div class="col-sm-6 col-lg-3">
													<div class="form-group cls-form-group">
														<div class="control">
									                		<label class="control-label">Circle <span class="clsRequiredFieldLable">*</span></label>
									                		
								                			<select id="circles" name="circles" class="chosen-select">
								                				<option value="" disabled="disabled" selected="selected">Select Circle</option>
								                				<?php 
																	foreach ($circle_data as $_circle_data):
																		if(isset($selected_circle_id) && $selected_circle_id == $_circle_data->id):
																			$selected = "selected=''";
																		else:
																			$selected = "";
																		endif;
																		if(@$_circle_data->district_name != "")
																			$tmp_districtname = @$_circle_data->district_name.' - ';
																		else
																			$tmp_districtname = "";
							                					?>
																	<option class="" value="<?php echo $_circle_data->id; ?>" <?php echo $selected;?> ><?php echo ucfirst($tmp_districtname.$_circle_data->name); ?></option>
																<?php 
							                							endforeach;
							                					?>
								                			</select>
									                	
									            		</div>
								            		</div>
												</div>
											<?php endif;?>	
												<div class="col-sm-6 col-lg-3">
													<div class="form-group cls-form-group">
								                		<label class="control-label">FSO Name</label>
								                		
								                			<select id="fsoname" name="fsoname" class="chosen-select">
								                				<option value="all">All FSO</option>
								                				<?php 
								                					if(!empty($fso_data)):
								                						foreach ($fso_data as $_fso_data):
								                							$selected = "";
								                							if(@$fsoname == $_fso_data->id)
								                								$selected = 'selected="selected"';
								                							
							                					?>
																				<option value="<?php echo $_fso_data->id; ?>" class="<?php echo $this->m_util->GetUserCircleStatusClass($_fso_data->circle_status);?>" <?php echo @$selected;?>><?php echo ucfirst($_fso_data->full_name); ?></option>
							                					<?php 
							                							endforeach;
						                							endif;
							                					?>
								                			</select>
								                		
								            		</div>
												</div>
												<div class="col-sm-6 col-lg-3">
													<div class="form-group cls-form-group">
														<label class="control-label">Year</label>
														
															<select id="year" name="year" class="chosen-select" >
																<?php 
																$selected = "";
																	for($i = $current_year; $i >= START_YEAR ;$i--){
																		if(($i == $current_year && @$year == "")  || @$year == $i)
																			$selected = "selected='true'";
																		else	
																			$selected = "";
																		?>
																		<option value="<?php echo $i;?>" <?php echo $selected;?> ><?php echo $i;?></option>
																	<?php 
																	}
																?>
																<?php 
																$selected = "";
																if(@$year == "All")	$selected = "selected='true'";
																?>
																<option value="All" <?php echo $selected;?> >All</option>
															</select>
														
													</div>
												</div>
												<!-- <?php if(!empty($circle_data)){?>
													<div class="clearfix"></div>
												<?php } ?> -->
												<div class="col-sm-6 col-lg-3">
													<div class="form-group cls-form-group">
														<label class="control-label">Search</label>

														<input id="search" name="search" class="form-control" maxlength="250" type="text" placeholder="Search" value="<?php echo @$search; ?>">

													</div>
												</div>
												<div class="col-sm-12 col-lg-12 ">
													<div class="text-center">
														<button class="btn btn-primary btn-sm" type="button" onClick="window.location='<?php echo @$base_url_for_search;?>'">Clear</button>
														<button class="btn btn-sm btn-success" type="submit">Submit</button>
													</div>
												</div>

										</form>
							    	</div>
								</div>
								<table id="example" class="table table-striped table-hover dt-responsive table-bordered" cellspacing="0" width="100%">
									<thead>
										<tr>
											<th width="25px">#</th>
											<th>Sample Name</th>
											<th>Sample Code</th>
											<th>Category Name</th>
											<th>FSO Name</th>
											<th>FBO Name</th>
											<th>Mode</th>
											<th>Lab Report</th>
											<th>Created Date</th>
											<th>Send Date</th>
											<th>Action</th>
										</tr>
									</thead>

									<tbody>
									<?php 
										if(!empty($sample_data)){
											$start_count = $start_index + 1;
											foreach ($sample_data as $key => $_sample_data){
									?>
									<tr>
										<td><?php echo $start_count;//$key+1;?></td>
										<td><?php echo $this->m_util->truncate($_sample_data->name,35);?></td>
										<td><?php echo $_sample_data->code;?></td>
										<td><?php echo $_sample_data->food_category_no . " - ".$this->m_util->truncate($_sample_data->food_category_name,35);?></td>
										<td><?php echo $_sample_data->created_by_full_name;?></td>
										<td><?php echo $_sample_data->fbo_name;?></td>
										<td>
											
											<?php if($_sample_data->is_offline == 0){?>
													<span class="label label-primary"><i class="fa fa-desktop"></i>&nbsp;Admin</span>
											<?php }else{ ?>
												<span class="label label-primary"><i class="fa fa-desktop"></i>&nbsp;BackData</span>
											<?php } ?>
										</td>
										<td>
										    <?php if(!empty($_sample_data->lab_dispatch_date)){?>
											<?php 
												if($_sample_data->lab_report_status != null && $_sample_data->lab_report_status == 0):
											?>
													<span class="label label-primary"><?php echo "Confirm";?></span>
											<?php 
												elseif($_sample_data->lab_report_status != null && $_sample_data->lab_report_status == 1):
											?>
													<span class="label label-danger"><?php echo "Unsafe";?></span>
											<?php 
												elseif($_sample_data->lab_report_status != null && $_sample_data->lab_report_status == 2):
											?>
													<span class="label label-warning"><?php echo "Misbranded";?></span>
											<?php 
												else:
											?>
													<span class="label label-warning"><?php echo "SubStandard";?></span>
											<?php 
												endif;
											?>
										<?php	}else{?>
										    <span class="label label-default">Pendding</span>
										<?php	}
											?>
										</td>
										<td><?php echo !empty($_sample_data->sample_created_date) ? $this->m_util->date_format($_sample_data->sample_created_date) : '-';?></td>
										<td>
											<?php 
												if(!empty($_sample_data->send_date))
												{
													$array_send_date = explode(' ', $_sample_data->send_date);
													echo $this->m_util->date_format($array_send_date[0]);
												}
												else 
												{
													echo "-";
												}
											?>
										</td>
										<td>
										    <?php if($_sample_data->is_offline == 0 && !empty($_sample_data->lab_prepare_form_b_date)):?>
												<a class="btn btn-primary btn-xs" href="<?php echo base_url()."printforms/printformb/".$_sample_data->unique_code;?>" target="_blank"><i class="fa fa-print"></i>&nbsp;Print Form B</a>
											<?php endif;?>
											<a class="btn btn-success btn-xs" href="<?php echo base_url()."sampleslisting/viewsample/".$_sample_data->unique_code;?>"><i class="fa fa-eye"></i>&nbsp;View</a>
											
					                    </td>
									</tr>
									<?php 
										$start_count++;
										}} 
									?>
		                        </tbody>
								</table>
								<?php if(isset($links)):?>
								<div class="row">
									<div class="col-sm-6">Showing <?php echo $start_index + 1;?> to <?php echo $start_count - 1;?> of <?php echo $total_records;?> entries</div>
									<div class="col-sm-6"><?php echo $links;?></div>
								</div>
								<?php endif;?>
							</div>
						</div>
					</div>

				</div>
			</div>
        <?php include_once dirname(__DIR__).'/templates/footer.php'; ?>

        </div>
        </div>

    <?php include_once dirname(__DIR__).'/templates/include_js.php'; ?>

    <script>
	    $(document).ready(function()
	    {
	    	var fso_count="<?php echo $fso_count; ?>";
	    	if(fso_count==0)
	    	{
				var selectList = $("#fsoname");
				selectList.find("option:gt(0)").remove();
				selectList.attr("disabled","disabled");
			}
	    	else
	    	{
				var selectList = $("#fsoname");
				selectList.removeAttr("disabled","disabled");
			}
			$('select[name="circles"]').on('change', function() {
			    var id = $(this).val();
        		fnGetFsoByCircle(id);
			});
			
		    $('#example').dataTable({
		    	"searching": false,
			    "ordering": false,
			    "paging": false,
			    "info": false,
				"columns": [
					{ "searchable": false },
					null,
					null,
					null,
					null,
					null, 
					null,
					null,
					null,
					null,
					{ "searchable": false },
				],
				"aoColumnDefs" : [
	 				 {
	 				   'bSortable' : false,
	 				   'aTargets' : [ 10 ]
	 			}],
		    });

		    $("#frmAdd").validate({
                rules:
                {
                	circles:{required: true,},
                },
                messages:
                {
                	circles:{required: 'Please select circle.'},
                },
                highlight: function(element) {
     	           //$(element).attr('class', 'filde error-border');
     	        }, unhighlight: function(element) {
     	           //$(element).removeClass('error-border');
     	        },
     			onfocusout: function(element) {
     				this.element(element);
     			},
     			submitHandler: function(form) {
     			    $(".footer_modal.modal").show();
                     form.submit();
                }
	        });
	    });
	    
	    function fnGetFsoByCircle(id)
		{
			if(id != ''){
				$.ajax({
		            type: "POST",
                    url: "<?php echo base_url() . "sampleslisting/getFsoByCircle";?>",
                    data: {
		                circle_id : id,
		            },
		            success: function(response) {
		            	var obj = jQuery.parseJSON(response);
		                 try {
		                     if (obj['fso_details'] != "") {
		                    	 var selectList = $("#fsoname");
								 selectList.removeAttr("disabled","disabled");
								 
		                    	 $('#fsoname')
		                    	     .find('option')
		                    	     .remove()
		                    	     .end()
		                    	     .append('<option value="all">All FSO</option>'); 
		                      	 $.each(obj['fso_details'], function(idx, obj) {
		                      	 	var cls = GetUserCircleStatusClass(obj.circle_status);
		                      		$("#fsoname").append("<option value='"+obj.id+"' class='"+ cls + "'>"+obj.full_name+"</option>");
		                      	 });
		                     }
		                     else
		                     {
		                    	 $('#fsoname')
		                	     .find('option')
		                	     .remove()
		                	     .end()
		                	     .append('<option value="all">All FSO</option>');
		                     }
		                     // $( "#fsoname" ).selectmenu( "refresh", true );
		                     $('#fsoname').trigger("chosen:updated");
		                 }
		                 catch (e) {
		                     alert('Exception while request..');
		                 }
		            }
		        });
	        
	        }
		}
		function searchFilter(){
			var filter = "";
			
			if($.trim($('#search').val()) != ""){
				filter += "&search=" + $.trim($('#search').val());
			}
			if($('#year').val() != "" && $("#year").val() != '<?php echo $current_year; ?>'){
				filter += "&year="+$('#year').val();
			}
			if($.trim($('select[name="circles"]').val()) != ""){
				filter += "&circles=" + $.trim($('select[name="circles"]').val());
			}
			if($.trim($('#fsoname').val()) != "" && $.trim($('#fsoname').val()) != "all"){
				filter += "&fsoname=" + $.trim($('#fsoname').val());
			}
			// console.log(filter);
			window.location='<?php echo $base_url_for_search;?>'+filter;
		}
    </script>
</body>

</html>