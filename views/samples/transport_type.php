<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Transport Type</title>

    <?php include_once dirname(__DIR__).'/templates/include_css.php';?>

</head>

<body>

    <div id="wrapper">

    <?php include_once dirname(__DIR__).'/templates/sidebar.php'; ?>

        <div id="page-wrapper" class="gray-bg">
        <?php include_once dirname(__DIR__).'/templates/header.php'; ?>
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-sm-8">
                    <h2>Transport Type</h2>
                    <ol class="breadcrumb">
                        <li>
                            Home
                        </li>
                        <li class="active">
                            <strong>Transport Type</strong>
                        </li>
                    </ol>
                </div>
            </div>
        	<div class="wrapper wrapper-content animated fadeInRight">
				<div class="row">
	                <div class="col-lg-12">
	                    <div class="ibox">
	                        <div class="ibox-title">
	                            <h5>Add Transport Type</h5>
	                        </div>
	                        <div class="ibox-content">
	                        	<!-- <div class="alert alert-danger fade in error" id="error"> </div> -->
	                        	<?php 
				            		if(!empty($this->session->flashdata('mendatory_taluka_data'))):
				            	?>
				            	<div class="alert alert-danger fade in error" id="error"> 
				            		<?php 
				            			echo $this->session->flashdata('mendatory_taluka_data');
				            		?>
				            	</div>
				            	<?php 
				            		endif;
				            	?>
	                            <form id="frmAdd" action="<?php echo base_url()."samples/addtransportlabdetails";?>" class="form-horizontal" method="post" enctype="multipart/form-data">                      
	                                <fieldset>
	                                    <div class="row">
	                                        <div class="col-lg-6">
	                                        	<div class="form-group cls-form-group">
                                                	<label class="col-sm-3 control-label">Sample Name</label>
                                                	<div class="col-sm-9">
                                                		<input id="sample_name" name="sample_name" type="text" class="form-control" style="color: red; font-weight:bold;" value="<?php echo (!empty($sample_data->name)) ? $sample_data->name : '';?>" readonly>
                                                	</div>
                                            	</div>
                                            	<div class="form-group cls-form-group">
                                                	<label class="col-sm-3 control-label">Sample Code</label>
                                                	<div class="col-sm-9">
                                                		<input id="sample_code" name="sample_code" type="text" class="form-control" style="color: red; font-weight:bold;" value="<?php echo (!empty($sample_data->code)) ? $sample_data->code : '';?>" readonly>
                                                	</div>
                                            	</div>
	                                        	<div class="form-group">
													<label class="col-sm-3 control-label">Transport Type <span class="clsRequiredFieldLable">*</span></label>
													<div class="col-sm-9">
														<div class="control">
															<select id="transport" name="transport" class="chosen-select" >
																<option disabled="disabled" selected="selected">Select</option>
																<?php 
																	if(!empty($transport_type_data)):
																		foreach ($transport_type_data as $_transport_type_data):
																?>
																			<option value="<?php echo $_transport_type_data->id;?>"><?php echo $_transport_type_data->type;?></option>
																<?php 
																		endforeach;
																	endif;
																?>
															</select>
														</div>
													</div>
												</div>
	                                        </div>
                                    	</div>
	                                    <div class="clsSubmitButtons">
	                                    	<input type="hidden" id="hdn_id" name="hdn_id" value="<?php echo $sample_data->unique_code;?>">
	                                    	<button id="btnAdd" class="btn btn-sm btn-success" name="btnAdd" type="submit">Submit</button>
											<a class="btn btn-white" href="javascript:history.back()">Cancel</a>
										</div>
	                                </fieldset>
								</form>
	                        </div>
	                    </div>
					</div>
	        	</div>
			</div>
        <?php include_once dirname(__DIR__).'/templates/footer.php'; ?>

        </div>
        </div>

    <?php include_once dirname(__DIR__).'/templates/include_js.php'; ?>

    <script>
    	$(document).ready(function(){
    		$.validator.addMethod("lettersonly", function (value, element) {
    			return this.optional(element) || /^[a-z]+$/i.test(value);
        	}, "Please enter only letters.");

    		$.validator.addMethod("lettersandspaceonly", function(value, element) {
    			return this.optional(element) || /^[a-z\s]+$/i.test(value);
    		}, "Accept only Letters and Space");
			
    		$.validator.addMethod("lettersnumbersonly", function(value, element) {
    	        return this.optional(element) || /^[a-z0-9]+$/i.test(value);
    	    }, "Accept only letters and numbers.");

    		$("#frmAdd").validate({
                rules:
                {
                	transport:{required: true},
                },
                messages:
                {
                	transport:{required: "Please select transport type.",},
                },
                highlight: function(element) {
     	           //$(element).attr('class', 'filde error-border');
     	        }, unhighlight: function(element) {
     	           //$(element).removeClass('error-border');
     	        },
     			onfocusout: function(element) {
     				this.element(element);
     			},
     			submitHandler: function(form) {
     				var is_confirm =confirm('Please check all detail are correctly entered in this form. Details can not be edit after submit. Are you sure want to submit?');
					if(is_confirm)
					{
                    	form.submit();
					}
					else
					{
						return false;
					}
                }
	        });

    	});
    </script>
</body>

</html>