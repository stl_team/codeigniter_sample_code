<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Samples Under Act</title>

    <?php include_once dirname(__DIR__).'/templates/include_css.php';?>

</head>

<body>

    <div id="wrapper">

    <?php include_once dirname(__DIR__).'/templates/sidebar.php'; ?>

        <div id="page-wrapper" class="gray-bg">
        <?php include_once dirname(__DIR__).'/templates/header.php'; ?>
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-sm-12">
                    <h2>Sample Collection Form For Samples Under Act</h2>
                    <ol class="breadcrumb">
                        <li>
                            Home
                        </li>
                        <li class="active">
                            <strong>Sample Collection Form For Samples Under Act</strong>
                        </li>
                    </ol>
                </div>
            </div>
        	<div class="wrapper wrapper-content animated fadeInRight">
				<div class="row">
	                <div class="col-lg-12">
	                    <div class="ibox">
	                        <div class="ibox-title">
	                            <h5>Sample Collection Form For Samples Under Act</h5>
	                        </div>
	                        <div class="ibox-content">
	                        	<!-- <div class="alert alert-danger fade in error" id="error"> </div> -->
	                        	<?php 
				            		if(!empty($this->session->flashdata('mendatory_sample_fbo_data'))):
				            	?>
				            	<div class="alert alert-danger fade in error" id="error"> 
				            		<?php 
				            			if(!empty($this->session->flashdata('mendatory_sample_fbo_data')))
				            			{
				            				echo $this->session->flashdata('mendatory_sample_fbo_data');
				            			}
				            		?>
				            	</div>
				            	<?php 
				            		endif;
				            	?>
	                            <form id="frmAdd" action="<?php echo base_url()."samples/create";?>" class="form-horizontal" method="post" enctype="multipart/form-data">                      
	                                <fieldset>
	                                    <div class="row">
	                                        <div class="col-lg-6">
	                                        	<div class="panel panel-info">
                                        			<div class="panel-heading">
                                            			FSO Details
                                        			</div>
                                        			<div class="panel-body">
                                        				<div class="form-group cls-form-group">
	                                                		<label class="col-sm-3 control-label">FSO Name</label>
	                                                		<div class="col-sm-9">
	                                                			<input id="fso_name" name="fso_name" type="text" class="form-control" value="<?php echo (!empty($user_data->full_name)) ? $user_data->full_name : '';?>" readonly>
	                                                		</div>
	                                            		</div>
	                                            		<div class="form-group cls-form-group">
	                                                		<label class="col-sm-3 control-label">Circle Name <span class="clsRequiredFieldLable">*</span></label>
	                                                		<div class="col-sm-9">
																<?php 
																if(@$circle_count > 1){
																?>
																<div class="control">
																	<select id="fbo_circle_id" name="fbo_circle_id" class="chosen-select" >
																		<option disabled="disabled" selected="selected">Select</option>
																		<?php
																		foreach ($circle_detail as $_circle_detail):
										                				?>
																			<option value="<?php echo $_circle_detail->id;?>"><?php echo $_circle_detail->name;?></option>
						                								<?php 
																		endforeach;
																		?>
																	</select>
																</div>
																<?php 
																}else{
																?>
																	<input id="fso_circle" name="fso_circle" type="text" class="form-control" value="<?php echo (!empty($circle_detail->name)) ? $circle_detail->name : '';?>" readonly>
																	<input id="fbo_circle_id" name="fbo_circle_id" type="hidden" class="form-control" value="<?php echo (!empty($circle_detail->id)) ? $circle_detail->id : 0;?>" readonly>
																<?php 
																}
																?>
	                                                		</div>
	                                            		</div>
	                                            		<div class="form-group cls-form-group" id="data_1">
															<label class="col-sm-3 control-label">Sample Collection Date <span class="clsRequiredFieldLable">*</span></label>
															<div class="col-sm-9">
																<div class="control">
																	<div class="input-group date">
									                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input id="created_date" name="created_date" readonly="" type="text" class="form-control">
									                                </div>
																</div>
															</div>
														</div>
														<div class="form-group cls-form-group">
															<label class="col-sm-3 control-label">Sample Collection Time <span class="clsRequiredFieldLable">*</span></label>
															<div class="col-sm-3">
																<div class="control">
																	<select id="hour" name="hour" class="chosen-select" >
										                				<option disabled="disabled" selected="selected">Select</option>
										                				<?php 
																			$pre_zero = 0;
										                					$val = 0;
										                					for($i=1;$i<=12;$i++):
										                					if($i < 10)
										                					{
										                						$val = $pre_zero.$i;
										                					}
										                					else 
										                					{
										                						$val = $i;
										                					}
										                				?>
										                					<option value="<?php echo $val;?>"><?php echo $val;?></option>
										                				<?php 
										                					endfor;
										                				?>
										                			</select>
																</div>
															</div>
															<div class="col-sm-3">
																<div class="control">
																	<select id="minute" name="minute" class="chosen-select" >
										                				<option disabled="disabled" selected="selected">Select</option>
										                				<?php 
											                				$pre_zero = 0;
											                				$val = 0;
											                				for($i=0;$i<60;$i++):
											                				if($i < 10)
											                				{
											                					$val = $pre_zero.$i;
											                				}
											                				else
											                				{
											                					$val = $i;
											                				}
										                				?>
										                					<option value="<?php echo $val;?>"><?php echo $val;?></option>
										                				<?php 
										                					endfor;
										                				?>
										                			</select>
																</div>
															</div>
															<div class="col-sm-3">
																<div class="control">
																	<select id="ampm" name="ampm" class="chosen-select" >
										                				<option disabled="disabled" selected="selected">Select</option>
										                				<option value="AM">AM</option>
										                				<option value="PM">PM</option>
										                			</select>
																</div>
															</div>
														</div>
	                                            	</div>
                                    			</div>
                                    			<div class="panel panel-primary">
                                        			<div class="panel-heading">
                                            			FBO Details
                                        			</div>
                                        			<div class="panel-body">
                                            			<div class="form-group cls-form-group">
	                                                		<label class="col-sm-3 control-label">FBO Name <span class="clsRequiredFieldLable">*</span></label>
	                                                		<div class="col-sm-9">
	                                                			<div class="control">
	                                                				<input id="fbo_name" name="fbo_name" type="text" class="form-control" maxlength="250">
	                                                			</div>
	                                                		</div>
	                                            		</div>
	                                            		<div class="form-group cls-form-group">
	                                                		<label class="col-sm-3 control-label">Firm Name <span class="clsRequiredFieldLable">*</span></label>
	                                                		<div class="col-sm-9">
	                                                			<div class="control">
	                                                				<input id="firm_name" name="firm_name" type="text" class="form-control" maxlength="250">
	                                                			</div>
	                                                		</div>
	                                            		</div>
	                                            		<div class="form-group cls-form-group">
									                		<label class="col-sm-3 control-label">FBO Status <span class="clsRequiredFieldLable">*</span></label>
									                		<div class="col-sm-9">
										                		<div class="control">
										                			<select id="fbo_status" name="fbo_status" class="chosen-select" >
										                				<option disabled="disabled" selected="selected">Select</option>
										                				<?php 
										                					if(!empty($fbo_status_details)):
										                						foreach ($fbo_status_details as $_fbo_status_details):
										                				?>
									                								<option value="<?php echo $_fbo_status_details->id;?>"><?php echo $_fbo_status_details->status;?></option>
						                								<?php 
										                						endforeach;
										                					endif;
						                								?>
						                								<option value="other_fbo_status">Other</option>
										                			</select>
										                		</div>
									                		</div>
					                					</div>
					                					<div id="divOtherFboStatus" class="form-group cls-form-group">
	                                                		<label class="col-sm-3 control-label">Other <span class="clsRequiredFieldLable">*</span></label>
	                                                		<div class="col-sm-9">
	                                                			<div class="control">
	                                                				<input id="other_fbo_status" name="other_fbo_status" type="text" class="form-control" maxlength="450">
	                                                			</div>
	                                                		</div>
	                                            		</div>
					                					<div class="form-group cls-form-group">
									                		<label class="col-sm-3 control-label">Business Address <span class="clsRequiredFieldLable">*</span></label>
				                                			<div class="col-sm-9">
				                                				<div class="control">
				                                					<textarea id="fbo_address1" name="fbo_address1" class="form-control" rows="2" maxlength="950"></textarea>
				                                				</div>
				                                			</div>
					                					</div>
					                					<div class="form-group cls-form-group">
									                		<label class="col-sm-3 control-label">Residential Address </label>
				                                			<div class="col-sm-9">
				                                				<textarea id="fbo_address2" name="fbo_address2" class="form-control" rows="2" maxlength="950"></textarea>
			                                				</div>
					                					</div>
					                					<div class="form-group cls-form-group">
									                		<label class="col-sm-3 control-label">District <span class="clsRequiredFieldLable">*</span></label>
									                		<div class="col-sm-9">
										                		<div class="control">
										                			<select id="fbo_district" name="fbo_district" class="chosen-select" >
										                				<option disabled="disabled" selected="selected">Select</option>
										                				<?php 
										                					if(!empty($district_details)):
										                						foreach ($district_details as $_district_details):
										                							if(isset($fso_district_id) && $fso_district_id == $_district_details->id):
										                				?>
									                									<option value="<?php echo $_district_details->id;?>" selected="selected"><?php echo $_district_details->name;?></option>
									                								<?php
																					else:
																					?>
																						<option value="<?php echo $_district_details->id;?>"><?php echo $_district_details->name;?></option>
						                								<?php 
						                											endif;
										                						endforeach;
										                					endif;
						                								?>
										                			</select>
										                		</div>
									                		</div>
					                					</div>
					                					<div class="form-group cls-form-group">
									                		<label class="col-sm-3 control-label">Taluko <span class="clsRequiredFieldLable">*</span></label>
									                		<div class="col-sm-9">
										                		<div class="control">
										                			<select id="fbo_taluko" name="fbo_taluko" class="chosen-select" >
										                				<option disabled="disabled" selected="selected">Select</option>
										                				<?php 
										                					if(!empty($taluka_details)):
										                						foreach ($taluka_details as $_taluka_details):
										                				?>
									                								<option value="<?php echo $_taluka_details->id;?>"><?php echo $_taluka_details->name;?></option>
						                								<?php 
										                						endforeach;
										                					endif;
						                								?>
										                			</select>
										                		</div>
									                		</div>
					                					</div>
					                					<div class="form-group cls-form-group">
	                                                		<label class="col-sm-3 control-label">Place <span class="clsRequiredFieldLable">*</span></label>
	                                                		<div class="col-sm-9">
	                                                			<div class="control">
	                                                				<input id="fbo_place" name="fbo_place" type="text" class="form-control" maxlength="950">
	                                                			</div>
	                                                		</div>
	                                            		</div>
	                                            		<div class="form-group cls-form-group">
	                                                		<label class="col-sm-3 control-label">Pincode</label>
	                                                		<div class="col-sm-9">
	                                                			<div class="control">
	                                                				<input id="fbo_pincode" name="fbo_pincode" type="text" class="form-control" maxlength="6">
	                                                			</div>
	                                                		</div>
	                                            		</div>
	                                            		<div class="form-group cls-form-group">
									                		<label class="col-sm-3 control-label">Kind of Business <span class="clsRequiredFieldLable">*</span></label>
									                		<div class="col-sm-9">
										                		<div class="control">
										                			<select id="fbo_business" name="fbo_business" class="chosen-select" >
										                				<option disabled="disabled" selected="selected">Select</option>
										                				<?php 
										                					if(!empty($kind_of_business_details)):
										                						foreach ($kind_of_business_details as $_kind_of_business_details):
										                				?>
									                								<option value="<?php echo $_kind_of_business_details->id;?>"><?php echo $_kind_of_business_details->business;?></option>
						                								<?php 
										                						endforeach;
										                					endif;
						                								?>
						                								<option value="other_business">Other</option>
										                			</select>
										                		</div>
									                		</div>
					                					</div>
					                					<div id="divOtherBusiness" class="form-group cls-form-group">
	                                                		<label class="col-sm-3 control-label">Other <span class="clsRequiredFieldLable">*</span></label>
	                                                		<div class="col-sm-9">
	                                                			<div class="control">
	                                                				<input id="other_business" name="other_business" type="text" class="form-control" maxlength="450">
	                                                			</div>
	                                                		</div>
	                                            		</div>
	                                            		<div class="form-group cls-form-group">
									                		<label class="col-sm-3 control-label">Firm Constitution <span class="clsRequiredFieldLable">*</span></label>
									                		<div class="col-sm-9">
										                		<div class="control">
										                			<select id="fbo_firmtype" name="fbo_firmtype" class="chosen-select" >
										                				<option disabled="disabled" selected="selected">Select</option>
										                				<?php 
										                					if(!empty($firm_type_details)):
										                						foreach ($firm_type_details as $_firm_type_details):
										                				?>
									                								<option value="<?php echo $_firm_type_details->id;?>"><?php echo $_firm_type_details->type;?></option>
						                								<?php 
										                						endforeach;
										                					endif;
						                								?>
						                								<option value="other_firmtype">Other</option>
										                			</select>
										                		</div>
									                		</div>
					                					</div>
					                					<div id="divOtherFirmType" class="form-group cls-form-group">
	                                                		<label class="col-sm-3 control-label">Other <span class="clsRequiredFieldLable">*</span></label>
	                                                		<div class="col-sm-9">
	                                                			<div class="control">
	                                                				<input id="other_firmtype" name="other_firmtype" type="text" class="form-control" maxlength="250">
	                                                			</div>
	                                                		</div>
	                                            		</div>
					                					<div class="form-group cls-form-group">
	                                                		<label class="col-sm-3 control-label">License/Registration No.</label>
	                                                		<div class="col-sm-9">
	                                                			<div class="control">
	                                                				<input id="fbo_license_reg_no" name="fbo_license_reg_no" type="text" class="form-control" maxlength="250">
	                                                			</div>
	                                                		</div>
	                                            		</div>
					                				</div>
                                    			</div>
	                                        </div>
	                                    	<div class="col-lg-6">
	                                        	<div class="panel panel-success">
                                        			<div class="panel-heading">
                                            			Sample Details
                                        			</div>
                                        			<div class="panel-body">
                                        				<div class="form-group cls-form-group">
									                		<label class="col-sm-3 control-label">Laboratory <span class="clsRequiredFieldLable">*</span></label>
									                		<div class="col-sm-9">
										                		<div class="control">
										                			<select id="lab_id" name="lab_id" class="chosen-select" >
										                				<option disabled="disabled" selected="selected">Select</option>
										                				<?php 
										                					if(!empty($lab_details)):
										                						foreach ($lab_details as $_lab_details):
										                				?>
									                								<option value="<?php echo $_lab_details->id;?>"><?php echo $_lab_details->name;?></option>
						                								<?php 
										                						endforeach;
										                					endif;
						                								?>
						                							</select>
										                		</div>
									                		</div>
					                					</div>
                                            			<div class="form-group cls-form-group">
	                                                		<label class="col-sm-3 control-label">Sample Name <span class="clsRequiredFieldLable">*</span></label>
	                                                		<div class="col-sm-9">
	                                                			<div class="control">
	                                                				<input id="sample_name" name="sample_name" type="text" class="form-control" maxlength="1950">
	                                                			</div>
	                                                		</div>
	                                            		</div>
	                                            		<div class="form-group cls-form-group">
	                                                		<label class="col-sm-3 control-label">Place of Collection <span class="clsRequiredFieldLable">*</span></label>
	                                                		<div class="col-sm-9">
	                                                			<div class="control">
	                                                				<input id="short_name" name="short_name" type="text" class="form-control" maxlength="250">
	                                                			</div>
	                                                		</div>
	                                            		</div>
	                                            		<div class="form-group cls-form-group">
	                                                		<label class="col-sm-3 control-label">Sample Code of DO Slip <span class="clsRequiredFieldLable">*</span></label>
	                                                		<div class="col-sm-9">
	                                                			<div class="control">
	                                                				<input id="sample_code" name="sample_code" type="text" class="form-control" maxlength="250">
	                                                			</div>
	                                                			<!-- <div class="col-sm-4"><?php //echo $circle_detail->circle_id.$circle_detail->short_name.$user_data->fso_id."/";?></div>
	                                                			<div class="control col-sm-3">
	                                                				<input id="sample_code" name="sample_code" type="text" class="form-control" value="<?php //echo (!empty($sample_no)) ? $sample_no : "";?>" maxlength="8">
	                                                			</div>
	                                                			<div class="col-sm-5"><?php //echo "/".$current_year;?></div> -->
	                                                		</div>
	                                            		</div>
	                                            		<div class="form-group cls-form-group">
									                		<label class="col-sm-3 control-label">Food Category <span class="clsRequiredFieldLable">*</span></label>
									                		<div class="col-sm-9">
										                		<div class="control">
										                			<select id="food_category" name="food_category" class="chosen-select" >
										                				<option disabled="disabled" selected="selected">Select</option>
										                				<?php 
										                					if(!empty($food_categories_details)):
										                						foreach ($food_categories_details as $_food_categories_details):
										                				?>
									                								<option value="<?php echo $_food_categories_details->id;?>"><?php echo $_food_categories_details->category_no . " - ". $_food_categories_details->name;?></option>
						                								<?php 
										                						endforeach;
										                					endif;
						                								?>
										                			</select>
										                		</div>
									                		</div>
					                					</div>
					                					<div class="form-group cls-form-group">
									                		<label class="col-sm-3 control-label">Food Sub Category <span class="clsRequiredFieldLable">*</span></label>
									                		<div class="col-sm-9">
										                		<div class="control">
										                			<select id="food_subcategory" name="food_subcategory" class="chosen-select" >
										                				<option disabled="disabled" selected="selected">Select</option>
										                			</select>
										                		</div>
									                		</div>
					                					</div>
					                					<div class="form-group cls-form-group">
									                		<label class="col-sm-3 control-label">Manufacturer Address <span class="clsRequiredFieldLable">*</span></label>
				                                			<div class="col-sm-9">
				                                				<div class="control">
				                                					<textarea id="manufacturer_address" name="manufacturer_address" class="form-control" rows="2" maxlength="950"></textarea>
				                                				</div>
				                                			</div>
					                					</div>
					                					<div class="form-group cls-form-group">
									                		<label class="col-sm-3 control-label">Type of Sample <span class="clsRequiredFieldLable">*</span></label>
									                		<div class="col-sm-9">
										                		<div class="control">
										                			<select id="pack_type" name="pack_type" class="chosen-select" >
										                				<option disabled="disabled" selected="selected">Select</option>
										                				<?php 
										                					if(!empty($pack_type_details)):
										                						foreach ($pack_type_details as $_pack_type_details):
										                				?>
									                								<option value="<?php echo $_pack_type_details->id;?>"><?php echo $_pack_type_details->type;?></option>
						                								<?php 
										                						endforeach;
										                					endif;
						                								?>
						                								<option value="other_packtype">Other</option>
										                			</select>
										                		</div>
									                		</div>
					                					</div>
					                					<div id="divOtherPackType" class="form-group cls-form-group">
	                                                		<label class="col-sm-3 control-label">Other <span class="clsRequiredFieldLable">*</span></label>
	                                                		<div class="col-sm-9">
	                                                			<div class="control">
	                                                				<input id="other_packtype" name="other_packtype" type="text" class="form-control" maxlength="250">
	                                                			</div>
	                                                		</div>
	                                            		</div>
	                                            		<div id="divPackOrPackFrom">
		                                            		<div class="form-group cls-form-group">
		                                                		<label class="col-sm-3 control-label">Lot/Code/Batch No. <span class="clsRequiredFieldLable">*</span></label>
		                                                		<div class="col-sm-9">
		                                                			<div class="control">
		                                                				<input id="batch_no" name="batch_no" type="text" class="form-control" maxlength="250">
		                                                			</div>
		                                                		</div>
		                                            		</div>
		                                            		<div class="form-group cls-form-group">
																<label class="col-sm-3 control-label">Date of Manufacture or Packing<span class="clsRequiredFieldLable">*</span></label>
																<div class="col-sm-9">
																	<div class="control">
																		<input id="packing_date" name="packing_date" type="text" class="form-control" maxlength="250">
										                            </div>
																</div>
															</div>
															<div class="form-group cls-form-group">
																<label class="col-sm-3 control-label">Best Before/Use By Date <span class="clsRequiredFieldLable">*</span></label>
																<div class="col-sm-9">
																	<div class="control">
																		<input id="used_by_date" name="used_by_date" type="text" class="form-control" maxlength="250">
										                            </div>
																</div>
															</div>
	                                            		</div>
					                					<div class="form-group cls-form-group">
	                                                		<label class="col-sm-3 control-label">Quantity <span class="clsRequiredFieldLable">*</span></label>
	                                                		<div class="col-sm-3">
	                                                			<div class="control">
	                                                				<input id="quantity" name="quantity" type="text" class="form-control" maxlength="15">
	                                                			</div>
	                                                		</div>
	                                                		<div class="col-sm-3">
	                                                			<div class="control">
	                                                				<input id="quantity_type" name="quantity_type" type="text" class="form-control" placeholder="GM/ML/Pack" maxlength="250">
	                                                			</div>
	                                                		</div>
	                                                		<div class="col-sm-3">
	                                                			<span class="help-block m-b-none clsHelpLable"> GM/ML/Pack</span>
	                                                		</div>
	                                                	</div>
	                                                	<div class="form-group cls-form-group">
	                                                		<label class="col-sm-3 control-label">Price <span class="clsRequiredFieldLable">*</span></label>
	                                                		<div class="col-sm-3">
	                                                			<div class="control">
	                                                				<input id="price" name="price" type="text" class="form-control" maxlength="15">
	                                                			</div>
	                                                		</div>
	                                                		<div class="col-sm-3">
	                                                			<div class="control">
	                                                				<input id="price_type" name="price_type" type="text" class="form-control" placeholder="KG/LTR/Pack" maxlength="250">
	                                                			</div>
	                                                		</div>
	                                                		<div class="col-sm-3">
	                                                			<span class="help-block m-b-none clsHelpLable">Per KG/LTR/Pack</span>
	                                                		</div>
	                                                	</div>
	                                                	<div class="form-group cls-form-group">
	                                                		<label class="col-sm-3 control-label">Payment made to FBO <span class="clsRequiredFieldLable">*</span></label>
	                                                		<div class="col-sm-9">
	                                                			<div class="control">
	                                                				<input id="payment_fbo" name="payment_fbo" type="text" class="form-control" maxlength="15">
	                                                			</div>
	                                                		</div>
	                                                	</div>
	                                                	<div class="form-group cls-form-group">
	                                                		<label class="col-sm-3 control-label">Cash Memo No.</label>
	                                                		<div class="col-sm-9">
	                                                			<div class="control">
	                                                				<input id="receipt_no" name="receipt_no" type="text" class="form-control" maxlength="450">
	                                                			</div>
	                                                		</div>
	                                                	</div>
	                                                	<div class="form-group cls-form-group">
									                		<label class="col-sm-3 control-label">Kind of Sample <span class="clsRequiredFieldLable">*</span></label>
									                		<div class="col-sm-9">
										                		<div class="control">
										                			<select id="investigation_type" name="investigation_type" class="chosen-select" >
										                				<option disabled="disabled" selected="selected">Select</option>
										                				<?php 
										                					if(!empty($investigation_type_details)):
										                						foreach ($investigation_type_details as $_investigation_type_details):
										                				?>
									                								<option value="<?php echo $_investigation_type_details->id;?>"><?php echo $_investigation_type_details->type;?></option>
						                								<?php 
										                						endforeach;
										                					endif;
						                								?>
										                			</select>
										                		</div>
									                		</div>
					                					</div>
					                					<div class="form-group cls-form-group">
									                		<label class="col-sm-3 control-label">Preservative Added</label>
				                                			<div class="col-sm-9">
				                                				<div class="control">
				                                					<textarea id="preservation_details" name="preservation_details" class="form-control" rows="2" maxlength="1950"></textarea>
				                                				</div>
				                                			</div>
					                					</div>
					                					<div class="form-group cls-form-group">
									                		<label class="col-sm-3 control-label">Disclosure of purchase of food by FBO</label>
				                                			<div class="col-sm-9">
				                                				<div class="control">
				                                					<textarea id="disclosure" name="disclosure" class="form-control" rows="2" maxlength="4950"></textarea>
				                                				</div>
				                                			</div>
					                					</div>
					                					<!-- <div class="form-group cls-form-group">
				                           					<p><label class="col-sm-3 control-label">Whether FBO demand for Referral Laboratory</label></p>
			                                        		<div class="col-sm-9">
				                                        		<div class="radio radio-inline" style="margin-top: 0 !important;">
				                                            		<input type="radio" id="reflab" value="1" name="reflab">
				                                            		<label for="reflab">Yes</label>
				                                        		</div>
				                                        		<div class="radio radio-inline">
				                                            		<input type="radio" id="reflab" value="0" name="reflab" checked>
				                                            		<label for="reflab">No</label>
				                                        		</div>
			                                        		</div>
														</div>
														<div id="divreflab" class="form-group cls-form-group" style="display: none;">
									                		<label class="col-sm-3 control-label">Referral Laboratory <span class="clsRequiredFieldLable">*</span></label>
									                		<div class="col-sm-9">
										                		<div class="control">
										                			<select id="referral_lab" name="referral_lab" class="form-control" >
										                				<option disabled="disabled" selected="selected">Select</option>
										                				<?php 
										                					//if(!empty($referral_lab_details)):
										                						//foreach ($referral_lab_details as $_referral_lab_details):
										                				?>
									                								<option value="<?php echo $_referral_lab_details->id;?>"><?php echo $_referral_lab_details->lab_name.' ('.$_referral_lab_details->place.')';?></option>
						                								<?php 
										                						//endforeach;
										                					//endif;
						                								?>
										                			</select>
										                		</div>
									                		</div>
					                					</div> -->
					                					<div id="divfboacclab" class="form-group cls-form-group">
				                           					<p><label class="col-sm-3 control-label">Whether FBO demand for Accredited Laboratory</label></p>
			                                        		<div class="col-sm-9">
				                                        		<div class="radio radio-inline" style="margin-top: 0 !important;">
				                                            		<input type="radio" id="lab" value="1" name="lab">
				                                            		<label for="lab">Yes</label>
				                                        		</div>
				                                        		<div class="radio radio-inline">
				                                            		<input type="radio" id="lab" value="0" name="lab" checked>
				                                            		<label for="lab">No</label>
				                                        		</div>
			                                        		</div>
														</div>
														<div id="divacclab" class="form-group cls-form-group" style="display: none;">
									                		<label class="col-sm-3 control-label">Accredited Laboratory <span class="clsRequiredFieldLable">*</span></label>
									                		<div class="col-sm-9">
										                		<div class="control">
										                			<select id="accredited_lab" name="accredited_lab" class="chosen-select" >
										                				<option disabled="disabled" selected="selected">Select</option>
										                				<?php 
										                					if(!empty($accredited_lab_details)):
										                						foreach ($accredited_lab_details as $_accredited_lab_details):
										                				?>
									                								<option value="<?php echo $_accredited_lab_details->id;?>"><?php echo $_accredited_lab_details->lab_name.' ('.$_accredited_lab_details->place.')';?></option>
						                								<?php 
										                						endforeach;
										                					endif;
						                								?>
										                			</select>
										                		</div>
									                		</div>
					                					</div>
					                					<div class="form-group cls-form-group">
									                		<label class="col-sm-3 control-label">Witness Name</label>
									                		<div class="col-sm-9">
									                			<div class="control">
									                				<input id="witness_name" name="witness_name" type="text" class="form-control" maxlength="450">
									                			</div>
									                		</div>
				                                		</div>
					                					<div class="form-group cls-form-group">
									                		<label class="col-sm-3 control-label">Witness Address</label>
				                                			<div class="col-sm-9">
				                                				<div class="control">
				                                					<textarea id="witness_address" name="witness_address" class="form-control" rows="2" maxlength="950"></textarea>
				                                				</div>
				                                			</div>
					                					</div>
					                					<div class="form-group cls-form-group">
				                           					<label class="col-sm-3 control-label">Food Article Seized</label>
			                                        		<div class="col-sm-9">
				                                        		<div class="radio radio-inline" style="margin-top: 0 !important;">
				                                            		<input type="radio" id="seized" value="1" name="seized">
				                                            		<label for="seized">Yes</label>
				                                        		</div>
				                                        		<div class="radio radio-inline">
				                                            		<input type="radio" id="seized" value="0" name="seized" checked>
				                                            		<label for="seized">No</label>
				                                        		</div>
			                                        		</div>
														</div>
														<div id="divQuantity" class="form-group cls-form-group">
	                                                		<label class="col-sm-3 control-label">Quantity (if in kg)</label>
	                                                		<div class="col-sm-3">
	                                                			<div class="control">
	                                                				<input id="seized_quantity" name="seized_quantity" type="text" class="form-control" maxlength="15">
	                                                			</div>
	                                                		</div>
	                                                		<div class="col-sm-3">
	                                                			<div class="control">
	                                                				<input id="seized_quantity_type" name="seized_quantity_type" type="text" class="form-control" placeholder="KG" maxlength="250">
	                                                			</div>
	                                                		</div>
	                                                		<div class="col-sm-3 cls-form-group">
	                                                			<span class="help-block m-b-none clsHelpLable">KG</span>
	                                                		</div>
	                                                	</div>
	                                                	<div id="divQuantityltr" class="form-group cls-form-group">
	                                                		<label class="col-sm-3 control-label">Quantity (if in ml/ltr)</label>
	                                                		<div class="col-sm-3">
	                                                			<div class="control">
	                                                				<input id="seized_quantity_ml_ltr" name="seized_quantity_ml_ltr" type="text" class="form-control" maxlength="15">
	                                                			</div>
	                                                		</div>
	                                                		<div class="col-sm-3">
	                                                			<div class="control">
	                                                				<input id="seized_quantity_type_ml_ltr" name="seized_quantity_type_ml_ltr" type="text" class="form-control" placeholder="ML/LTR" maxlength="250">
	                                                			</div>
	                                                		</div>
	                                                		<div class="col-sm-3 cls-form-group">
	                                                			<span class="help-block m-b-none clsHelpLable">ML/LTR</span>
	                                                		</div>
	                                                	</div>
	                                                	<div id="divPrice" class="form-group cls-form-group">
	                                                		<label class="col-sm-3 control-label">Total Price <span class="clsRequiredFieldLable">*</span></label>
	                                                		<div class="col-sm-9">
	                                                			<div class="control">
	                                                				<input id="seized_price" name="seized_price" type="text" class="form-control" maxlength="15">
	                                                			</div>
	                                                		</div>
	                                                	</div>
	                                                	<div class="form-group cls-form-group">
			                                                <label class="col-sm-3 control-label">Sample Image</label>
			                                                <div class="col-sm-9">
																<div class="control">
																	<input id="image" name="image" type="file">
																</div>
															</div>
			                                            </div>
			                                            <div class="form-group cls-form-group">
			                                                <label class="col-sm-3 control-label"></label>
			                                                <div class="col-sm-9">
																<div class="control">
																	<img id="imgSelect" src="" alt="" height="100px" width="100px"/>
																</div>
															</div>
			                                            </div>
			                                        </div>
                                    			</div>   
	                                    	</div>
	                                    </div>
	                                    <div class="clsSubmitButtons">
	                                    	<!-- <input type="hidden" id="hdn_sample_code" name="hdn_sample_code" value="<?php //echo (!empty($sample_code)) ? $sample_code : "";?>">
	                                    	<input type="hidden" id="hdn_sample_no" name="hdn_sample_no" value="<?php //echo (!empty($sample_no)) ? $sample_no : "";?>"> -->
	                                    	<button id="btnAdd" class="btn btn-sm btn-success" name="btnAdd" type="submit">Submit</button>
											<a class="btn btn-white" href="javascript:history.back()">Cancel</a>
										</div>
	                                </fieldset>
								</form>
	                        </div>
	                    </div>
					</div>
	        	</div>
			</div>
        <?php include_once dirname(__DIR__).'/templates/footer.php'; ?>

        </div>
        </div>

    <?php include_once dirname(__DIR__).'/templates/include_js.php'; ?>

    <script>
    	$(document).ready(function(){
    		var nowDate = new Date();
    		var dd = nowDate.getDate();
    	    var mm = nowDate.getMonth()+1; //January is 0!

    	    var yyyy = nowDate.getFullYear();
    	    if(dd<10){
    	        dd='0'+dd
    	    } 
    	    if(mm<10){
    	        mm='0'+mm
    	    } 
    	    var today = dd+'/'+mm+'/'+yyyy;
			//var today = new Date(nowDate.getFullYear(), nowDate.getMonth(), nowDate.getDate(), 0, 0, 0, 0);
			$('#created_date').val(today);
        	$('#data_1 .input-group.date').datepicker({
        		format: "dd/mm/yyyy",
        		endDate: today,
        		clearBtn: true,
                keyboardNavigation: false,
                forceParse: false,
                todayHighlight: true,
                autoclose: true
            });

        	/*$('#packing_date').val(today);
        	$('#data_2 .input-group.date').datepicker({
        		format: "dd/mm/yyyy",
        		endDate: today,
        		clearBtn: true,
                keyboardNavigation: false,
                forceParse: false,
                todayHighlight: true,
                autoclose: true
            });

        	$('#used_by_date').val(today);
        	$('#data_3 .input-group.date').datepicker({
        		format: "dd/mm/yyyy",
        		endDate: today,
        		clearBtn: true,
                keyboardNavigation: false,
                forceParse: false,
                todayHighlight: true,
                autoclose: true
            });*/
        	
    		$("#divQuantity").hide();
            $("#divQuantityltr").hide();
    		$("#divPrice").hide();
    		$("#divOtherFboStatus").hide();
    		$("#divOtherBusiness").hide();
    		$("#divOtherFirmType").hide();
    		$("#divOtherPackType").hide();
    		$("#divPackOrPackFrom").hide();
    		
    		$("#food_subcategory").attr("disabled","disabled");
    		
    		$("#image").change(function(){
                readURL(this);
            });

    		$('select[name="fbo_status"]').change(function(){
        		var id = $(this).val();
        		if(id == "other_fbo_status")
        		{
        			$("#divOtherFboStatus").show();
        		}
        		else
        		{
        			$("#divOtherFboStatus").hide();
        		}
        	});
    		
    		$('select[name="fbo_business"]').change(function(){
        		var id = $(this).val();
        		if(id == "other_business")
        		{
        			$("#divOtherBusiness").show();
        		}
        		else
        		{
        			$("#divOtherBusiness").hide();
        		}
        	});

    		$('select[name="fbo_firmtype"]').change(function(){
        		var id = $(this).val();
        		if(id == "other_firmtype")
        		{
        			$("#divOtherFirmType").show();
        		}
        		else
        		{
        			$("#divOtherFirmType").hide();
        		}
        	});

    		$('select[name="pack_type"]').change(function(){
        		var id = $(this).val();
        		if(id == "other_packtype")
        		{
        			$("#divOtherPackType").show();
        			$("#divPackOrPackFrom").hide();
        		}
        		else if(id == "1" || id == "2")
        		{
        			$("#divPackOrPackFrom").show();
        			$("#divOtherPackType").hide();
        		}
        		else
        		{
        			$("#divOtherPackType").hide();
        			$("#divPackOrPackFrom").hide();
        		}
        	});
    		
    		$('select[name="fbo_district"]').change(function(){
        		var id = $(this).val();
        		fnGetTalukaByDistrict(id);
        	});

    		$('select[name="food_category"]').change(function(){
        		var id = $(this).val();
        		fnSubCategoriesByFoodCategory(id);
        	});

    		/*$('input[name=reflab]').change(function(){
            	var value = $( 'input[name=reflab]:checked' ).val();
            	if(value=='1')
            	{
            		$("#divreflab").show();
            		$("#divfboacclab").hide();
            		$("#divacclab").hide();
                }
            	else
            	{
            		$("#divreflab").hide();
            		$("#divfboacclab").show();
            		$("input[name=lab][value=0]").prop('checked', true);
            		$("#divacclab").hide();
                }
            });*/

    		$('input[name=lab]').change(function(){
            	var value = $( 'input[name=lab]:checked' ).val();
            	if(value=='1')
            	{
            		$("#divacclab").show();
            	}
            	else
            	{
            		$("#divacclab").hide();
            	}
            });

    		$('input[name=seized]').change(function(){
            	var value = $( 'input[name=seized]:checked' ).val();
            	if(value=='1')
            	{
            		$("#divQuantity").show();
            		$("#divQuantityltr").show();
            		$("#divPrice").show();
                }
            	else
            	{
            		$("#divQuantity").hide();
            		$("#divQuantityltr").hide();
            		$("#divPrice").hide();
                }
            });

    		$.validator.addMethod("lettersonly", function (value, element) {
    			return this.optional(element) || /^[a-z\s]+$/i.test(value);
        	}, "Accept only letters and space.");

$.validator.addMethod("zeronotallow", function (value, element) {
    			if(value <= 0)
{
return false;
}
else{return true;}
        	}, "Please enter value greater then zero(0).");
 			
			$.validator.addMethod('greaterThan', function(value, element, param) {
				var pricevalue = $(param).val();
			      return this.optional(element) || parseInt(value) <= parseInt(pricevalue);
			}, 'Payment made to FBO can not be greater than price.');
			
    		$("#frmAdd").validate({
                rules:
                {
                	fso_name:{allenglish:true},
                	fbo_license_reg_no:{allenglish:true},
                	seized_quantity_type:{allenglish:true},
                	seized_quantity_type_ml_ltr:{allenglish:true},
                	created_date:{required: true,},
                	fbo_circle_id:{required: true,},
                	hour:{required: true,},
                	minute:{required: true,},
                	ampm:{required: true,},
                	fbo_name:{allenglish:true,required: true, maxlength: 250, lettersonly: true,},
                	firm_name:{allenglish:true,required: true,},
                	fbo_address1:{allenglish:true},
                	fbo_place:{allenglish:true},
                	fbo_status:{required: true,},
                	other_fbo_status:{	
                		allenglish:true,
                		required:
                		{
                			depends: function(element) 
                			{
                				if($('#fbo_status').val() == "other_fbo_status")
	                	        {
                	       			return true;
                	        	}
                	        	else
	                	        {
                	         		return false;
                	        	}
                			}
	                	},
	                	maxlength: 250,lettersonly: true,
                	},
                	fbo_address1:{allenglish:true,required: true, maxlength: 950,},
                	fbo_address2:{allenglish:true,maxlength: 950,},
                	fbo_district:{required: true,},
                	fbo_taluko:{required: true,},
                	fbo_place:{allenglish:true,required: true, maxlength: 950,},
                	fbo_pincode:{digits: true, minlength: 6, maxlength: 6,},
                	fbo_business:{required: true,},
                	other_business:{
                		allenglish:true,
                		required:
                		{
                			depends: function(element) 
                			{
                				if($('#fbo_business').val() == "other_business")
	                	        {
                	       			return true;
                	        	}
                	        	else
	                	        {
                	         		return false;
                	        	}
                			}
	                	},
	                	maxlength: 450,lettersonly: true,
                	},
                	fbo_firmtype:{required: true,},
                	other_firmtype:{
                		allenglish:true,
                		required:
                		{
                			depends: function(element) 
                			{
                				if($('#fbo_firmtype').val() == "other_firmtype")
	                	        {
                	       			return true;
                	        	}
                	        	else
	                	        {
                	         		return false;
                	        	}
                			}
	                	},
	                	maxlength: 250,lettersonly: true,
                	},
                	sample_name:{required: true, maxlength: 1950,},
                	short_name:{allenglish:true,required: true, maxlength: 950,},
                	sample_code:{allenglish:true,required: true,},
                	food_category:{required: true,},
                	food_subcategory:{required: true,},
                	manufacturer_address:{allenglish:true,required: true, maxlength: 950,},
                	pack_type:{required: true,},
                	other_packtype:{
                		allenglish:true,
                		required:
                		{
                			depends: function(element) 
                			{
                				if($('#pack_type').val() == "other_packtype")
	                	        {
                	       			return true;
                	        	}
                	        	else
	                	        {
                	         		return false;
                	        	}
                			}
	                	},
	                	maxlength: 250,lettersonly: true,
                	},
                	batch_no:{
                		allenglish:true,
                		required:
                		{
                			depends: function(element) 
                			{
                				if($('#pack_type').val() == "1" || $('#pack_type').val() == "2")
	                	        {
                	       			return true;
                	        	}
                	        	else
	                	        {
                	         		return false;
                	        	}
                			}
	                	},
	                	maxlength: 250,
                	},
                	packing_date:{
                		required:
                		{
                			depends: function(element) 
                			{
                				if($('#pack_type').val() == "1" || $('#pack_type').val() == "2")
	                	        {
                	       			return true;
                	        	}
                	        	else
	                	        {
                	         		return false;
                	        	}
                			}
	                	},
	                },
                	used_by_date:{
                		required:
                		{
                			depends: function(element) 
                			{
                				if($('#pack_type').val() == "1" || $('#pack_type').val() == "2")
	                	        {
                	       			return true;
                	        	}
                	        	else
	                	        {
                	         		return false;
                	        	}
                			}
	                	},
	                },
                	quantity:{required: true, digits: true, maxlength: 15,zeronotallow:true},
                	quantity_type:{allenglish:true,required: true,},
                	price:{required: true, number: true, maxlength: 15,zeronotallow:true},
                	price_type:{allenglish:true,required: true,},
                	payment_fbo:{required: true, number: true, maxlength: 15,zeronotallow:true},
                	receipt_no:{allenglish:true,maxlength: 450,},
                	investigation_type:{required: true,},
                	preservation_details:{allenglish:true,maxlength: 1950,},
                	disclosure:{allenglish:true,maxlength: 4950,},
                	accredited_lab:{
                		allenglish:true,
                		required:
                		{
                			depends: function(element) 
                			{
                				if($( 'input[name=lab]:checked' ).val() == 1)
	                	        {
                	       			return true;
                	        	}
                	        	else
	                	        {
                	         		return false;
                	        	}
                			}
	                	},
                    },
                	witness_name:{allenglish:true,maxlength: 450,  lettersonly: true,},
                	witness_address:{allenglish:true,maxlength: 950,},
                	/*seized_quantity:{
                		required:
                		{
                			depends: function(element) 
                			{
                				if($( 'input[name=seized]:checked' ).val() == 1)
	                	        {
                	       			return true;
                	        	}
                	        	else
	                	        {
                	         		return false;
                	        	}
                			}
	                	},
	                	digits: true, maxlength: 15,zeronotallow:true
                	},
                	seized_quantity_type:{
                		required:
                		{
                			depends: function(element) 
                			{
                				if($( 'input[name=seized]:checked' ).val() == 1)
	                	        {
                	       			return true;
                	        	}
                	        	else
	                	        {
                	         		return false;
                	        	}
                			}
	                	},
	                },*/
                	seized_price:{
                		required:
                		{
                			depends: function(element) 
                			{
                	        	if($( 'input[name=seized]:checked' ).val() == 1)
	                	        {
                	         		return true;
                	        	}
                	        	else
	                	        {
                	         		return false;
                	        	}
                			}
	                	},
	                	number: true, maxlength: 15,zeronotallow:true
                	},
                	image:{extension: "jpg|jpeg|png|gif|pdf|docx|doc",},
                },
                messages:
                {
					created_date:{required: 'Please select sample created date'},
					fbo_circle_id:{required: 'Please select Circle.',},
                	hour:{required: 'Please select hour.',},
                	minute:{required: 'Please select minute.',},
                	ampm:{required: 'Please select am or pm.',},
                	image:{extension: "Please upload only jpg, jpeg, gif, png, pdf, doc or docx files.",},
                },
                highlight: function(element) {
     	           //$(element).attr('class', 'filde error-border');
     	        }, unhighlight: function(element) {
     	           //$(element).removeClass('error-border');
     	        },
     			onfocusout: function(element) {
     				this.element(element);
     			},
     			submitHandler: function(form) {
     				var is_confirm =confirm('Please check all detail are correctly entered in this form. Details can not be edit after submit. Are you sure want to submit?');
					if(is_confirm)
					{
                    	form.submit();
					}
					else
					{
						return false;
					}
                }
	        });

    		function fnGetTalukaByDistrict(id)
    		{
    			if(id != ''){
    				$.ajax({
    		            type: "POST",
                        url: "<?php echo base_url() . "samples/getTalukaByDistrict";?>",
                        data: {
    		                district_id : id,
    		            },
    		            success: function(response) {
    		            	var obj = jQuery.parseJSON(response);
    		                 try {
    		                     if (obj['taluka_details'] != "") {
    		                    	 $('#fbo_taluko')
    		                    	     .find('option')
    		                    	     .remove()
    		                    	     .end()
    		                    	     .append('<option value="0" selected disabled>Select</option>'); 
    		                      	 $.each(obj['taluka_details'], function(idx, obj) {
    		                      		$("#fbo_taluko").append("<option value='"+obj.id+"'>"+obj.name+"</option>");
    		                      	 });
    		                     }
    		                     else
    		                     {
    		                    	 $('#fbo_taluko')
    		                	     .find('option')
    		                	     .remove()
    		                	     .end()
    		                	     .append('<option value="0" selected disabled>Select</option>');
    		                     }
    		                     $('#fbo_taluko').trigger("chosen:updated");
    		                 }
    		                 catch (e) {
    		                     alert('Exception while request..');
    		                 }
    		            }
    		        });
    	        
    	        }
    		}

        	function fnSubCategoriesByFoodCategory(id)
    		{
    			if(id != ''){
    				$.ajax({
    		            type: "POST",
                        url: "<?php echo base_url() . "samples/getSubcategoriesByCategory";?>",
                        data: {
    		                category_id : id,
    		            },
    		            success: function(response) {
    		            	var obj = jQuery.parseJSON(response);
    		                 try {
    		                     if (obj['subcategory_details'] != "") {
    		                    	 $('#food_subcategory').removeAttr("disabled", "disabled");
    		                    	 $('#food_subcategory')
    		                    	     .find('option')
    		                    	     .remove()
    		                    	     .end()
    		                    	     .append('<option value="0" selected disabled>Select</option>'); 
    		                      	 $.each(obj['subcategory_details'], function(idx, obj) {
    		                      		$("#food_subcategory").append("<option value='"+obj.id+"'>"+obj.category_no + " - " + obj.name+"</option>");
    		                      	 });
    		                     }
    		                     else
    		                     {
    		                    	 $('#food_subcategory')
    		                	     .find('option')
    		                	     .remove()
    		                	     .end()
    		                	     .append('<option value="0" selected disabled>Select</option>');

    		                	     $('#food_subcategory').chosen();
    		                     }
		                	    $('#food_subcategory').trigger("chosen:updated");

    		                 }
    		                 catch (e) {
    		                     alert('Exception while request..');
    		                 }
    		            }
    		        });
    	        
    	        }
    		}

        	function readURL(input) {
    			if (input.files && input.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function (e) {
                        $('#imgSelect').attr('src', e.target.result);
                    }

                    reader.readAsDataURL(input.files[0]);
                }
            }
    	});
    </script>
</body>

</html>