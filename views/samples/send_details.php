<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Sample Send Details</title>

    <?php include_once dirname(__DIR__).'/templates/include_css.php';?>

</head>

<body>

    <div id="wrapper">

    <?php include_once dirname(__DIR__).'/templates/sidebar.php'; ?>

        <div id="page-wrapper" class="gray-bg">
        <?php include_once dirname(__DIR__).'/templates/header.php'; ?>
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-sm-8">
                    <h2>Sample Send Details</h2>
                    <ol class="breadcrumb">
                        <li>
                            Home
                        </li>
                        <li class="active">
                            <strong>Sample Send Details</strong>
                        </li>
                    </ol>
                </div>
            </div>
        	<div class="wrapper wrapper-content animated fadeInRight">
				<div class="row">
	                <div class="col-lg-12">
	                    <div class="ibox">
	                        <div class="ibox-title">
	                            <h5>Add Sample Send Details</h5>
	                        </div>
	                        <div class="ibox-content">
	                        	<!-- <div class="alert alert-danger fade in error" id="error"> </div> -->
	                        	<?php 
				            		if(!empty($this->session->flashdata('mendatory_taluka_data'))):
				            	?>
				            	<div class="alert alert-danger fade in error" id="error"> 
				            		<?php 
				            			echo $this->session->flashdata('mendatory_taluka_data');
				            		?>
				            	</div>
				            	<?php 
				            		endif;
				            	?>
	                            <form id="frmAdd" action="<?php echo base_url()."samples/addsamplesenddetails";?>" class="form-horizontal" method="post" enctype="multipart/form-data">                      
	                                <fieldset>
	                                    <div class="row">
	                                        <div class="col-lg-6">
	                                        	<div class="form-group cls-form-group">
                                                	<label class="col-sm-4 control-label">Sample Name</label>
                                                	<div class="col-sm-8">
                                                		<input id="sample_name" name="sample_name" type="text" class="form-control" style="color: red; font-weight:bold;" value="<?php echo (!empty($sample_data->name)) ? $sample_data->name : '';?>" readonly>
                                                	</div>
                                            	</div>
                                            	<div class="form-group cls-form-group">
                                                	<label class="col-sm-4 control-label">Sample Code</label>
                                                	<div class="col-sm-8">
                                                		<input id="sample_code" name="sample_code" type="text" class="form-control" style="color: red; font-weight:bold;" value="<?php echo (!empty($sample_data->code)) ? $sample_data->code : '';?>" readonly>
                                                	</div>
                                            	</div>
	                                        	<div class="form-group cls-form-group">
													<label class="col-sm-4 control-label">Your Transportation Type</label>
													<div class="col-sm-8">
														<div class="control">
															<select id="transport" name="transport" class="chosen-select" >
                                                                <option disabled="disabled" selected="selected">Select</option>
                                                                <?php 
                                                                    if(!empty($transport_type_data)):
                                                                        foreach ($transport_type_data as $_transport_type_data):
                                                                            $selected = "";
                                                                            if($_transport_type_data->id == $sample_data->transport_type_id)
                                                                                    $selected = "selected='selected'";
                                                                ?>
                                                                            <option value="<?php echo $_transport_type_data->id;?>" <?php echo $selected;?>><?php echo $_transport_type_data->type;?></option>
                                                                <?php 
                                                                        endforeach;
                                                                    endif;
                                                                ?>
                                                            </select>
														</div>
													</div>
												</div>
												<div class="form-group cls-form-group" id="data_2">
													<label class="col-sm-4 control-label">Sample Dispatch Date <span class="clsRequiredFieldLable">*</span></label>
													<div class="col-sm-8">
														<div class="control">
															<div class="input-group date">
							                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input id="send_date" name="send_date" readonly="" type="text" class="form-control">
							                                </div>
														</div>
													</div>
												</div>
	                                        	<div class="form-group cls-form-group">
													<label class="col-sm-4 control-label">Slip Number / Messager Name</label>
													<div class="col-sm-8">
														<div class="control">
															<input id="slip_no" name="slip_no" type="text" class="form-control" maxlength="250">
														</div>
													</div>
												</div>
												<div class="form-group cls-form-group" id="data_1">
													<label class="col-sm-4 control-label">Slip Date </label>
													<div class="col-sm-8">
														<div class="control">
															<div class="input-group date">
							                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input id="slip_date" name="slip_date" readonly="" type="text" class="form-control">
							                                </div>
														</div>
													</div>
												</div>
												
												<?php if(!empty($is_upload_slip)):?>
												<div class="form-group cls-form-group">
													<label class="col-sm-4 control-label">Upload Scan Copy of Slip </label>
													<div class="col-sm-8">
														<div class="control">
															<input id="image" name="image" type="file">
														</div>
													</div>
												</div>
												<div class="form-group cls-form-group">
	                                                <label class="col-sm-4 control-label"></label>
	                                                <div class="col-sm-8">
														<div class="control">
															<img id="imgMemberSelect" src="<?php echo base_url().'files/transport_slips/thumb/default.png';?>" alt="" height="100px" width="100px"/>
														</div>
													</div>
	                                            </div>
	                                            <?php endif;?>
	                                        </div>
                                    	</div>
	                                    <div class="clsSubmitButtons">
	                                    	<input type="hidden" id="hdn_id" name="hdn_id" value="<?php echo $sample_data->unique_code;?>">
	                                    	<input type="hidden" id="hdn_sample_code" name="hdn_sample_code" value="<?php echo $sample_data->code;?>">
	                                    	<input type="hidden" id="hdn_lab_id" name="hdn_lab_id" value="<?php echo $sample_data->lab_id;?>">
	                                    	<input type="hidden" id="hdn_circle_id" name="hdn_circle_id" value="<?php echo $sample_data->circle_id;?>">
	                                    	<button id="btnAdd" class="btn btn-sm btn-success" name="btnAdd" type="submit">Submit</button>
											<a class="btn btn-white" href="javascript:history.back()">Cancel</a>
										</div>
	                                </fieldset>
								</form>
	                        </div>
	                    </div>
					</div>
	        	</div>
			</div>
        <?php include_once dirname(__DIR__).'/templates/footer.php'; ?>

        </div>
        </div>

    <?php include_once dirname(__DIR__).'/templates/include_js.php'; ?>

    <script>
    	$(document).ready(function(){
    		var nowDate = new Date();
    		var dd = nowDate.getDate();
    	    var mm = nowDate.getMonth()+1; //January is 0!

    	    var yyyy = nowDate.getFullYear();
    	    if(dd<10){
    	        dd='0'+dd
    	    } 
    	    if(mm<10){
    	        mm='0'+mm
    	    } 
    	    var today = dd+'/'+mm+'/'+yyyy;
			//var today = new Date(nowDate.getFullYear(), nowDate.getMonth(), nowDate.getDate(), 0, 0, 0, 0);
			// $('#slip_date').val(today);
        	$('#data_1 .input-group.date').datepicker({
        		format: "dd/mm/yyyy",
        		endDate: today,
        		clearBtn: true,
                keyboardNavigation: false,
                forceParse: false,
                todayHighlight: true,
                autoclose: true
            });

        	// $('#send_date').val(today);
        	$('#data_2 .input-group.date').datepicker({
        		format: "dd/mm/yyyy",
        		endDate: today,
        		clearBtn: true,
                keyboardNavigation: false,
                forceParse: false,
                todayHighlight: true,
                autoclose: true
            });

        	$("#image").change(function(){
                readURL(this);
            });
            
    		$.validator.addMethod("lettersonly", function (value, element) {
    			return this.optional(element) || /^[a-z]+$/i.test(value);
        	}, "Please enter only letters.");

    		$.validator.addMethod("lettersandspaceonly", function(value, element) {
    			return this.optional(element) || /^[a-z\s]+$/i.test(value);
    		}, "Accept only Letters and Space");
			
    		$.validator.addMethod("lettersnumbersonly", function(value, element) {
    	        return this.optional(element) || /^[a-z0-9]+$/i.test(value);
    	    }, "Accept only letters and numbers.");

    		$("#frmAdd").validate({
                rules:
                {
                	send_date:{required: true},
                },
                messages:
                {
                	send_date:{required: "Please select sample dispatch date.",},
                },
                highlight: function(element) {
     	           //$(element).attr('class', 'filde error-border');
     	        }, unhighlight: function(element) {
     	           //$(element).removeClass('error-border');
     	        },
     			onfocusout: function(element) {
     				this.element(element);
     			},
     			submitHandler: function(form) {
     				var is_confirm =confirm('Please check all detail are correctly entered in this form. Details can not be edit after submit. Are you sure want to submit?');
					if(is_confirm)
					{
                    	form.submit();
					}
					else
					{
						return false;
					}
                }
	        });

    	});

    	function readURL(input) {
			if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#imgMemberSelect').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>
</body>

</html>