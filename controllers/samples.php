<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Samples extends MY_Controller
{
	function __construct()
    {
   		parent::__construct();
		$this->checkSession();
		$this->load->model('models/samplei_model');
		$this->load->model('models/samplei_media_model');
		$this->load->model('models/samplei_edit_request_model');

   		$this->load->model('sample_model', 'm_sample', true);
   		$this->load->model('circle_model', 'm_circle', true);
   		$this->load->model('fbo_model', 'm_fbo', true);
   		$this->load->model('fbo_status_model', 'm_fbo_status', true);
   		$this->load->model('district_model', 'm_district', true);
   		$this->load->model('taluka_model', 'm_taluka', true);
   		$this->load->model('firm_type_model', 'm_firm_type', true);
   		$this->load->model('kind_of_business_model', 'm_kind_of_business', true);
   		$this->load->model('food_category_model', 'm_food_category', true);
   		$this->load->model('pack_type_model', 'm_pack_type', true);
   		$this->load->model('investigation_type_model', 'm_investigation_type', true);
   		$this->load->model('transport_type_model', 'm_transport_type', true);
   		$this->load->model('lab_model', 'm_lab', true);
   		$this->load->model('user_model', 'm_user', true);
   		$this->load->model('sample_edit_request_model', 'm_sample_edit_request', true);
   		$this->load->model('sample_media_model', 'm_sample_media', true);
   		$this->load->model('accredited_lab_model', 'm_accredited_lab', true);
   		$this->load->model('referral_lab_model', 'm_referral_lab', true);
   		$this->load->model('util_model','m_util',true);
   		ini_set('max_execution_time', 0);
   		ini_set('memory_limit', '-1');
		set_time_limit(0);
 	}
 	function updateuuid(){
 		$this->m_sample->generate_unique_code();
 	}
	function index(){
		$user_data = $this->session->userdata('user_data');
		$page = !empty($this->input->get('per_page')) ? trim($this->input->get('per_page')) : 1;
		$data['search'] = $_POST['search'] = !empty($this->input->get('search')) ? trim($this->input->get('search')) : '';
		$data['year'] = $_POST['search_year'] = !empty($this->input->get('year')) ? trim($this->input->get('year')) : '';
		$data['current_year'] = $this->utils->get_year();
		$_POST['is_send'] = 0;
		$data['sample_data'] = $this->samplei_model->GetData(PAGE_SIZE,--$page);
		if(@$data['sample_data'][0]->Error_Message){
			$total_records =  0;
			$data['sample_data'] = array();
		}else{
			$total_records =  @$data['sample_data'][0]->rowcount;
		}
		$limit_per_page = PAGE_SIZE;
		$start_index = !empty($page) ? ($page) * $limit_per_page : 0;
		$search_parameter = "";
		$data['base_url_for_search'] = site_url().'samples?';
		if($total_records > 0){
			if($data['search'] != ""){
				$search_parameter .= "&search=".$data['search'];
			}
			if($data['year'] != ""){
				$search_parameter .= "&year=".$data['year'];
			}
			$data['base_url'] = site_url().'samples?'.@$search_parameter;
			$data['links'] = $this->m_util->GetPagination($data['base_url'],$total_records,$limit_per_page);
			$data['start_index'] = $start_index;
			$data['total_records'] = $total_records;
		}
		$this->load->view('samples/index',$data);
	}

	function delete($delete_unique_code = 0){
		$data['sample_data'] = $this->samplei_model->GetDataByID($delete_unique_code);
		if(!@$data['sample_data']->Error_Message && @$data['sample_data']->is_send == 0){
			$where = "unique_code = '$delete_unique_code'";
			$list['status']=0;
			$list['del_flag']=1;
			$this->m_sample->edit($where,$list);
			$where = "sample_id = " .$data['sample_data']->id;
			$this->m_sample_edit_request->edit($where,$list);
			redirect(site_url('samples'));
		}else{
			show_404();
		}
	}
	
	public function archives(){
		$user_data = $this->session->userdata('user_data');
		$page = !empty($this->input->get('per_page')) ? trim($this->input->get('per_page')) : 1;
		$data['search'] = $_POST['search'] = !empty($this->input->get('search')) ? trim($this->input->get('search')) : '';
		$data['year'] = $year = !empty($this->input->get('year')) ? trim($this->input->get('year')) : '';
		$data['current_year'] = $this->utils->get_year();
		$_POST['is_send'] = 1;
		$_POST['search_year'] = $year;
		
		$data['sample_data'] = $this->samplei_model->GetData(PAGE_SIZE,--$page);
		if(@$data['sample_data'][0]->Error_Message){
			$total_records =  0;
			$data['sample_data'] = array();
		}else{
			$total_records =  @$data['sample_data'][0]->rowcount;
		}
		$config = array();
		$limit_per_page = PAGE_SIZE;
		$start_index = !empty($page) ? ($page) * $limit_per_page : 0;
		$search_parameter = "";
		$data['base_url_for_search'] = site_url().'samples/archives?';
		if($total_records > 0){
			if($data['search'] != ""){
				$search_parameter .= "&search=".$data['search'];
			}
			if($data['year'] != ""){
				$search_parameter .= "&year=".$data['year'];
			}
			$data['base_url'] = site_url().'samples/archives?'.@$search_parameter;
			$data['links'] = $this->m_util->GetPagination($data['base_url'],$total_records,$limit_per_page);
			$data['start_index'] = $start_index;
			$data['total_records'] = $total_records;
			
		}
		$this->load->view('samples/archives',$data);
	}
	
	public function add(){
		$data['user_data'] = $this->session->userdata('user_data');
		$data['current_year'] = $this->utils->get_year();
		$where = "status = 1 and del_flag = 0";
		$wherein = explode(',',$data['user_data']->circle_id);
		if(!empty($wherein) && in_array(GANDHINAGAR_CIRCLE_ID, $wherein)){
			$wherein = NULL;
		}
		$data['circle_detail'] = $this->m_circle->getAllCirclesWhereIdIn($where,$wherein);
		$data['circle_count'] = count($data['circle_detail']);
		if($data['circle_count'] == 1){
			$data['circle_detail'] = $data['circle_detail'][0];
		}
		$where = "created_by = '".$data['user_data']->id."'";
		$orderby = "id desc";
		$sample_detail = $this->m_sample->getFsoLastSample($where, $orderby);
		
		if(!empty($sample_detail)){
			$sample_no = $sample_detail->sample_no;
			if(!empty($sample_no)){
				$sample_no += 1;
			}else{
				$sample_no = 1;
			}
		}else{
			$sample_no = 1;
		}
		
		$data['sample_no'] = $sample_no;
		
		// $data['fso_district_id'] = $data['circle_detail']->district_id;
		
		// $where = "district_id = '".$data['circle_detail']->district_id."' and status = 1 and del_flag = 0";
		// $orderby = "name asc";
		// $data['taluka_details'] = $this->m_taluka->getAllTalukas($where,$orderby);
		
		$where = "flag = 1 and del_flag = 0";
		$orderby = "status asc";
		$data['fbo_status_details'] = $this->m_fbo_status->getAllFboStatus($where,$orderby);
		
		$where = "status = 1 and del_flag = 0";
		$orderby = "name asc";
		$data['district_details'] = $this->m_district->getAllDistricts($where,$orderby);
		
		$where = "status = 1 and del_flag = 0";
		$orderby = "type asc";
		$data['firm_type_details'] = $this->m_firm_type->getAllFirmTypes($where,$orderby);
		
		$where = "status = 1 and del_flag = 0";
		$orderby = "business asc";
		$data['kind_of_business_details'] = $this->m_kind_of_business->getAllKindOfBusinesses($where,$orderby);
		
		$where = "parent_id = 0 and status = 1 and del_flag = 0";
		$orderby = "name asc";
		$data['food_categories_details'] = $this->m_food_category->getAllFoodCategories($where,$orderby);
		
		$where = "status = 1 and del_flag = 0";
		$orderby = "id asc";
		$data['pack_type_details'] = $this->m_pack_type->getAllPackTypes($where,$orderby);
		
		$where = "status = 1 and del_flag = 0";
		$orderby = "id asc";
		$data['investigation_type_details'] = $this->m_investigation_type->getAllInvestigationTypes($where,$orderby);
		
		$where = "status = 1 and del_flag = 0";
		$orderby = "id asc";
		$data['lab_details'] = $this->m_lab->getAllLab($where,$orderby);
		
		$where = "status = 1 and del_flag = 0";
		$orderby = "id asc";
		$data['referral_lab_details'] = $this->m_referral_lab->getAllReferralLab($where,$orderby);
		
		$where = "status = 1 and del_flag = 0";
		$orderby = "id asc";
		$data['accredited_lab_details'] = $this->m_accredited_lab->getAllAccreditedLab($where,$orderby);
		
		/*$sample_code_details = $this->get_sample_code_details();
		if(!empty($sample_code_details))
		{
			$data['sample_no'] = $sample_code_details['sample_no'];
			$data['sample_code'] = $sample_code_details['sample_code'];
		}*/
		
		$this->load->view('samples/create',$data);
	}
	
	public function create(){
		if(
			!empty($this->input->post('created_date')) && 
			!empty($this->input->post('hour')) && 
			!empty($this->input->post('minute')) && 
			!empty($this->input->post('ampm')) && 
			!empty($this->input->post('fbo_circle_id')) && 
			!empty($this->input->post('fbo_name')) && 
			!empty($this->input->post('firm_name')) && 
			!empty($this->input->post('fbo_status')) && 
			!empty($this->input->post('fbo_address1')) && 
			!empty($this->input->post('fbo_district')) && 
			!empty($this->input->post('fbo_taluko')) && 
			!empty($this->input->post('fbo_place')) && 
			!empty($this->input->post('fbo_business')) && 
			!empty($this->input->post('fbo_firmtype')) && 
			!empty($this->input->post('sample_name')) && 
			!empty($this->input->post('short_name')) && 
			!empty($this->input->post('sample_code')) && 
			!empty($this->input->post('food_category')) && 
			!empty($this->input->post('manufacturer_address')) && 
			!empty($this->input->post('pack_type')) && 
			!empty($this->input->post('quantity')) && 
			!empty($this->input->post('quantity_type')) && 
			!empty($this->input->post('price')) && 
			!empty($this->input->post('price_type')) && 
			!empty($this->input->post('payment_fbo')) && 
			!empty($this->input->post('investigation_type')) && 
			!empty($this->input->post('lab_id')))
		{
			$user_data = $this->session->userdata('user_data');
			$created_date = $this->utils->get_date_time();
			$current_year = $this->utils->get_year();
			
			// $where = "id = '".$user_data->circle_id."' and status = 1 and del_flag = 0";
			// $circle_detail = $this->m_circle->getCircle($where);
			
			$sample_created_date = trim($this->input->post('created_date'));
			$sample_created_date_array = explode('/',$sample_created_date);
			$sample_created_date = date('Y-m-d',mktime(0,0,0,$sample_created_date_array[1],$sample_created_date_array[0],$sample_created_date_array[2]));
			$sample_created_time = trim($this->input->post('hour')).':'.trim($this->input->post('minute')).' '.trim($this->input->post('ampm'));
			
			$list_fbo['name'] = trim($this->input->post('fbo_name'));
			$list_fbo['firm_name'] = trim($this->input->post('firm_name'));
			$fbo_status_id = trim($this->input->post('fbo_status'));
			if($fbo_status_id == "other_fbo_status")
			{
				if(!empty($this->input->post('other_fbo_status')))
				{
					$fbo_status = trim($this->input->post('other_fbo_status'));
					$list_fbo_status['status'] = $fbo_status;
					$list_fbo_status['created_by'] = $user_data->id;
					$list_fbo_status['created_date'] = $created_date;
					$insert_fbo_status_id = $this->m_fbo_status->insert($list_fbo_status);
					$list_fbo['status_id'] = $insert_fbo_status_id;
				}
			}
			else
			{
				$list_fbo['status_id'] = $fbo_status_id;
			}
			$list_fbo['address_line1'] = trim($this->input->post('fbo_address1'));
			$list_fbo['address_line2'] = !empty($this->input->post('fbo_address2')) ? trim($this->input->post('fbo_address2')) : '';
			$list_fbo['district_id'] = !empty($this->input->post('fbo_district')) ? trim($this->input->post('fbo_district')) : '';
			$list_fbo['taluka_id'] = !empty($this->input->post('fbo_taluko')) ? trim($this->input->post('fbo_taluko')) : '';
			$list_fbo['place'] = !empty($this->input->post('fbo_place')) ? trim($this->input->post('fbo_place')) : '';
			$list_fbo['pincode'] = !empty($this->input->post('fbo_pincode')) ? trim($this->input->post('fbo_pincode')) : '';
			$firm_type_id = trim($this->input->post('fbo_firmtype'));
			if($firm_type_id == "other_firmtype")
			{
				if(!empty($this->input->post('other_firmtype')))
				{
					$firm_type = trim($this->input->post('other_firmtype'));
					$list_firm_type['type'] = $firm_type;
					$list_firm_type['created_by'] = $user_data->id;
					$list_firm_type['created_date'] = $created_date;
					$insert_firm_type_id = $this->m_firm_type->insert($list_firm_type);
					$list_fbo['firm_type_id'] = $insert_firm_type_id;
				}
			}
			else 
			{
				$list_fbo['firm_type_id'] = $firm_type_id;
			}
			$kind_of_business_id = trim($this->input->post('fbo_business'));
			if($kind_of_business_id == "other_business")
			{
				if(!empty($this->input->post('other_business')))
				{
					$kind_of_business = trim($this->input->post('other_business'));
					$list_kind_of_business['business'] = $kind_of_business;
					$list_kind_of_business['created_by'] = $user_data->id;
					$list_kind_of_business['created_date'] = $created_date;
					$insert_kind_of_business_id = $this->m_kind_of_business->insert($list_kind_of_business);
					$list_fbo['kind_of_business_id'] = $insert_kind_of_business_id;
				}
			}
			else 
			{
				$list_fbo['kind_of_business_id'] = trim($this->input->post('fbo_business'));
			}
			$list_fbo['license_reg_no'] = !empty($this->input->post('fbo_license_reg_no')) ? trim($this->input->post('fbo_license_reg_no')) : '';
			
			//$list_sample['sample_no'] = !empty($this->input->post('hdn_sample_no')) ? trim($this->input->post('hdn_sample_no')) : '';
			//$list_sample['sample_no'] = trim($this->input->post('sample_code'));
			$list_sample['name'] = trim($this->input->post('sample_name'));
			$list_sample['short_name'] = trim($this->input->post('short_name'));
			$list_sample['code'] = trim($this->input->post('sample_code'));
			//$list_sample['code'] = !empty($this->input->post('hdn_sample_code')) ? trim($this->input->post('hdn_sample_code')) : '';
			//$list_sample['code'] = $circle_detail->circle_id.$circle_detail->short_name.$user_data->fso_id."/".$list_sample['sample_no']."/".$current_year;
			$list_sample['category_id'] = !empty($this->input->post('food_subcategory')) ? trim($this->input->post('food_subcategory')) : trim($this->input->post('food_category'));
			$list_sample['manufacturer_address'] = trim($this->input->post('manufacturer_address'));
			$pack_id = trim($this->input->post('pack_type'));
			if($pack_id == "other_packtype")
			{
				if(!empty($this->input->post('other_packtype')))
				{
					$pack_type = trim($this->input->post('other_packtype'));
					$list_pack_type['type'] = $pack_type;
					$list_pack_type['created_by'] = $user_data->id;
					$list_pack_type['created_date'] = $created_date;
					$insert_pack_type_id = $this->m_pack_type->insert($list_pack_type);
					$list_sample['pack_id'] = $insert_pack_type_id;
				}
			}
			else 
			{
				$list_sample['pack_id'] = trim($this->input->post('pack_type'));
			}
			
			if($pack_id == "1" || $pack_id == "2")
			{
				$list_sample['batch_no'] = !empty($this->input->post('batch_no')) ? trim($this->input->post('batch_no')) : null;
				$list_sample['packing_date'] = !empty($this->input->post('packing_date')) ? trim($this->input->post('packing_date')) : null;
				$list_sample['used_by_date'] = !empty($this->input->post('used_by_date')) ? trim($this->input->post('used_by_date')) : null;
				/*$packing_date = trim($this->input->post('packing_date'));
				$packing_date_array = explode('/',$packing_date);
				$packing_date = date('Y-m-d',mktime(0,0,0,$packing_date_array[1],$packing_date_array[0],$packing_date_array[2]));
				
				$used_by_date = trim($this->input->post('used_by_date'));
				$used_by_date_array = explode('/',$used_by_date);
				$used_by_date = date('Y-m-d',mktime(0,0,0,$used_by_date_array[1],$used_by_date_array[0],$used_by_date_array[2]));
				
				$list_sample['packing_date'] = $packing_date;
				$list_sample['used_by_date'] = $used_by_date;*/
			}
			else
			{
				$list_sample['batch_no'] = null;
				$list_sample['packing_date'] = null;
				$list_sample['used_by_date'] = null;
			}
			
			$list_sample['quantity'] = trim($this->input->post('quantity'));
			$list_sample['quantity_type'] = trim($this->input->post('quantity_type'));
			$list_sample['price'] = trim($this->input->post('price'));
			$list_sample['price_type'] = trim($this->input->post('price_type'));
			$list_sample['payment_to_fbo'] = trim($this->input->post('payment_fbo'));
			$list_sample['receipt_no'] = !empty($this->input->post('receipt_no')) ? trim($this->input->post('receipt_no')) : '';
			$list_sample['investigation_id'] = trim($this->input->post('investigation_type'));
			$list_sample['preservation_added'] = !empty($this->input->post('preservation_details')) ? trim($this->input->post('preservation_details')) : '';
			$list_sample['disclosure'] = !empty($this->input->post('disclosure')) ? trim($this->input->post('disclosure')) : '';
			if(!empty($this->input->post('reflab')))
			{
				$list_sample['is_demand_referral_lab'] = trim($this->input->post('reflab'));
				$list_sample['referral_lab_id'] = !empty($this->input->post('referral_lab')) ? trim($this->input->post('referral_lab')) : 0;
			}
			else
			{
				$list_sample['is_demand_referral_lab'] = 0;
				$list_sample['referral_lab_id'] = 0;
			}
				
			if(!empty($this->input->post('lab')))
			{
				$list_sample['is_demand_accredited_lab'] = trim($this->input->post('lab'));
				$list_sample['accredited_lab_id'] = !empty($this->input->post('accredited_lab')) ? trim($this->input->post('accredited_lab')) : 0;
			}
			else
			{
				$list_sample['is_demand_accredited_lab'] = 0;
				$list_sample['accredited_lab_id'] = 0;
			}
			/*$list_sample['is_demand_referral_lab'] = trim($this->input->post('reflab'));
			if(!empty($this->input->post('reflab')))
			{
				if(!empty($this->input->post('referral_lab')))
				{
					$list_sample['referral_lab_id'] = trim($this->input->post('referral_lab'));
				}
			}
			$list_sample['is_demand_accredited_lab'] = trim($this->input->post('lab'));
			if(!empty($this->input->post('lab')))
			{
				if(!empty($this->input->post('accredited_lab')))
				{
					$list_sample['accredited_lab_id'] = trim($this->input->post('accredited_lab'));
				}
			}*/
			$list_sample['witness_name'] = !empty($this->input->post('witness_name')) ? trim($this->input->post('witness_name')) : '';
			$list_sample['witness_address'] = !empty($this->input->post('witness_address')) ? trim($this->input->post('witness_address')) : '';
			$list_sample['is_seizzer'] = trim($this->input->post('seized'));
			if(!empty($this->input->post('seized')))
			{
				if(!empty($this->input->post('seized_quantity')))
				{
					$list_sample['seizzer_quantity'] = trim($this->input->post('seized_quantity'));
				}
				else 
				{
					$list_sample['seizzer_quantity'] = 0;
				}
				
				if(!empty($this->input->post('seized_quantity_type')))
				{
					$list_sample['seizzer_quantity_type'] = trim($this->input->post('seized_quantity_type'));
				}
				
				if(!empty($this->input->post('seized_quantity_ml_ltr')))
				{
					$list_sample['seizzer_quantity_ml_ltr'] = trim($this->input->post('seized_quantity_ml_ltr'));
				}
				else 
				{
					$list_sample['seizzer_quantity_ml_ltr'] = 0;
				}
				
				if(!empty($this->input->post('seized_quantity_type_ml_ltr')))
				{
					$list_sample['seizzer_quantity_type_ml_ltr'] = trim($this->input->post('seized_quantity_type_ml_ltr'));
				}
				
				if(!empty($this->input->post('seized_price')))
				{
					$list_sample['seizzer_price'] = trim($this->input->post('seized_price'));
				}
			}
			else
			{
				$list_sample['seizzer_quantity'] = 0;
				$list_sample['seizzer_quantity_type'] = null;
				$list_sample['seizzer_quantity_ml_ltr'] = 0;
				$list_sample['seizzer_quantity_type_ml_ltr'] = null;
				$list_sample['seizzer_price'] = 0;
			}
			
			/*$sample_code_details = $this->get_sample_code_details();
			if(!empty($sample_code_details))
			{
				if($sample_code_details['sample_no'] != $list_sample['sample_no'])
				{
					$list_sample['sample_no'] = $sample_code_details['sample_no'];
					$list_sample['sample_code'] = $sample_code_details['sample_code'];
				}
			}*/
			
			//$where = "id = '".$user_data->circle_id."' and status = 1 and del_flag = 0";
			//$circle_detail = $this->m_circle->getCircle($where);
			/*if(!empty($circle_detail->lab_id))
			{
				$list_sample['lab_id'] = $circle_detail->lab_id;
			}*/
			$list_sample['lab_id'] = trim($this->input->post('lab_id'));
			$list_sample['created_by'] = $user_data->id;
			$list_sample['sample_created_date'] = $sample_created_date;
			$list_sample['sample_created_time'] = $sample_created_time;
			$list_sample['created_date'] = $created_date;
			$list_sample['circle_id'] = $this->input->post('fbo_circle_id');
			//print_r($list_sample);
			//print_r($list_fbo);exit;
			$sample_id = $this->m_sample->insert($list_sample);
			
			if(!empty($sample_id))
			{
				$list_fbo['sample_id'] = $sample_id;
				$list_fbo['created_by'] = $user_data->id;
				$list_fbo['created_date'] = $created_date;
				
				$fbo_id = $this->m_fbo->insert($list_fbo);
				
				$where = "id = '".$sample_id."'";
				$sample_detail = $this->m_sample->getSample($where);
				
				if(!empty($_FILES['image']['name']))
				{
					$sample_code = str_replace('/', '_', $sample_detail->code);
					$prefix = 'sample_'.$sample_code.'_'.time().rand(1000000,9999999);
					$result=$this->utils->image_upload($_FILES['image'],'files/sample_images/',$prefix);
					if($result['error']== 0)
					{
						$file_name = $result['file_name'];
						$this->m_util->generate_thumb('files/sample_images/'.$file_name, 'files/sample_images/'.$file_name, 0, 0);
						$this->m_util->generate_thumb('files/sample_images/'.$file_name, 'files/sample_images/thumb/'.$file_name, 400, 0);
						
						$list_sample_media['sample_id'] = $sample_id;
						$list_sample_media['image'] = $file_name;
						$list_sample_media['thumb'] = $file_name;
						$list_sample_media['created_by'] = $user_data->id;
						$list_sample_media['created_date'] = $created_date;
						
						$sample_media_id = $this->m_sample_media->insert($list_sample_media);
					}
				}
				
				/*if(!empty($fbo_id))
				{
					// Send Email To User Start
					$subject= "FDCA - New Sample Creation";
					//$to=$list_user['email'];
					$message = file_get_contents(base_url().'files/assets/email_templates/createNewSample.php');
					$message = str_replace('%BasePath%', base_url(), $message);
					$message = str_replace('%samplename%', $sample_detail->name, $message);
					$message = str_replace('%shortname%', $sample_detail->short_name, $message);
					$message = str_replace('%samplecode%', $sample_detail->code, $message);
					$where = "id = '".$sample_detail->category_id."' and status = 1 and del_flag = 0";
					$food_categories_detail = $this->m_food_category->getFoodCategory($where);
					$message = str_replace('%foodcategory%', $food_categories_detail->name, $message);
					$message = str_replace('%fboname%', $list_fbo['name'], $message);
					$message = str_replace('%fsoname%', $user_data->full_name, $message);
					//$where = "id = '".$user_data->circle_id."' and status = 1 and del_flag = 0";
					//$circle_detail = $this->m_circle->getCircle($where);
					$message = str_replace('%circlename%', $circle_detail->name, $message);
					$message = str_replace('%createddate%', $this->m_util->date_format($sample_detail->sample_created_date), $message);
					
					$emails_to_send = array();
					array_push($emails_to_send, $user_data->email);
					$do_details = $this->m_user->getDoUsersFromCircleId($user_data->circle_id);
					if(!empty($do_details))
					{
						foreach ($do_details as $_do_details)
						{
							array_push($emails_to_send, $_do_details->email);
						}
					}
					
					if(!empty($emails_to_send))
					{
						foreach ($emails_to_send as $_emails_to_send)
						{
							$to = $_emails_to_send;
							$this->m_util->email($to,$subject,$message);
						}
					}
					// Send Email To User End
				}*/
			}
			
			$url= base_url().'samples';
			redirect($url,"refresh");
		}
		else 
		{
			$this->session->set_flashdata('mendatory_sample_fbo_data', SAMPLE_FBO_REQUIRE_FIELDS);
			$url= base_url().'samples/add';
			redirect($url,"refresh");
		}
	}
	
	public function edit($sample_unique_code){
		$data['sample_data'] = $this->samplei_model->GetDataByID($sample_unique_code);
		if(!@$data['sample_data']->Error_Message && @$data['sample_data']->edit_request_flag == 0 && @$data['sample_data']->is_offline == 0){
			$data['user_data'] = $this->session->userdata('user_data');
			$where = "status = 1 and del_flag = 0";
			$wherein = explode(',',$data['user_data']->circle_id);
			if(!empty($wherein) && in_array(GANDHINAGAR_CIRCLE_ID, $wherein)){
				$wherein = NULL;
			}
			if(!empty($wherein) && !in_array($data['sample_data']->circle_id,$wherein)){
				$where .= " and id = ".$data['sample_data']->circle_id;
				$wherein = array();
			}

			$data['circle_detail'] = $this->m_circle->getAllCirclesWhereIdIn($where,$wherein);
			$data['circle_count'] = count($data['circle_detail']);
			if($data['circle_count'] == 1){
				$data['circle_detail'] = $data['circle_detail'][0];
			}

			$sample_id = $data['sample_data']->id;
			$data['current_year'] = $this->utils->get_year();
			
			$where = "status = 1 and del_flag = 0";
			$orderby = "business asc";
			$data['kind_of_business_details'] = $this->m_kind_of_business->getAllKindOfBusinesses($where,$orderby);
			
			$where = "id = '".$data['sample_data']->fbo_firm_type_id."' and status = 1 and del_flag = 0";
			$data['fbo_firm_type_data'] = $this->m_firm_type->getFirmType($where);
			
			$where = "id = '".$data['sample_data']->category_id."' and status = 1 and del_flag = 0";
			$data['sample_food_category_data'] = $this->m_food_category->getFoodCategory($where);
			
			$where = "status = 1 and del_flag = 0";
			$orderby = "id asc";
			$data['pack_type_details'] = $this->m_pack_type->getAllPackTypes($where,$orderby);
			
			$where = "status = 1 and del_flag = 0";
			$orderby = "id asc";
			$data['lab_details'] = $this->m_lab->getAllLab($where,$orderby);
			
			$where = "status = 1 and del_flag = 0";
			$orderby = "id asc";
			$data['referral_lab_details'] = $this->m_referral_lab->getAllReferralLab($where,$orderby);
			
			$where = "status = 1 and del_flag = 0";
			$orderby = "id asc";
			$data['accredited_lab_details'] = $this->m_accredited_lab->getAllAccreditedLab($where,$orderby);
			$this->load->view('samples/edit',$data);
		}else{
			show_404();
		}
	}
	
	public function update(){
		
		if(
			!empty($this->input->post('hdn_fbo_id')) &&
			!empty($this->input->post('fbo_circle_id')) &&
			!empty($this->input->post('hdn_sample_id')) &&
			!empty($this->input->post('fbo_address1')) &&
			!empty($this->input->post('fbo_business')) &&
			!empty($this->input->post('short_name')) &&
			!empty($this->input->post('sample_code')) && 
			!empty($this->input->post('manufacturer_address')) && 
			!empty($this->input->post('pack_type')) && 
			!empty($this->input->post('lab_id')))
		{
			$user_data = $this->session->userdata('user_data');
			$updated_date = $this->utils->get_date_time();
			$created_date = $this->utils->get_date_time();
			$current_year = $this->utils->get_year();
			
			// $where = "id = '".$user_data->circle_id."' and status = 1 and del_flag = 0";
			// $circle_detail = $this->m_circle->getCircle($where);
			/*$sample_created_date = trim($this->input->post('created_date'));
			$sample_created_date_array = explode('/',$sample_created_date);
			$sample_created_date = date('Y-m-d',mktime(0,0,0,$sample_created_date_array[1],$sample_created_date_array[0],$sample_created_date_array[2]));*/
			
			$fbo_id = trim($this->input->post('hdn_fbo_id'));
			$sample_id = trim($this->input->post('hdn_sample_id'));
			
			//$list_sample['sample_no'] = trim($this->input->post('sample_code'));
			$list_sample['short_name'] = trim($this->input->post('short_name'));
			$list_sample['code'] = trim($this->input->post('sample_code'));
			//$list_sample['code'] = $circle_detail->circle_id.$circle_detail->short_name.$user_data->fso_id."/".$list_sample['sample_no']."/".$current_year;
			$list_sample['manufacturer_address'] = trim($this->input->post('manufacturer_address'));
			//$list_sample['pack_id'] = trim($this->input->post('pack_type'));
			$pack_id = trim($this->input->post('pack_type'));
			if($pack_id == "other_packtype")
			{
				if(!empty($this->input->post('other_packtype')))
				{
					$pack_type = trim($this->input->post('other_packtype'));
					$list_pack_type['type'] = $pack_type;
					$list_pack_type['created_by'] = $user_data->id;
					$list_pack_type['created_date'] = $created_date;
					$insert_pack_type_id = $this->m_pack_type->insert($list_pack_type);
					$list_sample['pack_id'] = $insert_pack_type_id;
				}
			}
			else
			{
				$list_sample['pack_id'] = trim($this->input->post('pack_type'));
			}
			
			if($pack_id == "1" || $pack_id == "2")
			{
				$list_sample['batch_no'] = !empty($this->input->post('batch_no')) ? trim($this->input->post('batch_no')) : null;
				$list_sample['packing_date'] = !empty($this->input->post('packing_date')) ? trim($this->input->post('packing_date')) : null;
				$list_sample['used_by_date'] = !empty($this->input->post('used_by_date')) ? trim($this->input->post('used_by_date')) : null;
				/*$packing_date = trim($this->input->post('packing_date'));
				$packing_date_array = explode('/',$packing_date);
				$packing_date = date('Y-m-d',mktime(0,0,0,$packing_date_array[1],$packing_date_array[0],$packing_date_array[2]));
			
				$used_by_date = trim($this->input->post('used_by_date'));
				$used_by_date_array = explode('/',$used_by_date);
				$used_by_date = date('Y-m-d',mktime(0,0,0,$used_by_date_array[1],$used_by_date_array[0],$used_by_date_array[2]));
			
				$list_sample['packing_date'] = $packing_date;
				$list_sample['used_by_date'] = $used_by_date;*/
			}
			else 
			{
				$list_sample['batch_no'] = null;
				$list_sample['packing_date'] = null;
				$list_sample['used_by_date'] = null;
			}
			
			$list_sample['receipt_no'] = !empty($this->input->post('receipt_no')) ? trim($this->input->post('receipt_no')) : '';
			$list_sample['disclosure'] = !empty($this->input->post('disclosure')) ? trim($this->input->post('disclosure')) : '';
			if(!empty($this->input->post('reflab')))
			{
				$list_sample['is_demand_referral_lab'] = trim($this->input->post('reflab'));
				$list_sample['referral_lab_id'] = !empty($this->input->post('referral_lab')) ? trim($this->input->post('referral_lab')) : 0;
			}
			else
			{
				$list_sample['is_demand_referral_lab'] = 0;
				$list_sample['referral_lab_id'] = 0;
			}
				
			if(!empty($this->input->post('lab')))
			{
				$list_sample['is_demand_accredited_lab'] = trim($this->input->post('lab'));
				$list_sample['accredited_lab_id'] = !empty($this->input->post('accredited_lab')) ? trim($this->input->post('accredited_lab')) : 0;
			}
			else
			{
				$list_sample['is_demand_accredited_lab'] = 0;
				$list_sample['accredited_lab_id'] = 0;
			}
			$list_sample['witness_name'] = !empty($this->input->post('witness_name')) ? trim($this->input->post('witness_name')) : '';
			$list_sample['witness_address'] = !empty($this->input->post('witness_address')) ? trim($this->input->post('witness_address')) : '';
			$list_sample['is_app'] = 2;
			$list_sample['edit_request_flag'] = 1;
			$list_sample['lab_id'] = trim($this->input->post('lab_id'));
			/*if(!empty($circle_detail->lab_id))
			{
				$list_sample['lab_id'] = $circle_detail->lab_id;
			}*/
			//$list_sample['sample_created_date'] = $sample_created_date;
			$list_sample['updated_by'] = $user_data->id;
			$list_sample['updated_date'] = $updated_date;
			$list_sample['circle_id'] = $this->input->post('fbo_circle_id');
			$where = "id = '".$sample_id."'";
			$this->m_sample->edit($where,$list_sample);
			
			$list_fbo['address_line1'] = trim($this->input->post('fbo_address1'));
			$list_fbo['address_line2'] = !empty($this->input->post('fbo_address2')) ? trim($this->input->post('fbo_address2')) : '';
			$list_fbo['pincode'] = !empty($this->input->post('fbo_pincode')) ? trim($this->input->post('fbo_pincode')) : '';
			$kind_of_business_id = trim($this->input->post('fbo_business'));
			if($kind_of_business_id == "other_business")
			{
				if(!empty($this->input->post('other_business')))
				{
					$kind_of_business = trim($this->input->post('other_business'));
					$list_kind_of_business['business'] = $kind_of_business;
					$list_kind_of_business['created_by'] = $user_data->id;
					$list_kind_of_business['created_date'] = $created_date;
					$insert_kind_of_business_id = $this->m_kind_of_business->insert($list_kind_of_business);
					$list_fbo['kind_of_business_id'] = $insert_kind_of_business_id;
				}
			}
			else
			{
				$list_fbo['kind_of_business_id'] = trim($this->input->post('fbo_business'));
			}
			$list_fbo['license_reg_no'] = !empty($this->input->post('fbo_license_reg_no')) ? trim($this->input->post('fbo_license_reg_no')) : '';
			$list_fbo['updated_by'] = $user_data->id;
			$list_fbo['updated_date'] = $updated_date;
			
			$where = "id = '".$fbo_id."'";
			$this->m_fbo->edit($where,$list_fbo);
			
			$where = "id = '".$sample_id."'";
			$sample_detail = $this->m_sample->getSample($where);
			
			if(!empty($_FILES['image']['name']))
			{
				$sample_code = str_replace('/', '_', $sample_detail->code);
				$prefix = 'sample_'.$sample_code.'_'.time().rand(1000000,9999999);
				$result=$this->utils->image_upload($_FILES['image'],'files/sample_images/',$prefix);
				if($result['error']== 0)
				{
					$file_name = $result['file_name'];
					$this->m_util->generate_thumb('files/sample_images/'.$file_name, 'files/sample_images/'.$file_name, 0, 0);
					$this->m_util->generate_thumb('files/sample_images/'.$file_name, 'files/sample_images/thumb/'.$file_name, 400, 0);
			
					$list_sample_media['sample_id'] = $sample_id;
					$list_sample_media['image'] = $file_name;
					$list_sample_media['thumb'] = $file_name;
					$list_sample_media['created_by'] = $user_data->id;
					$list_sample_media['created_date'] = $updated_date;
			
					$sample_media_id = $this->m_sample_media->insert($list_sample_media);
				}
			}
			
			$url= base_url().'samples';
			redirect($url,"refresh");
		}else{
			$this->session->set_flashdata('mendatory_sample_fbo_data_update', SAMPLE_FBO_REQUIRE_FIELDS_UPDATE);
			$url= base_url().'samples';
			redirect($url,"refresh");
		}
	}
	
	public function view($sample_unique_code){
		$data['sample_data'] = $this->samplei_model->GetDataByID($sample_unique_code);
		if(!@$data['sample_data']->Error_Message){
			$sample_id = $data['sample_data']->id;
			$data['user_data'] = $this->session->userdata('user_data');
			$data['sample_edit_request_details'] = $this->samplei_edit_request_model->GetData(-1,0,$sample_id);
			$data['sample_media'] = $this->samplei_media_model->GetData(-1,0,$sample_id);
			$data['lab_report_status_data'] = $this->m_util->getStatusByReportStatusNumber($data['sample_data']->lab_report_status);
			$data['accredited_lab_report_status_data'] = $this->m_util->getStatusByReportStatusNumber($data['sample_data']->accredited_lab_report_status);
			$data['referral_lab_report_status_data'] = $this->m_util->getStatusByReportStatusNumber($data['sample_data']->referral_lab_report_status);
			$this->load->view('samples/view',$data);
		}else{
			show_404();
		}
	}
	
	public function view_sample_archives($sample_unique_code){
		$data['sample_data'] = $this->samplei_model->GetDataByID($sample_unique_code);
		if(!@$data['sample_data']->Error_Message){
			$sample_id = $data['sample_data']->id;
			$data['user_data'] = $this->session->userdata('user_data');
			$data['sample_edit_request_details'] = $this->samplei_edit_request_model->GetData(-1,0,$sample_id);
			$data['sample_media'] = $this->samplei_media_model->GetData(-1,0,$sample_id);
			$data['lab_report_status_data'] = $this->m_util->getStatusByReportStatusNumber($data['sample_data']->lab_report_status);
			$data['accredited_lab_report_status_data'] = $this->m_util->getStatusByReportStatusNumber($data['sample_data']->accredited_lab_report_status);
			$data['referral_lab_report_status_data'] = $this->m_util->getStatusByReportStatusNumber($data['sample_data']->referral_lab_report_status);
			$this->load->view('samples/view_archives',$data);
		}else{
			show_404();
		}
	}
	
	public function view_sample($sample_unique_code){
		$data['sample_data'] = $this->samplei_model->GetDataByID($sample_unique_code);
		if(!@$data['sample_data']->Error_Message){
			$sample_id = $data['sample_data']->id;

			$data['user_data'] = $this->session->userdata('user_data');
			$data['sample_edit_request_details'] = $this->samplei_edit_request_model->GetData(-1,0,$sample_id);
			$data['sample_media'] = $this->samplei_media_model->GetData(-1,0,$sample_id);			
			$data['lab_report_status_data'] = $this->m_util->getStatusByReportStatusNumber($data['sample_data']->lab_report_status);
			$data['accredited_lab_report_status_data'] = $this->m_util->getStatusByReportStatusNumber($data['sample_data']->accredited_lab_report_status);
			$data['referral_lab_report_status_data'] = $this->m_util->getStatusByReportStatusNumber($data['sample_data']->referral_lab_report_status);
			$this->load->view('samples/view',$data);
		}else{
			show_404();
		}
	}
	
	public function get_transport_lab_details($sample_unique_code){
		$data['sample_data'] = $this->samplei_model->GetDataByID($sample_unique_code);
		if(!@$data['sample_data']->Error_Message && @$data['sample_data']->is_app != 1 && empty(@$data['sample_data']->transport_type_id)){
			$sample_id = $data['sample_data']->id;
			$where = "status = 1 and del_flag = 0";
			$orderby = "id asc";
			$data['transport_type_data'] = $this->m_transport_type->getAllTransportTypes($where,$orderby);
			$this->load->view('samples/transport_type',$data);
		}else{
			show_404();
		}
	}
	
	public function add_transport_lab_details(){
		if(!empty($this->input->post('hdn_id')) && !empty($this->input->post('transport'))){
			$user_data = $this->session->userdata('user_data');
			$updated_date = $this->utils->get_date_time();
			$unique_code = trim($this->input->post('hdn_id'));
				
			$list_sample['transport_type_id'] = trim($this->input->post('transport'));
			$list_sample['updated_by'] = $user_data->id;
			$list_sample['updated_date'] = $updated_date;
				
			$where = "unique_code = '".$unique_code."'";
			$this->m_sample->edit($where,$list_sample);
			
			$url= base_url().'samples';
			redirect($url,"refresh");
		}
		else 
		{
			$this->session->set_flashdata('mendatory_sample_transoprt_lab_data', SAMPLE_TRANSPORT_LAB_REQUIRE_FIELDS);
			$url= base_url().'samples';
			redirect($url,"refresh");
		}
	}
	
	public function get_send_details($sample_unique_code){
		$data['sample_data'] = $this->samplei_model->GetDataByID($sample_unique_code);
		if(!@$data['sample_data']->Error_Message && empty(@$data['sample_data']->slip_no) && !empty(@$data['sample_data']->transport_type_id)){
			$where = "id = '".$data['sample_data']->transport_type_id."' and status = 1 and del_flag = 0";
			$transport_type_detail = $this->m_transport_type->getTransportType($where);
			$data['is_upload_slip'] = $transport_type_detail->is_upload_slip;

			$where = "status = 1 and del_flag = 0";
			$orderby = "id asc";
			$data['transport_type_data'] = $this->m_transport_type->getAllTransportTypes($where,$orderby);
			$this->load->view('samples/send_details',$data);
		}else{
			show_404();
		}
	}
	
	public function add_sample_send_details()
	{
		if(
			!empty($this->input->post('hdn_id')) && 
			!empty($this->input->post('hdn_sample_code')) && 
			!empty($this->input->post('hdn_lab_id')) && 
			!empty($this->input->post('hdn_circle_id')) && 
			!empty($this->input->post('send_date'))
		){
			$user_data = $this->session->userdata('user_data');
			$updated_date = $this->utils->get_date_time();

			$list_sample['transport_type_id'] = trim($this->input->post('transport'));
			$list_sample['updated_by'] = $user_data->id;
			$list_sample['updated_date'] = $updated_date;
			$unique_code = $this->input->post('hdn_id');
			$where = "unique_code = '".$unique_code."'";
			$this->m_sample->edit($where,$list_sample);
				
			
			$sample_data = $this->samplei_model->GetDataByID($this->input->post('hdn_id'));
			$sample_id = $sample_data->id;
			$sample_code = trim($this->input->post('hdn_sample_code'));
			$lab_id = trim($this->input->post('hdn_lab_id'));
			$circle_id = trim($this->input->post('hdn_circle_id'));
			$list_sample['slip_no'] = trim($this->input->post('slip_no'));
			if(trim($this->input->post('slip_date')) != ""){
				$slip_date = trim($this->input->post('slip_date'));
				$slip_date_array = explode('/',$slip_date);
				$slip_date = date('Y-m-d',mktime(0,0,0,$slip_date_array[1],$slip_date_array[0],$slip_date_array[2]));
				$list_sample['slip_date'] = $slip_date;	
			}
			
			$send_date = trim($this->input->post('send_date'));
			$send_date_array = explode('/',$send_date);
			$send_date = date('Y-m-d',mktime(0,0,0,$send_date_array[1],$send_date_array[0],$send_date_array[2]));
			$list_sample['send_date'] = $send_date;
			
			$file_name = '';
			if(!empty($_FILES['image']['name'])){
				$sample_code = str_replace('/', '_', $sample_code);
				$prefix = 'slip_'.$sample_code.'_'.time().rand(1000000,9999999);
				$result=$this->utils->image_upload($_FILES['image'],'files/transport_slips/',$prefix);
				if($result['error']== 0){
					$file_name = $result['file_name'];
					$this->m_util->generate_thumb('files/transport_slips/'.$file_name, 'files/transport_slips/'.$file_name, 0, 0);
					$this->m_util->generate_thumb('files/transport_slips/'.$file_name, 'files/transport_slips/thumb/'.$file_name, 400, 0);
				}
			}
			if(!empty($sample_data->lab_id)){
				$data = $this->m_user->getUsers(PAGE_SIZE,0,5,'',-1,$sample_data->lab_id);
				if(!@$data[0]->Error_Message){
					if(@$data[0]->rowcount == 1){
						$list_sample['fa_id'] = $data[0]->id;
					}
				}
			}
			$list_sample['slip_image'] = $file_name;
			$list_sample['is_send'] = 1;
			$list_sample['updated_by'] = $user_data->id;
			$list_sample['updated_date'] = $updated_date;
			$where = "id = '".$sample_id."'";
			$this->m_sample->edit($where,$list_sample);
			
			// Send Email To User Start
			
			/*$subject= "FDCA - Send Sample To ".$labname;
			$message = file_get_contents(base_url().'files/assets/email_templates/sendSampleToLab.php');
			$message = str_replace('%BasePath%', base_url(), $message);
			$message = str_replace('%samplename%', $sample_detail->name, $message);
			$message = str_replace('%shortname%', $sample_detail->short_name, $message);
			$message = str_replace('%samplecode%', $sample_detail->code, $message);
			$where = "id = '".$sample_detail->category_id."' and status = 1 and del_flag = 0";
			$food_categories_detail = $this->m_food_category->getFoodCategory($where);
			$message = str_replace('%foodcategory%', $food_categories_detail->name, $message);
			$where = "sample_id = '".$sample_detail->id."' and status = 1 and del_flag = 0";
			$fbo_detail = $this->m_fbo->getFbo($where);
			$message = str_replace('%fboname%', $fbo_detail->name, $message);
			$message = str_replace('%fsoname%', $user_data->full_name, $message);
			$message = str_replace('%circlename%', $circle_detail->name, $message);
			$message = str_replace('%labname%', $labname, $message);
			$message = str_replace('%createddate%', $this->m_util->date_format($sample_detail->sample_created_date), $message);
			$where = "id = '".$sample_detail->transport_type_id."' and status = 1 and del_flag = 0";
			$transport_type_detail = $this->m_transport_type->getTransportType($where);
			$message = str_replace('%transportationtype%', $transport_type_detail->type, $message);
			$message = str_replace('%slipno%', $sample_detail->slip_no, $message);
			$message = str_replace('%senddate%', $this->m_util->date_time_format($sample_detail->send_date), $message);
			
			$emails_to_send = array();
			array_push($emails_to_send, $user_data->email);
			
			if(!empty($do_users_details))
			{
				foreach ($do_users_details as $_do_users_details)
				{
					array_push($emails_to_send, $_do_users_details->email);
				}
			}
			
			if(!empty($emails_to_send))
			{
				foreach ($emails_to_send as $_emails_to_send)
				{
					$to = $_emails_to_send;
					$this->m_util->email($to,$subject,$message);
				}
			}
			
			$where = "type = 6 and status = 1 and del_flag = 0";
			$jct_user_detail = $this->m_user->getUser($where);
			
			$lab_subject = "FDCA - New Sample Received For Testing";
			$jct_to = $jct_user_detail->email;
			$lab_message = file_get_contents(base_url().'files/assets/email_templates/sendSampleToLabFoodAnalyst.php');
			$lab_message = str_replace('%BasePath%', base_url(), $lab_message);
			$lab_message = str_replace('%samplename%', $sample_detail->name, $lab_message);
			$lab_message = str_replace('%shortname%', $sample_detail->short_name, $lab_message);
			$lab_message = str_replace('%samplecode%', $sample_detail->code, $lab_message);
			$where = "id = '".$sample_detail->category_id."' and status = 1 and del_flag = 0";
			$food_categories_detail = $this->m_food_category->getFoodCategory($where);
			$lab_message = str_replace('%foodcategory%', $food_categories_detail->name, $lab_message);
			$where = "sample_id = '".$sample_detail->id."' and status = 1 and del_flag = 0";
			$fbo_detail = $this->m_fbo->getFbo($where);
			$lab_message = str_replace('%fboname%', $fbo_detail->name, $lab_message);
			$lab_message = str_replace('%fsoname%', $user_data->full_name, $lab_message);
			$lab_message = str_replace('%circlename%', $circle_detail->name, $lab_message);
			$lab_message = str_replace('%labname%', $labname, $lab_message);
			$lab_message = str_replace('%createddate%', $this->m_util->date_format($sample_detail->sample_created_date), $lab_message);
			$where = "id = '".$sample_detail->transport_type_id."' and status = 1 and del_flag = 0";
			$transport_type_detail = $this->m_transport_type->getTransportType($where);
			$lab_message = str_replace('%transportationtype%', $transport_type_detail->type, $lab_message);
			$lab_message = str_replace('%slipno%', $sample_detail->slip_no, $lab_message);
			$lab_message = str_replace('%senddate%', $this->m_util->date_time_format($sample_detail->send_date), $lab_message);
			
			$this->m_util->email($jct_to,$lab_subject,$lab_message);
			
			if(!empty($food_analyst_id))
			{
				$fa_users_detail = $this->m_user->getFoodAnalystDetailFromUserId($food_analyst_id);
				$fa_to = $fa_users_detail->email;
				
				$this->m_util->email($fa_to,$lab_subject,$lab_message);
			}*/
			// Send Email To User End
			$url= base_url().'samples';
			redirect($url,"refresh");
		}else {
			$this->session->set_flashdata('mendatory_sample_send_data', SAMPLE_SEND_REQUIRE_FIELDS);
			$url= base_url().'samples';
			redirect($url,"refresh");
		}
	}
	
	public function edit_request($sample_unique_code){
		$data['sample_data'] = $this->samplei_model->GetDataByID($sample_unique_code);
		if(!@$data['sample_data']->Error_Message && @$data['sample_data']->is_app != 1 && @$data['sample_data']->edit_request_flag == 1){
			$this->load->view('samples/edit_request',$data);
		}else{
			show_404();
		}
	}
	
	public function insert_edit_request(){
		if(!empty($this->input->post('hdn_id')) && !empty($this->input->post('note'))){
			$user_data = $this->session->userdata('user_data');
			$created_date = $this->utils->get_date_time();
			$unique_code = trim($this->input->post('hdn_id'));
			$note = trim($this->input->post('note'));
			$sample_detail = $this->samplei_model->GetDataByID($unique_code);
			// $do_users_details = $this->m_user->getDoUsersFromCircleId($sample_detail->circle_id);
			$sample_id = $sample_detail->id;
			// if(!empty($do_users_details)){
				// foreach ($do_users_details as $_do_users_details){
					$list['sample_id'] = $sample_detail->id;
					$list['fso_id'] = $user_data->id;
					$list['do_id'] = 0;//$_do_users_details->id;
					$list['circle_id'] = $sample_detail->circle_id;
					$list['fso_note'] = $note;
					$list['created_by'] = $user_data->id;
					$res = $this->samplei_edit_request_model->Insert($list);
			// 	}
			// }
			// Send Email To User Start
			/*$subject= "FDCA - Create New Sample Edit Request";
			//$to=$list_user['email'];
			$message = file_get_contents(base_url().'files/assets/email_templates/createSampleEditRequest.php');
			$message = str_replace('%BasePath%', base_url(), $message);
			$message = str_replace('%samplename%', $sample_detail->name, $message);
			$message = str_replace('%shortname%', $sample_detail->short_name, $message);
			$message = str_replace('%samplecode%', $sample_detail->code, $message);
			$where = "id = '".$sample_detail->category_id."' and status = 1 and del_flag = 0";
			$food_categories_detail = $this->m_food_category->getFoodCategory($where);
			$message = str_replace('%foodcategory%', $food_categories_detail->name, $message);
			$where = "sample_id = '".$sample_detail->id."' and status = 1 and del_flag = 0";
			$fbo_detail = $this->m_fbo->getFbo($where);
			$message = str_replace('%fboname%', $fbo_detail->name, $message);
			$message = str_replace('%fsoname%', $user_data->full_name, $message);
			$where = "id = '".$user_data->circle_id."' and status = 1 and del_flag = 0";
			$circle_detail = $this->m_circle->getCircle($where);
			$message = str_replace('%circlename%', $circle_detail->name, $message);
			$message = str_replace('%createddate%', !empty($sample_detail->sample_created_date) ? $this->m_util->date_format($sample_detail->sample_created_date) : '', $message);
				
			$emails_to_send = array();
			if(!empty($do_users_details))
			{
				foreach ($do_users_details as $_do_users_details)
				{
					array_push($emails_to_send, $_do_users_details->email);
				}
			}
				
			if(!empty($emails_to_send))
			{
				foreach ($emails_to_send as $_emails_to_send)
				{
					$to = $_emails_to_send;
					$this->m_util->email($to,$subject,$message);
				}
			}*/
			// Send Email To User End
			
			$url= base_url().'samples';
			redirect($url,"refresh");
		}
		else
		{
			$this->session->set_flashdata('mendatory_sample_edit_request_data', SAMPLE_EDIT_REQUEST_REQUIRE_FIELDS);
			$url= base_url().'samples';
			redirect($url,"refresh");
		}
	}
	
	public function edit_sample($sample_unique_code){
		$data['sample_data'] = $this->samplei_model->GetDataByID($sample_unique_code);
		if(!@$data['sample_data']->Error_Message 
			){
			$data['user_data'] = $this->session->userdata('user_data');
			$where = "status = 1 and del_flag = 0";
			$wherein = explode(',',$data['user_data']->circle_id);
			if(!empty($wherein) && in_array(GANDHINAGAR_CIRCLE_ID, $wherein)){
				$wherein = NULL;
			}

			if(!empty($wherein) && !in_array($data['sample_data']->circle_id,$wherein)){
				$where .= " and id = ".$data['sample_data']->circle_id;
				$wherein = array();
			}
			$data['circle_detail'] = $this->m_circle->getAllCirclesWhereIdIn($where,$wherein);
			$data['circle_count'] = count($data['circle_detail']);
			if($data['circle_count'] == 1){
				$data['circle_detail'] = $data['circle_detail'][0];
			}
			$sample_id = $data['sample_data']->id;
			$data['current_year'] = $this->utils->get_year();

			$where = "flag = 1 and del_flag = 0";
			$orderby = "status asc";
			$data['fbo_status_details'] = $this->m_fbo_status->getAllFboStatus($where,$orderby);
			
			$where = "status = 1 and del_flag = 0";
			$orderby = "name asc";
			$data['district_details'] = $this->m_district->getAllDistricts($where,$orderby);
			
			$where = "district_id = '".$data['sample_data']->fbo_district_id."' and status = 1 and del_flag = 0";
			$orderby = "name asc";
			$data['taluka_details'] = $this->m_taluka->getAllTalukas($where,$orderby);
			
			$where = "status = 1 and del_flag = 0";
			$orderby = "type asc";
			$data['firm_type_details'] = $this->m_firm_type->getAllFirmTypes($where,$orderby);
			
			$where = "status = 1 and del_flag = 0";
			$orderby = "business asc";
			$data['kind_of_business_details'] = $this->m_kind_of_business->getAllKindOfBusinesses($where,$orderby);
			
			$where = "parent_id = 0 and status = 1 and del_flag = 0";
			$orderby = "name asc";
			$data['food_categories_details'] = $this->m_food_category->getAllFoodCategories($where,$orderby);
			
			$where = "status = 1 and del_flag = 0";
			$orderby = "id asc";
			$data['pack_type_details'] = $this->m_pack_type->getAllPackTypes($where,$orderby);
			
			$where = "status = 1 and del_flag = 0";
			$orderby = "id asc";
			$data['investigation_type_details'] = $this->m_investigation_type->getAllInvestigationTypes($where,$orderby);
			
			$where = "status = 1 and del_flag = 0";
			$orderby = "id asc";
			$data['lab_details'] = $this->m_lab->getAllLab($where,$orderby);
			
			$where = "status = 1 and del_flag = 0";
			$orderby = "id asc";
			$data['referral_lab_details'] = $this->m_referral_lab->getAllReferralLab($where,$orderby);
			
			$where = "status = 1 and del_flag = 0";
			$orderby = "id asc";
			$data['accredited_lab_details'] = $this->m_accredited_lab->getAllAccreditedLab($where,$orderby);
			
			
			$where = "parent_id = '".$data['sample_data']->food_category_parent_id."' and status = 1 and del_flag = 0";
			$orderby = "name asc";
			$data['sub_food_categories_details'] = $this->m_food_category->getAllFoodCategories($where,$orderby);
			$this->load->view('samples/edit_sample',$data);
		}else{
			show_404();
		}
	}
	public function update_sample()
	{
		if(
			!empty($this->input->post('hdn_fbo_id')) && 
			!empty($this->input->post('fbo_circle_id')) && 
			!empty($this->input->post('hdn_sample_id')) && 
			!empty($this->input->post('created_date')) && 
			!empty($this->input->post('hour')) && 
			!empty($this->input->post('minute')) && 
			!empty($this->input->post('ampm')) && 
			!empty($this->input->post('fbo_name')) && 
			!empty($this->input->post('firm_name')) && 
			!empty($this->input->post('fbo_status')) && 
			!empty($this->input->post('fbo_address1')) && 
			!empty($this->input->post('fbo_district')) && 
			!empty($this->input->post('fbo_taluko')) && 
			!empty($this->input->post('fbo_place')) && 
			!empty($this->input->post('fbo_business')) && 
			!empty($this->input->post('fbo_firmtype')) && 
			!empty($this->input->post('sample_name'))  && 
			!empty($this->input->post('short_name')) && 
			!empty($this->input->post('sample_code')) && 
			!empty($this->input->post('food_category')) && 
			!empty($this->input->post('manufacturer_address')) && 
			!empty($this->input->post('pack_type')) && 
			!empty($this->input->post('quantity')) && 
			!empty($this->input->post('quantity_type')) && 
			!empty($this->input->post('price')) && 
			!empty($this->input->post('price_type')) && 
			!empty($this->input->post('payment_fbo')) && 
			!empty($this->input->post('investigation_type')) && 
			!empty($this->input->post('lab_id')))
		{
			$user_data = $this->session->userdata('user_data');
			$updated_date = $this->utils->get_date_time();
			$current_year = $this->utils->get_year();
				
			// $where = "id = '".$user_data->circle_id."' and status = 1 and del_flag = 0";
			// $circle_detail = $this->m_circle->getCircle($where);
			
			$fbo_id = trim($this->input->post('hdn_fbo_id'));
			$sample_id = trim($this->input->post('hdn_sample_id'));
			$is_require = false;
			
			$sample_created_date = trim($this->input->post('created_date'));
			$sample_created_date_array = explode('/',$sample_created_date);
			$sample_created_date = date('Y-m-d',mktime(0,0,0,$sample_created_date_array[1],$sample_created_date_array[0],$sample_created_date_array[2]));
			$sample_created_time = trim($this->input->post('hour')).':'.trim($this->input->post('minute')).' '.trim($this->input->post('ampm'));
				
			$list_fbo['name'] = trim($this->input->post('fbo_name'));
			$list_fbo['firm_name'] = trim($this->input->post('firm_name'));
			$fbo_status_id = trim($this->input->post('fbo_status'));
			if($fbo_status_id == "other_fbo_status"){
				if(!empty($this->input->post('other_fbo_status'))){
					$fbo_status = trim($this->input->post('other_fbo_status'));
					$list_fbo_status['status'] = $fbo_status;
					$list_fbo_status['created_by'] = $user_data->id;
					$list_fbo_status['created_date'] = $updated_date;
					$insert_fbo_status_id = $this->m_fbo_status->insert($list_fbo_status);
					$list_fbo['status_id'] = $insert_fbo_status_id;
				}
			}else{
				$list_fbo['status_id'] = $fbo_status_id;
			}
			$list_fbo['address_line1'] = trim($this->input->post('fbo_address1'));
			$list_fbo['address_line2'] = !empty($this->input->post('fbo_address2')) ? trim($this->input->post('fbo_address2')) : '';
			$list_fbo['district_id'] = !empty($this->input->post('fbo_district')) ? trim($this->input->post('fbo_district')) : '';
			$list_fbo['taluka_id'] = !empty($this->input->post('fbo_taluko')) ? trim($this->input->post('fbo_taluko')) : '';
			$list_fbo['place'] = !empty($this->input->post('fbo_place')) ? trim($this->input->post('fbo_place')) : '';
			$list_fbo['pincode'] = !empty($this->input->post('fbo_pincode')) ? trim($this->input->post('fbo_pincode')) : '';
			$firm_type_id = trim($this->input->post('fbo_firmtype'));
			if($firm_type_id == "other_firmtype"){
				if(!empty($this->input->post('other_firmtype'))){
					$firm_type = trim($this->input->post('other_firmtype'));
					$list_firm_type['type'] = $firm_type;
					$list_firm_type['created_by'] = $user_data->id;
					$list_firm_type['created_date'] = $updated_date;
					$insert_firm_type_id = $this->m_firm_type->insert($list_firm_type);
					$list_fbo['firm_type_id'] = $insert_firm_type_id;
				}
			}else{
				$list_fbo['firm_type_id'] = $firm_type_id;
			}
			$kind_of_business_id = trim($this->input->post('fbo_business'));
			if($kind_of_business_id == "other_business"){
				if(!empty($this->input->post('other_business'))){
					$kind_of_business = trim($this->input->post('other_business'));
					$list_kind_of_business['business'] = $kind_of_business;
					$list_kind_of_business['created_by'] = $user_data->id;
					$list_kind_of_business['created_date'] = $updated_date;
					$insert_kind_of_business_id = $this->m_kind_of_business->insert($list_kind_of_business);
					$list_fbo['kind_of_business_id'] = $insert_kind_of_business_id;
				}
			}else{
				$list_fbo['kind_of_business_id'] = trim($this->input->post('fbo_business'));
			}
			$list_fbo['license_reg_no'] = !empty($this->input->post('fbo_license_reg_no')) ? trim($this->input->post('fbo_license_reg_no')) : '';
				
			//$list_sample['sample_no'] = !empty($this->input->post('hdn_sample_no')) ? trim($this->input->post('hdn_sample_no')) : '';
			//$list_sample['sample_no'] = trim($this->input->post('sample_code'));
			$list_sample['name'] = trim($this->input->post('sample_name'));
			$list_sample['short_name'] = trim($this->input->post('short_name'));
			$list_sample['code'] = trim($this->input->post('sample_code'));
			//$list_sample['code'] = $circle_detail->circle_id.$circle_detail->short_name.$user_data->fso_id."/".$list_sample['sample_no']."/".$current_year;
			//$list_sample['code'] = !empty($this->input->post('hdn_sample_code')) ? trim($this->input->post('hdn_sample_code')) : '';
			$list_sample['category_id'] = !empty($this->input->post('food_subcategory')) ? trim($this->input->post('food_subcategory')) : trim($this->input->post('food_category'));
			$list_sample['manufacturer_address'] = trim($this->input->post('manufacturer_address'));
			$pack_id = trim($this->input->post('pack_type'));
			if($pack_id == "other_packtype"){
				if(!empty($this->input->post('other_packtype'))){
					$pack_type = trim($this->input->post('other_packtype'));
					$list_pack_type['type'] = $pack_type;
					$list_pack_type['created_by'] = $user_data->id;
					$list_pack_type['created_date'] = $updated_date;
					$insert_pack_type_id = $this->m_pack_type->insert($list_pack_type);
					$list_sample['pack_id'] = $insert_pack_type_id;
				}
			}else{
				$list_sample['pack_id'] = trim($this->input->post('pack_type'));
			}
			
			if($pack_id == "1" || $pack_id == "2"){
				$list_sample['batch_no'] = !empty($this->input->post('batch_no')) ? trim($this->input->post('batch_no')) : null;
				$list_sample['packing_date'] = !empty($this->input->post('packing_date')) ? trim($this->input->post('packing_date')) : null;
				$list_sample['used_by_date'] = !empty($this->input->post('used_by_date')) ? trim($this->input->post('used_by_date')) : null;
				
				/*$packing_date = trim($this->input->post('packing_date'));
				$packing_date_array = explode('/',$packing_date);
				$packing_date = date('Y-m-d',mktime(0,0,0,$packing_date_array[1],$packing_date_array[0],$packing_date_array[2]));
					
				$used_by_date = trim($this->input->post('used_by_date'));
				$used_by_date_array = explode('/',$used_by_date);
				$used_by_date = date('Y-m-d',mktime(0,0,0,$used_by_date_array[1],$used_by_date_array[0],$used_by_date_array[2]));
					
				$list_sample['packing_date'] = $packing_date;
				$list_sample['used_by_date'] = $used_by_date;*/
			}else{
				$list_sample['batch_no'] = null;
				$list_sample['packing_date'] = null;
				$list_sample['used_by_date'] = null;
			}
			
			$list_sample['quantity'] = trim($this->input->post('quantity'));
			$list_sample['quantity_type'] = trim($this->input->post('quantity_type'));
			$list_sample['price'] = trim($this->input->post('price'));
			$list_sample['price_type'] = trim($this->input->post('price_type'));
			$list_sample['payment_to_fbo'] = trim($this->input->post('payment_fbo'));
			$list_sample['receipt_no'] = !empty($this->input->post('receipt_no')) ? trim($this->input->post('receipt_no')) : '';
			$list_sample['investigation_id'] = trim($this->input->post('investigation_type'));
			$list_sample['preservation_added'] = !empty($this->input->post('preservation_details')) ? trim($this->input->post('preservation_details')) : '';
			$list_sample['disclosure'] = !empty($this->input->post('disclosure')) ? trim($this->input->post('disclosure')) : '';
			$list_sample['lab_id'] = trim($this->input->post('lab_id'));

			$list_sample['is_demand_referral_lab'] = 0;
			$list_sample['referral_lab_id'] = 0;
			if(!empty($this->input->post('reflab'))){
				$list_sample['is_demand_referral_lab'] = trim($this->input->post('reflab'));
				$list_sample['referral_lab_id'] = !empty($this->input->post('referral_lab')) ? trim($this->input->post('referral_lab')) : 0;
			}
			
			$list_sample['is_demand_accredited_lab'] = 0;
				$list_sample['accredited_lab_id'] = 0;
			if(!empty($this->input->post('lab'))){
				$list_sample['is_demand_accredited_lab'] = trim($this->input->post('lab'));
				$list_sample['accredited_lab_id'] = !empty($this->input->post('accredited_lab')) ? trim($this->input->post('accredited_lab')) : 0;
			}

			$list_sample['witness_name'] = !empty($this->input->post('witness_name')) ? trim($this->input->post('witness_name')) : '';
			$list_sample['witness_address'] = !empty($this->input->post('witness_address')) ? trim($this->input->post('witness_address')) : '';
			$list_sample['is_seizzer'] = trim($this->input->post('seized'));
			if(!empty($this->input->post('seized'))){
				if(!empty($this->input->post('seized_quantity'))){
					$list_sample['seizzer_quantity'] = trim($this->input->post('seized_quantity'));
				}else{
					$list_sample['seizzer_quantity'] = 0;
				}
			
				if(!empty($this->input->post('seized_quantity_type'))){
					$list_sample['seizzer_quantity_type'] = trim($this->input->post('seized_quantity_type'));
				}
			
				if(!empty($this->input->post('seized_quantity_ml_ltr'))){
					$list_sample['seizzer_quantity_ml_ltr'] = trim($this->input->post('seized_quantity_ml_ltr'));
				}else{
					$list_sample['seizzer_quantity_ml_ltr'] = 0;
				}
			
				if(!empty($this->input->post('seized_quantity_type_ml_ltr'))){
					$list_sample['seizzer_quantity_type_ml_ltr'] = trim($this->input->post('seized_quantity_type_ml_ltr'));
				}
			
				if(!empty($this->input->post('seized_price'))){
					$list_sample['seizzer_price'] = trim($this->input->post('seized_price'));
				}
			}else{
				$list_sample['seizzer_quantity'] = 0;
				$list_sample['seizzer_quantity_type'] = null;
				$list_sample['seizzer_quantity_ml_ltr'] = 0;
				$list_sample['seizzer_quantity_type_ml_ltr'] = null;
				$list_sample['seizzer_price'] = 0;
			}
			
			$list_sample['edit_request_flag'] = 1;
			$list_sample['sample_created_date'] = $sample_created_date;
			$list_sample['sample_created_time'] = $sample_created_time;
			$list_sample['updated_by'] = $user_data->id;
			$list_sample['updated_date'] = $updated_date;
			$list_sample['circle_id'] = $this->input->post('fbo_circle_id');
			
			
			$where = "id = '".$sample_id."'";
			$this->m_sample->edit($where,$list_sample);
				
			$list_fbo['updated_by'] = $user_data->id;
			$list_fbo['updated_date'] = $updated_date;
			
			$where = "id = '".$fbo_id."'";
			$this->m_fbo->edit($where,$list_fbo);
			
			$where = "id = '".$sample_id."'";
			$sample_detail = $this->samplei_model->GetDataByID('',$sample_id);
			
			if(!empty($_FILES['image']['name'])){
				$sample_code = str_replace('/', '_', $sample_detail->code);
				$prefix = 'sample_'.$sample_code.'_'.time().rand(1000000,9999999);
				$result=$this->utils->image_upload($_FILES['image'],'files/sample_images/',$prefix);
				if($result['error']== 0){
					$file_name = $result['file_name'];
					$this->m_util->generate_thumb('files/sample_images/'.$file_name, 'files/sample_images/thumb/'.$file_name, 0, 0);
					$this->m_util->generate_thumb('files/sample_images/'.$file_name, 'files/sample_images/thumb/'.$file_name, 400, 0);
					$list_sample_media['sample_id'] = $sample_id;
					$list_sample_media['image'] = $file_name;
					$list_sample_media['thumb'] = $file_name;
					$list_sample_media['created_by'] = $user_data->id;
					$list_sample_media['created_date'] = $updated_date;
					$sample_media_id = $this->m_sample_media->insert($list_sample_media);
				}
			}
			$url= base_url().'samples';
			redirect($url,"refresh");
		}else{
			$this->session->set_flashdata('mendatory_sample_fbo_data_edit', SAMPLE_FBO_REQUIRE_FIELDS_EDIT);
			$url= base_url().'samples';
			redirect($url,"refresh");
		}
	}
	public function edit_backdata($sample_unique_code){
		$data['sample_data'] = $this->samplei_model->GetDataByID($sample_unique_code);
		if(!@$data['sample_data']->Error_Message && @$data['sample_data']->is_offline == 1){
			$data['user_data'] = $this->session->userdata('user_data');
			$where = "status = 1 and del_flag = 0";
			$wherein = explode(',',$data['user_data']->circle_id);
			if(!empty($wherein) && in_array(GANDHINAGAR_CIRCLE_ID, $wherein)){
				$wherein = NULL;
			}
			if(!empty($wherein) && !in_array($data['sample_data']->circle_id,$wherein)){
				$where .= " and id = ".$data['sample_data']->circle_id;
				$wherein = array();
			}
			$data['food_categories_detail'] = $this->m_util->getCategoryTree('0',$data['sample_data']->category_id);
			$data['circle_detail'] = $this->m_circle->getAllCirclesWhereIdIn($where,$wherein);
			$data['circle_count'] = count($data['circle_detail']);
			if($data['circle_count'] == 1){
				$data['circle_detail'] = $data['circle_detail'][0];
			}
			$sample_id = $data['sample_data']->id;
			$data['current_year'] = $this->utils->get_year();

			$where = "flag = 1 and del_flag = 0";
			$orderby = "status asc";
			$data['fbo_status_details'] = $this->m_fbo_status->getAllFboStatus($where,$orderby);
			
			$where = "status = 1 and del_flag = 0";
			$orderby = "name asc";
			$data['district_details'] = $this->m_district->getAllDistricts($where,$orderby);
			
			$where = "district_id = '".$data['sample_data']->fbo_district_id."' and status = 1 and del_flag = 0";
			$orderby = "name asc";
			$data['taluka_details'] = $this->m_taluka->getAllTalukas($where,$orderby);
			
			$where = "status = 1 and del_flag = 0";
			$orderby = "type asc";
			$data['firm_type_details'] = $this->m_firm_type->getAllFirmTypes($where,$orderby);
			
			$where = "status = 1 and del_flag = 0";
			$orderby = "business asc";
			$data['kind_of_business_details'] = $this->m_kind_of_business->getAllKindOfBusinesses($where,$orderby);
			
			$where = "parent_id = 0 and status = 1 and del_flag = 0";
			$orderby = "name asc";
			$data['food_categories_details'] = $this->m_food_category->getAllFoodCategories($where,$orderby);
			
			$where = "status = 1 and del_flag = 0";
			$orderby = "id asc";
			$data['pack_type_details'] = $this->m_pack_type->getAllPackTypes($where,$orderby);
			
			$where = "status = 1 and del_flag = 0";
			$orderby = "id asc";
			$data['investigation_type_details'] = $this->m_investigation_type->getAllInvestigationTypes($where,$orderby);
			
			$where = "status = 1 and del_flag = 0";
			$orderby = "id asc";
			$data['lab_details'] = $this->m_lab->getAllLab($where,$orderby);
			
			$where = "status = 1 and del_flag = 0";
			$orderby = "id asc";
			$data['referral_lab_details'] = $this->m_referral_lab->getAllReferralLab($where,$orderby);
			
			$where = "status = 1 and del_flag = 0";
			$orderby = "id asc";
			$data['accredited_lab_details'] = $this->m_accredited_lab->getAllAccreditedLab($where,$orderby);
			
			
			$where = "parent_id = '".$data['sample_data']->food_category_parent_id."' and status = 1 and del_flag = 0";
			$orderby = "name asc";
			$data['sub_food_categories_details'] = $this->m_food_category->getAllFoodCategories($where,$orderby);
			$this->load->view('samples/edit_backdata',$data);
		}else{
			show_404();
		}
	}	
	public function update_sample_backdata()
	{
		if(
			!empty($this->input->post('hdn_fbo_id')) && 
			!empty($this->input->post('fbo_circle_id')) && 
			!empty($this->input->post('hdn_sample_id')) && 
			!empty($this->input->post('created_date')) && 
			// !empty($this->input->post('hour')) && 
			// !empty($this->input->post('minute')) && 
			// !empty($this->input->post('ampm')) && 
			!empty($this->input->post('fbo_name')) && 
			// !empty($this->input->post('firm_name')) && 
			!empty($this->input->post('fbo_status')) && 
			!empty($this->input->post('fbo_address1')) && 
			// !empty($this->input->post('fbo_district')) && 
			// !empty($this->input->post('fbo_taluko')) && 
			!empty($this->input->post('fbo_place')) && 
			// !empty($this->input->post('fbo_business')) && 
			// !empty($this->input->post('fbo_firmtype')) && 
			!empty($this->input->post('sample_name'))  && 
			// !empty($this->input->post('short_name')) && 
			!empty($this->input->post('sample_code')) && 
			!empty($this->input->post('food_subcategory')) && 
			!empty($this->input->post('manufacturer_address')) &&
			// !empty($this->input->post('pack_type')) && 
			// !empty($this->input->post('quantity')) && 
			// !empty($this->input->post('quantity_type')) && 
			!empty($this->input->post('price'))
			// !empty($this->input->post('price_type')) && 
			// !empty($this->input->post('payment_fbo')) && 
			// !empty($this->input->post('investigation_type')) && 
			// !empty($this->input->post('lab_id')))
		){
			$user_data = $this->session->userdata('user_data');
			$updated_date = $this->utils->get_date_time();
			$current_year = $this->utils->get_year();
				
			// $where = "id = '".$user_data->circle_id."' and status = 1 and del_flag = 0";
			// $circle_detail = $this->m_circle->getCircle($where);
			
			$fbo_id = trim($this->input->post('hdn_fbo_id'));
			$sample_id = trim($this->input->post('hdn_sample_id'));
			$is_require = false;
			
			$sample_created_date = trim($this->input->post('created_date'));
			$sample_created_date_array = explode('/',$sample_created_date);
			$sample_created_date = date('Y-m-d',mktime(0,0,0,$sample_created_date_array[1],$sample_created_date_array[0],$sample_created_date_array[2]));
			$sample_created_time = "";
			if($this->input->post('hour') != "" && $this->input->post('minute') != "" && $this->input->post('ampm') != ""){
				$sample_created_time = trim($this->input->post('hour')).':'.trim($this->input->post('minute')).' '.trim($this->input->post('ampm'));
			}
			$list_fbo['name'] = trim($this->input->post('fbo_name'));
			$list_fbo['firm_name'] = trim($this->input->post('firm_name'));
			$fbo_status_id = trim($this->input->post('fbo_status'));
			if($fbo_status_id == "other_fbo_status"){
				if(!empty($this->input->post('other_fbo_status'))){
					$fbo_status = trim($this->input->post('other_fbo_status'));
					$list_fbo_status['status'] = $fbo_status;
					$list_fbo_status['created_by'] = $user_data->id;
					$list_fbo_status['created_date'] = $updated_date;
					$insert_fbo_status_id = $this->m_fbo_status->insert($list_fbo_status);
					$list_fbo['status_id'] = $insert_fbo_status_id;
				}
			}else{
				$list_fbo['status_id'] = $fbo_status_id;
			}
			$list_fbo['address_line1'] = trim($this->input->post('fbo_address1'));
			$list_fbo['address_line2'] = !empty($this->input->post('fbo_address2')) ? trim($this->input->post('fbo_address2')) : '';
			$list_fbo['district_id'] = !empty($this->input->post('fbo_district')) ? trim($this->input->post('fbo_district')) : '';
			$list_fbo['taluka_id'] = !empty($this->input->post('fbo_taluko')) ? trim($this->input->post('fbo_taluko')) : '';
			$list_fbo['place'] = !empty($this->input->post('fbo_place')) ? trim($this->input->post('fbo_place')) : '';
			$list_fbo['pincode'] = !empty($this->input->post('fbo_pincode')) ? trim($this->input->post('fbo_pincode')) : '';
			$firm_type_id = trim($this->input->post('fbo_firmtype'));
			if($firm_type_id == "other_firmtype"){
				if(!empty($this->input->post('other_firmtype'))){
					$firm_type = trim($this->input->post('other_firmtype'));
					$list_firm_type['type'] = $firm_type;
					$list_firm_type['created_by'] = $user_data->id;
					$list_firm_type['created_date'] = $updated_date;
					$insert_firm_type_id = $this->m_firm_type->insert($list_firm_type);
					$list_fbo['firm_type_id'] = $insert_firm_type_id;
				}
			}else if(!empty($firm_type_id)){
				$list_fbo['firm_type_id'] = $firm_type_id;
			}
			$kind_of_business_id = trim($this->input->post('fbo_business'));
			if($kind_of_business_id == "other_business"){
				if(!empty($this->input->post('other_business'))){
					$kind_of_business = trim($this->input->post('other_business'));
					$list_kind_of_business['business'] = $kind_of_business;
					$list_kind_of_business['created_by'] = $user_data->id;
					$list_kind_of_business['created_date'] = $updated_date;
					$insert_kind_of_business_id = $this->m_kind_of_business->insert($list_kind_of_business);
					$list_fbo['kind_of_business_id'] = $insert_kind_of_business_id;
				}
			}else if(!empty($kind_of_business_id)){
				$list_fbo['kind_of_business_id'] = $kind_of_business_id;
			}
			$list_fbo['license_reg_no'] = !empty($this->input->post('fbo_license_reg_no')) ? trim($this->input->post('fbo_license_reg_no')) : '';
				
			//$list_sample['sample_no'] = !empty($this->input->post('hdn_sample_no')) ? trim($this->input->post('hdn_sample_no')) : '';
			//$list_sample['sample_no'] = trim($this->input->post('sample_code'));
			$list_sample['name'] = trim($this->input->post('sample_name'));
			$list_sample['short_name'] = trim($this->input->post('short_name'));
			$list_sample['code'] = trim($this->input->post('sample_code'));
			//$list_sample['code'] = $circle_detail->circle_id.$circle_detail->short_name.$user_data->fso_id."/".$list_sample['sample_no']."/".$current_year;
			//$list_sample['code'] = !empty($this->input->post('hdn_sample_code')) ? trim($this->input->post('hdn_sample_code')) : '';
			$list_sample['category_id'] = !empty($this->input->post('food_subcategory')) ? trim($this->input->post('food_subcategory')) : trim($this->input->post('food_category'));
			$list_sample['manufacturer_address'] = trim($this->input->post('manufacturer_address'));
			$pack_id = trim($this->input->post('pack_type'));
			if($pack_id == "other_packtype"){
				if(!empty($this->input->post('other_packtype'))){
					$pack_type = trim($this->input->post('other_packtype'));
					$list_pack_type['type'] = $pack_type;
					$list_pack_type['created_by'] = $user_data->id;
					$list_pack_type['created_date'] = $updated_date;
					$insert_pack_type_id = $this->m_pack_type->insert($list_pack_type);
					$list_sample['pack_id'] = $insert_pack_type_id;
				}
			}else if(!empty($pack_id)){
				$list_sample['pack_id'] = $pack_id;
			}
			
			if($pack_id == "1" || $pack_id == "2"){
				$list_sample['batch_no'] = !empty($this->input->post('batch_no')) ? trim($this->input->post('batch_no')) : null;
				$list_sample['packing_date'] = !empty($this->input->post('packing_date')) ? trim($this->input->post('packing_date')) : null;
				$list_sample['used_by_date'] = !empty($this->input->post('used_by_date')) ? trim($this->input->post('used_by_date')) : null;
				
				/*$packing_date = trim($this->input->post('packing_date'));
				$packing_date_array = explode('/',$packing_date);
				$packing_date = date('Y-m-d',mktime(0,0,0,$packing_date_array[1],$packing_date_array[0],$packing_date_array[2]));
					
				$used_by_date = trim($this->input->post('used_by_date'));
				$used_by_date_array = explode('/',$used_by_date);
				$used_by_date = date('Y-m-d',mktime(0,0,0,$used_by_date_array[1],$used_by_date_array[0],$used_by_date_array[2]));
					
				$list_sample['packing_date'] = $packing_date;
				$list_sample['used_by_date'] = $used_by_date;*/
			}else{
				$list_sample['batch_no'] = null;
				$list_sample['packing_date'] = null;
				$list_sample['used_by_date'] = null;
			}
			
			$list_sample['quantity'] = (!empty($this->input->post('quantity')))?trim($this->input->post('quantity')):'';
			$list_sample['quantity_type'] = (!empty($this->input->post('quantity_type')))?trim($this->input->post('quantity_type')):'';
			$list_sample['price'] = (!empty($this->input->post('price')))?trim($this->input->post('price')):'';
			$list_sample['price_type'] = (!empty($this->input->post('price_type')))?trim($this->input->post('price_type')):'';
			$list_sample['payment_to_fbo'] = (!empty($this->input->post('payment_fbo')))?trim($this->input->post('payment_fbo')):'';
			$list_sample['receipt_no'] = !empty($this->input->post('receipt_no')) ? trim($this->input->post('receipt_no')) : '';
			$list_sample['investigation_id'] = (!empty($this->input->post('investigation_type')))?trim($this->input->post('investigation_type')):'';
			$list_sample['preservation_added'] = !empty($this->input->post('preservation_details')) ? trim($this->input->post('preservation_details')) : '';
			$list_sample['disclosure'] = !empty($this->input->post('disclosure')) ? trim($this->input->post('disclosure')) : '';
			$list_sample['lab_id'] = !empty($this->input->post('lab_id'))?trim($this->input->post('lab_id')):'';

			$list_sample['is_demand_referral_lab'] = 0;
			$list_sample['referral_lab_id'] = 0;
			if(!empty($this->input->post('reflab'))){
				$list_sample['is_demand_referral_lab'] = trim($this->input->post('reflab'));
				$list_sample['referral_lab_id'] = !empty($this->input->post('referral_lab')) ? trim($this->input->post('referral_lab')) : 0;
			}
			
			$list_sample['is_demand_accredited_lab'] = 0;
				$list_sample['accredited_lab_id'] = 0;
			if(!empty($this->input->post('lab'))){
				$list_sample['is_demand_accredited_lab'] = trim($this->input->post('lab'));
				$list_sample['accredited_lab_id'] = !empty($this->input->post('accredited_lab')) ? trim($this->input->post('accredited_lab')) : 0;
			}

			$list_sample['witness_name'] = !empty($this->input->post('witness_name')) ? trim($this->input->post('witness_name')) : '';
			$list_sample['witness_address'] = !empty($this->input->post('witness_address')) ? trim($this->input->post('witness_address')) : '';
			$list_sample['is_seizzer'] = trim($this->input->post('seized'));
			if(!empty($this->input->post('seized'))){
				if(!empty($this->input->post('seized_quantity'))){
					$list_sample['seizzer_quantity'] = trim($this->input->post('seized_quantity'));
				}else{
					$list_sample['seizzer_quantity'] = 0;
				}
			
				if(!empty($this->input->post('seized_quantity_type'))){
					$list_sample['seizzer_quantity_type'] = trim($this->input->post('seized_quantity_type'));
				}
			
				if(!empty($this->input->post('seized_quantity_ml_ltr'))){
					$list_sample['seizzer_quantity_ml_ltr'] = trim($this->input->post('seized_quantity_ml_ltr'));
				}else{
					$list_sample['seizzer_quantity_ml_ltr'] = 0;
				}
			
				if(!empty($this->input->post('seized_quantity_type_ml_ltr'))){
					$list_sample['seizzer_quantity_type_ml_ltr'] = trim($this->input->post('seized_quantity_type_ml_ltr'));
				}
			
				if(!empty($this->input->post('seized_price'))){
					$list_sample['seizzer_price'] = trim($this->input->post('seized_price'));
				}
			}else{
				$list_sample['seizzer_quantity'] = 0;
				$list_sample['seizzer_quantity_type'] = null;
				$list_sample['seizzer_quantity_ml_ltr'] = 0;
				$list_sample['seizzer_quantity_type_ml_ltr'] = null;
				$list_sample['seizzer_price'] = 0;
			}
			$list_sample['sample_created_date'] = $sample_created_date;
			if(!empty($sample_created_time)){
				$list_sample['sample_created_time'] = $sample_created_time;
			}
			$list_sample['updated_by'] = $user_data->id;
			$list_sample['updated_date'] = $updated_date;
			$list_sample['circle_id'] = $this->input->post('fbo_circle_id');
			
			
			$where = "id = '".$sample_id."'";
			$this->m_sample->edit($where,$list_sample);
				
			$list_fbo['updated_by'] = $user_data->id;
			$list_fbo['updated_date'] = $updated_date;
			
			$where = "id = '".$fbo_id."'";
			$this->m_fbo->edit($where,$list_fbo);
			
			$where = "id = '".$sample_id."'";
			$sample_detail = $this->samplei_model->GetDataByID('',$sample_id);
			
			if(!empty($_FILES['image']['name'])){
				$sample_code = str_replace('/', '_', $sample_detail->code);
				$prefix = 'sample_'.$sample_code.'_'.time().rand(1000000,9999999);
				$result=$this->utils->image_upload($_FILES['image'],'files/sample_images/',$prefix);
				if($result['error']== 0){
					$file_name = $result['file_name'];
					$this->m_util->generate_thumb('files/sample_images/'.$file_name, 'files/sample_images/thumb/'.$file_name, 0, 0);
					$this->m_util->generate_thumb('files/sample_images/'.$file_name, 'files/sample_images/thumb/'.$file_name, 400, 0);
					$list_sample_media['sample_id'] = $sample_id;
					$list_sample_media['image'] = $file_name;
					$list_sample_media['thumb'] = $file_name;
					$list_sample_media['created_by'] = $user_data->id;
					$list_sample_media['created_date'] = $updated_date;
					$sample_media_id = $this->m_sample_media->insert($list_sample_media);
				}
			}
			$url= base_url().'backdata';
			redirect($url,"refresh");
		}else{
			$this->session->set_flashdata('mendatory_sample_fbo_data_edit', SAMPLE_FBO_REQUIRE_FIELDS_EDIT);
			$url= base_url().'backdata';
			redirect($url,"refresh");
		}
	}
	
	public function get_taluka_by_district()
	{
		if(!empty($this->input->post('district_id')))
		{
			$district_id = trim($this->input->post('district_id'));
			$where = "district_id = '".$district_id."' and status = 1 and del_flag = 0";
			$orderby = "name asc";
			$data['taluka_details'] = $this->m_taluka->getAllTalukas($where,$orderby);
			echo json_encode($data); exit;
		}
	}
	
	public function get_subcategories_by_category()
	{
		if(!empty($this->input->post('category_id')))
		{
			$category_id = trim($this->input->post('category_id'));
			$where = "parent_id = '".$category_id."' and status = 1 and del_flag = 0";
			$orderby = "name asc";
			$data['subcategory_details'] = $this->m_food_category->getAllFoodCategories($where,$orderby);
			echo json_encode($data); exit;
		}
	}
	
	public function get_sample_code_details()
	{
		$user_data = $this->session->userdata('user_data');
		$current_year = $this->utils->get_year();
		
		$where = "id = '".$user_data->circle_id."' and status = 1 and del_flag = 0";
		$circle_detail = $this->m_circle->getCircle($where);
		
		$where = "created_by = '".$user_data->id."'";
		$orderby = "id desc";
		$sample_detail = $this->m_sample->getFsoLastSample($where, $orderby);
		
		if(!empty($sample_detail))
		{
			$sample_no = $sample_detail->sample_no;
			if(!empty($sample_no))
			{
				$sample_no += 1;
			}
			else 
			{
				$sample_no = 1;
			}
		}
		else 
		{
			$sample_no = 1;
		}
		
		$list['sample_no'] = $sample_no;
		$list['sample_code'] = $circle_detail->circle_id.$circle_detail->short_name.$user_data->fso_id."/".$sample_no."/".$current_year;
		return $list;
	}
	
	public function sample_image_index($sample_unique_code){
		$data['sample_data'] = $this->samplei_model->GetDataByID($sample_unique_code);
		if(!@$data['sample_data']->Error_Message){
			$this->load->view('samples/upload_sample_images',$data);
		}else{
			show_404();
		}
	}
	
	public function upload_sample_images(){
		if(!empty($this->input->post('hdn_id'))){
			$user_data = $this->session->userdata('user_data');
			$created_date = $this->utils->get_date_time();
			$unique_code = trim($this->input->post('hdn_id')); 
			$sample_detail = $this->samplei_model->GetDataByID($unique_code);
			if(!empty($_FILES['image']['name'])){
				$sample_id = $sample_detail->id;
				$sample_code = str_replace('/', '_', $sample_detail->code);
				$prefix = 'sample_'.$sample_code.'_'.time().rand(1000000,9999999);
				$result=$this->utils->image_upload($_FILES['image'],'files/sample_images/',$prefix);
				if($result['error']== 0){
					$file_name = $result['file_name'];
					$this->m_util->generate_thumb('files/sample_images/'.$file_name, 'files/sample_images/'.$file_name, 0, 0);
					$this->m_util->generate_thumb('files/sample_images/'.$file_name, 'files/sample_images/thumb/'.$file_name, 400, 0);
					$list_sample_media['sample_id'] = $sample_id;
					$list_sample_media['image'] = $file_name;
					$list_sample_media['thumb'] = $file_name;
					$list_sample_media['created_by'] = $user_data->id;
					$list_sample_media['created_date'] = $created_date;
					$res = $this->samplei_media_model->Insert($list_sample_media);
					if(@$res->ID){
						$url= base_url().'samples/view/'.$sample_detail->unique_code;
						redirect($url,"refresh");
					}else{
						$this->session->set_flashdata('mendatory_sample_transoprt_lab_data', PLEASE_TRY_AGAIN);
						$url= base_url().'samples/sampleimageindex/'.$sample_detail->unique_code;
						redirect($url,"refresh");
					}
				}
			}else{
				$this->session->set_flashdata('mendatory_sample_transoprt_lab_data', FILE_NOT_FOUND);
				$url= base_url().'samples/sampleimageindex/'.$sample_detail->unique_code;
				redirect($url,"refresh");
			}
		}else{
			$this->session->set_flashdata('mendatory_sample_transoprt_lab_data', SAMPLE_TRANSPORT_LAB_REQUIRE_FIELDS);
			$url= base_url().'samples';
			redirect($url,"refresh");
		}
	}
	
	public function get_fso_by_circle()
	{
		if(!empty($this->input->post('circle_id')))
		{
			$circle_id = trim($this->input->post('circle_id'));
			$data['fso_details'] = $this->m_user->getFsoDetailFromCircleId($circle_id);
			echo json_encode($data); exit;
		}
	}
	
	

	/* 
	 * PAGINATION FUNCTIONALITY START
	 * Following function used for display circle wise fso sample details with pagination.
	 */
	function circlewise_listing(){
		$data = array();
		$data['user_data'] = $this->session->userdata('user_data');
		$data['search'] = $_POST['search'] = !empty($this->input->get('search')) ? trim($this->input->get('search')) : '';
		$data['year'] = $_POST['search_year'] = !empty($this->input->get('year')) ? trim($this->input->get('year')) : '';
		$circle_id = $data['circles'] = $_POST['circles'] = !empty($this->input->get('circles')) ? trim($this->input->get('circles')) : @$data['user_data']->circle_id;
		$fso_id = $data['fsoname'] = !empty($this->input->get('fsoname'))?trim($this->input->get('fsoname')):"all";
		$page = !empty($this->input->get('per_page')) ? trim($this->input->get('per_page')) : 1;
		
		$fso_ids = array();
		if(($data['user_data']->type == 2 && $data['user_data']->do_flag == 1) || $data['user_data']->type == 3 ){
			$wherecircle = "status = 1 AND del_flag = 0";
			$orderbycircle = "name asc";
			$wherein = explode(',',$data['user_data']->circle_id);
			$CircleCount  = count($wherein);
			if(!empty($wherein) && in_array(GANDHINAGAR_CIRCLE_ID, $wherein)){
				$wherein = NULL;
				$CircleCount = TOTAL_CIRCLE_COUNT;
			}
			if($CircleCount > 1){
				$data['circle_data'] = $this->m_circle->getAllCirclesWhereIdIn($wherecircle,$wherein,$orderbycircle);
				if(!empty($this->input->get('circles'))){
					$data['fso_data'] = $this->m_user->getFsoDetailFromCircleId($circle_id);
				}else{
					$data['fso_data'] = array();
				}
				
			}else{
				$data['circle_data'] = "";
				$data['fso_data'] = $this->m_user->getFsoDetailFromCircleId($data['user_data']->circle_id);
			}
		}else{
			if($circle_id == '') $circle_id = -1;
			if(!empty($this->input->get('circles')))
				$data['fso_data'] = $this->m_user->getFsoDetailFromCircleId($circle_id);
			$wherecircle = "status = 1 AND del_flag = 0";
			$orderbycircle = "district_name ASC";
			$data['circle_data'] = $this->m_circle->getAllCircles1($wherecircle,$orderbycircle);
		}
		if(!empty($this->input->get('circles'))){
			$data['selected_circle_id'] = $circle_id;
		}
		if(!empty($this->input->get('fsoname'))){
			$data['selected_fso_id'] = $fso_id;
		}
		if($fso_id == 'all'){
			$data['fso_id'] = 'all';
			$_POST['UserID'] = 'all';
		}else{
			$_POST['UserID'] = $data['fso_id'] = $fso_id;
		}
		$data['fso_count']= count(@$data['fso_data']);
		if($circle_id != "" && $circle_id != -1){
			$data['sample_data'] = $this->samplei_model->GetData(PAGE_SIZE,--$page);
		}
		if(@$data['sample_data'][0]->Error_Message && $circle_id != ""){
			$total_records =  0;
			$data['sample_data'] = array();
		}else{
			$total_records =  @$data['sample_data'][0]->rowcount;
		}
		$limit_per_page = PAGE_SIZE;
		$start_index = !empty($page) ? ($page) * $limit_per_page : 0;
		$search_parameter = "";
		$data['base_url_for_search'] = site_url().'sampleslisting/circlewiselisting?';

		if($total_records > 0){
			if($data['search'] != ""){
				$search_parameter .= "&search=".$data['search'];
			}
			if($data['year'] != ""){
				$search_parameter .= "&year=".$data['year'];
			}
			if($data['circles'] != ""){
				$search_parameter .= "&circles=".$data['circles'];
			}
			if($data['fsoname'] != ""){
				$search_parameter .= "&fsoname=".$data['fsoname'];
			}
			$data['base_url'] = site_url().'sampleslisting/circlewiselisting?'.@$search_parameter;
			$data['links'] = $this->m_util->GetPagination($data['base_url'],$total_records,$limit_per_page);
			$data['start_index'] = $start_index;
			$data['total_records'] = $total_records;
		}
		
		$data['current_year'] = $this->utils->get_year();
		$this->load->view('samples/listing',$data);
	}
	/*
	 * PAGINATION FUNCTIONALITY END
	 *
	 /
	 /* This Functions are Not Used Start */
	function listing()
	{
		if($this->input->get()){
			$user_data = $this->session->userdata('user_data');
			$page = !empty($this->input->get('per_page')) ? trim($this->input->get('per_page')) : 1;
			$data['search'] = $_POST['search'] = !empty($this->input->get('search')) ? trim($this->input->get('search')) : '';
			$data['year'] = $year = !empty($this->input->get('year')) ? trim($this->input->get('year')) : '';
			$data['fsoname'] = $fsoname = !empty($this->input->get('fsoname')) ? trim($this->input->get('fsoname')) : -1;
			$data['current_year'] = $this->utils->get_year();
			$_POST['is_send'] = 0;
			$_POST['search_year'] = $year;
		}
		$data['user_data'] = $this->session->userdata('user_data');
		if($data['user_data']->type == 3)
		{
			// $data['fso_data'] = $this->m_user->getFsoDetailFromCircleId($data['user_data']->circle_id);
			$data['fso_data'] = $this->m_user->getFsoDetailFromCircleId($data['user_data']->circle_id);
			$data['fso_count']= count($data['fso_data']);
			$fso_ids = array();
			if(!empty($data['fso_data']))
			{
				foreach($data['fso_data'] as $_fso_data)
				{
					array_push($fso_ids, $_fso_data->id);
				}
			}
			//$where = "status = 1 and del_flag = 0 and circle_id = '".$data['user_data']->circle_id."'";
			/*$where = "status = 1 and del_flag = 0";
			$wherein = $fso_ids;
			$orderby = "created_by asc";
			$data['sample_data'] = $this->m_sample->getAllSamplesDetailsWhereInFSOId($where,$wherein,$orderby);*/
		}
		elseif($data['user_data']->type == 2 && $data['user_data']->do_flag == 1)
		{
			// $data['fso_data'] = $this->m_user->getFsoDetailFromCircleId($data['user_data']->circle_id);
			$data['fso_data'] = $this->m_user->getFsoDetailFromCircleId($data['user_data']->circle_id);
			$data['fso_count']= count($data['fso_data']);
			$fso_ids = array();
			if(!empty($data['fso_data']))
			{
				foreach($data['fso_data'] as $_fso_data)
				{
					array_push($fso_ids, $_fso_data->id);
				}
			}
			
			/*$where = "status = 1 and del_flag = 0 and circle_id = '".$data['user_data']->circle_id."'";
			$where = "status = 1 and del_flag = 0";
			$wherein = $fso_ids;
			$orderby = "created_by asc";
			$data['sample_data'] = $this->m_sample->getAllSamplesDetailsWhereInFSOId($where,$wherein,$orderby);*/
			
			//print_r($data); exit;
		}
		else
		{
			/*$where = "status = 1 and del_flag = 0";
			$orderby = "name asc";
			$data['circle_data'] = $this->m_circle->getAllCircles($where,$orderby);*/
			
			$wherecircle = "status = 1 AND del_flag = 0";
			$orderbycircle = "district_name ASC";
			$data['circle_data'] = $this->m_circle->getAllCircles1($wherecircle,$orderbycircle);
			
			$data['fso_data'] = 0;
			$data['fso_count']= 0;
		}
		
		
		
		$this->load->view('samples/listing',$data);
	}
	public function circle_wise_fso_sample_details()
	{
		$data['user_data'] = $this->session->userdata('user_data');
		$circle_id = !empty($this->input->post('circles')) ? trim($this->input->post('circles')) : '';
		$fso_id = trim($this->input->post('fsoname'));
		$fso_ids = array();
		$wherein = '';
		if(($data['user_data']->type == 3) || ($data['user_data']->type == 2 && $data['user_data']->do_flag == 1)){
			$data['fso_data'] = $this->m_user->getFsoDetailFromCircleId($data['user_data']->circle_id);
		}else{
			$data['fso_data'] = $this->m_user->getFsoDetailFromCircleId($circle_id);
			$wherecircle = "status = 1 AND del_flag = 0";
			$orderbycircle = "district_name ASC";
			$data['circle_data'] = $this->m_circle->getAllCircles1($wherecircle,$orderbycircle);
			
			$data['selected_circle_id'] = $circle_id;
		}
		
		if($fso_id == 'all')
		{
			if(!empty($data['fso_data']))
			{
				foreach($data['fso_data'] as $fso_data)
				{
					array_push($fso_ids, $fso_data->id);
				}
			}
		}
		else 
		{
			array_push($fso_ids, $fso_id);
		}
		
		
		$data['fso_count']= count($data['fso_data']);
		$data['selected_fso_id'] = $fso_id;
		
		if($data['user_data']->type != 3)
		{
			$where = "status = 1 and del_flag = 0 and circle_id = '".$circle_id."'";
		}
		else 
		{
			$where = "status = 1 and del_flag = 0 and circle_id = '".$data['user_data']->circle_id."'";
		}
		if($fso_id != 'all')
		{
		    $wherein = '';
		    $where .= " and created_by = '".$fso_id."'";
		}
		$orderby = "sample_created_date asc";
		//	echo $where.'------------'.$orderby;
		//	print_r($wherein);
	
		$data['sample_data'] = $this->m_sample->getAllSamplesDetailsWhereInFSOId($where,$wherein,$orderby);
		$this->load->view('samples/listing',$data);
		
	}
	/* This Functions are Not Used end */	
}