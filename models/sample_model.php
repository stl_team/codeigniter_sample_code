<?php
class Sample_model extends CI_Model{
	function getAllSamples($where = NULL, $orderby = NULL,$limit = NULL, $start = NULL)
	{
		if(!empty($where))
			$this->db->where($where);
			
		if(!empty($orderby))
			$this->db->order_by($orderby);
		
		if(!empty($limit))
			$this->db->limit($limit, $start);
		return $this->db->get('sample')->result();
	}
	
	
	function getSample($where = NULL)
	{
		if(!empty($where))
			$this->db->where($where);
		return $this->db->get('sample')->first_row();
	}
	
	function getFsoLastSample($where = NULL, $orderby = NULL)
	{
		if(!empty($where))
			$this->db->where($where);
		
		if(!empty($orderby))
			$this->db->order_by($orderby);
		
		$this->db->limit(1);	
		return $this->db->get('sample')->first_row();
	}
	
	function getAllSamplesWhereIdIn($where = NULL, $wherein = NULL, $orderby = NULL)
	{
		if(!empty($where))
			$this->db->where($where);
	
		if(!empty($wherein))
			$this->db->where_in('id', $wherein);
					
		if(!empty($orderby))
			$this->db->order_by($orderby);
	
		return $this->db->get('sample')->result();
	}
	
	function getAllSamplesWhereIn($where = NULL, $wherein = NULL, $orderby = NULL)
	{
		if(!empty($where))
			$this->db->where($where);
		
		if(!empty($wherein))
			$this->db->where_in('circle_id', $wherein);
			
		if(!empty($orderby))
			$this->db->order_by($orderby);

		return $this->db->get('sample')->result();
	}
	
	function getAllSamplesWhereInUserId($where = NULL, $wherein = NULL, $whereor = NULL, $orderby = NULL)
	{
		if(!empty($where))
			$this->db->where($where);
	
		if(!empty($wherein))
			$this->db->where_in('created_by', $wherein);
		
		if(!empty($whereor))
			$this->db->or_where($whereor);
		
		if(!empty($orderby))
			$this->db->order_by($orderby);
		
		return $this->db->get('sample')->result();
	}
	
	function getAllSamplesDetailsWhereInFSOId($where = NULL, $wherein = NULL, $orderby = NULL, $whereor = NULL)
	{
		if(!empty($where))
			$this->db->where($where);
	
		if(!empty($wherein))
			$this->db->where_in('created_by', $wherein);
	
		if(!empty($orderby))
				$this->db->order_by($orderby);
	
		return $this->db->get('sample')->result();
	}

	/*
	 * FOR PAGINATION FUNCTIONALITY START
	 */
	function getAllSamplesDetailsWhereInFSOIdCount($where = NULL, $wherein = NULL, $orderby = NULL, $whereor = NULL)
	{
		$this->db->from('sample');
		if(!empty($where))
			$this->db->where($where);
	
		if(!empty($wherein))
			$this->db->where_in('created_by', $wherein);
	
		if(!empty($orderby))
			$this->db->order_by($orderby);
	
		return $this->db->count_all_results();
	}
	
	function getAllSamplesDetailsWhereInFSOIdFiltered($where = NULL, $wherein = NULL, $orderby = NULL, $limit = NULL, $start = NULL)
	{
		if(!empty($where))
			$this->db->where($where);
	
		if(!empty($wherein))
			$this->db->where_in('created_by', $wherein);
	
		if(!empty($orderby))
			$this->db->order_by($orderby);
	
		if(!empty($limit))
		{
			$this->db->limit($limit, $start);
		}
		
		return $this->db->get('sample')->result();
	}
	/*
	 * FOR PAGINATION FUNCTIONALITY END
	 */
	
	function getSampleForJctListing()
	{
		$sql="SELECT s.id, s.unique_code, s.sample_no, s.name, s.short_name, s.code, s.category_id, s.manufacturer_address, s.pack_id, s.quantity, s.price, s.payment_to_fbo, s.receipt_no, s.investigation_id, 
				s.preservation_added, s.disclosure, s.is_demand_referral_lab, s.referral_lab_id, s.is_demand_accredited_lab, s.accredited_lab_id, s.witness_name, s.witness_address, s.is_seizzer, 
				s.seizzer_quantity, s.seizzer_price, s.circle_id, c.name AS circle_name, s.lab_id, l.name AS lab_name, l.place AS lab_place, s.fa_id, s.is_app, s.edit_request_flag, s.is_print, 
				s.transport_type_id, s.slip_no, s.slip_image, s.slip_date, s.is_send, s.send_date, s.is_slip_print, s.lab_received_date, s.lab_box_type_id, s.lab_received_sample_no, 
				s.lab_suitable_sample_no, s.lab_unsuitable_sample_no, s.lab_is_form_vi_received, s.lab_is_sample_under_act, s.lab_second_form_vi_received_date, s.lab_is_rejected, s.lab_serial_no, 
				s.lab_sample_conditions, s.lab_sample_other_conditions, s.lab_acknowledgement_date, s.lab_is_acknoledgement, s.lab_acknowledgement_created_by, s.lab_seal_type_id, s.lab_container_type_id, 
				s.lab_seals_on_outer_cover, s.lab_seals_on_wrapper, s.lab_seals_on_container, s.lab_prepare_sample_details_date, s.lab_is_prepare_sample_details, s.lab_sample_details_created_by, 
				s.lab_report_no, s.lab_entry_no, s.lab_report_status, s.lab_analysis_start_date, s.lab_analysis_end_date, s.lab_regulation_no, s.lab_sample_description, s.lab_appearance, 
				s.lab_name_of_food, s.lab_label, s.lab_opinion, s.lab_is_prepare_form_b, s.lab_form_b_created_by, s.lab_prepare_form_b_date, s.lab_is_print_form_b, s.lab_report_remark, 
				s.is_lab_report_prepare_by_do, s.accredited_lab_report_date, s.accredited_lab_report_status, s.accredited_lab_report_remark, s.accredited_lab_report_image, 
				s.is_accredited_lab_report_prepare_by_do, s.referral_lab_report_date, s.referral_lab_report_status, s.referral_lab_report_remark, s.referral_lab_report_image, 
				s.is_referral_lab_report_prepare_by_do, s.adjudication_flag, s.courtcase_flag, s.is_allocated, s.status, s.del_flag, s.created_by, s.sample_created_date, s.created_date, 
				s.sync_date, s.updated_by, s.updated_date FROM sample AS s LEFT JOIN circles AS c ON s.circle_id = c.id LEFT JOIN lab AS l ON s.lab_id = l.id 
				WHERE s.status = 1 AND s.del_flag = 0 AND s.is_send = 1 AND s.fa_id = 0 AND l.lab_type = 0 ORDER BY s.send_date DESC";
		
		if(!empty($sql))
		{
			$query = $this->db->query($sql);
		
			if($query->num_rows() > 0)
			{
				return $query->result();
			}
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}
	}
	
	function getCircleForFAResult(){
		$sql = "SELECT 
					C.id,
					C.name
				FROM 
					circles C
				INNER JOIN sample S ON (S.circle_id = C.id AND S.status = 1 AND S.del_flag = 0)
				WHERE 
					S.lab_is_print_form_b = 1 AND S.is_send = 1
				GROUP BY C.id";
		$query = $this->db->query($sql);
		return $query->result();
	}
	function getFsoDetailFromCircleId($circle_id='')
	{
		if(!empty($circle_id))
		{
			
			$sql="SELECT s.created_by,u.id,u.unique_code,u.full_name,u.email,u.username,u.designation,u.type,u.phone,u.joining_date,u.image,u.circle_id,u.lab_id,u.is_hod,u.do_flag,u.user_id,u.signature,Fn_GetCircleList(s.circle_id) AS circle_name from sample as s,users as u where u.del_flag = 0 AND s.circle_id in(".$circle_id.") and s.created_by = u.id GROUP by s.created_by";
		}else
		{
			$sql="SELECT s.created_by,u.id,u.unique_code,u.full_name,u.email,u.username,u.designation,u.type,u.phone,u.joining_date,u.image,u.circle_id,u.lab_id,u.is_hod,u.do_flag,u.user_id,u.signature,Fn_GetCircleList(circle_id) AS circle_name from sample as s,users as u where u.del_flag = 0 AND s.created_by = u.id GROUP by s.created_by";
		}
		if(!empty($sql))
		{
			$query = $this->db->query($sql);
				
			if($query->num_rows() > 0)
			{
				$fso_data = $query->result();
				return $fso_data;
			}
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}
	}
	
	function getSampleForJctArchivesListing()
	{
		$sql="SELECT s.id, s.sample_no, s.name, s.short_name, s.code, s.category_id, s.manufacturer_address, s.pack_id, s.quantity, s.price, s.payment_to_fbo, s.receipt_no, s.investigation_id, 
				s.preservation_added, s.disclosure, s.is_demand_referral_lab, s.referral_lab_id, s.is_demand_accredited_lab, s.accredited_lab_id, s.witness_name, s.witness_address, s.is_seizzer, 
				s.seizzer_quantity, s.seizzer_price, s.circle_id, c.name AS circle_name, s.lab_id, l.name AS lab_name, l.place AS lab_place, s.fa_id, u.full_name AS fa_name, s.is_app, s.edit_request_flag, 
				s.is_print, s.transport_type_id, s.slip_no, s.slip_image, s.slip_date, s.is_send, s.send_date, s.is_slip_print, s.lab_received_date, s.lab_box_type_id, s.lab_received_sample_no, 
				s.lab_suitable_sample_no, s.lab_unsuitable_sample_no, s.lab_is_form_vi_received, s.lab_is_sample_under_act, s.lab_second_form_vi_received_date, s.lab_is_rejected, s.lab_serial_no, 
				s.lab_sample_conditions, s.lab_sample_other_conditions, s.lab_acknowledgement_date, s.lab_is_acknoledgement, s.lab_acknowledgement_created_by, s.lab_seal_type_id, s.lab_container_type_id, 
				s.lab_seals_on_outer_cover, s.lab_seals_on_wrapper, s.lab_seals_on_container, s.lab_prepare_sample_details_date, s.lab_is_prepare_sample_details, s.lab_sample_details_created_by, 
				s.lab_report_no, s.lab_entry_no, s.lab_report_status, s.lab_analysis_start_date, s.lab_analysis_end_date, s.lab_regulation_no, s.lab_sample_description, s.lab_appearance, 
				s.lab_name_of_food, s.lab_label, s.lab_opinion, s.lab_is_prepare_form_b, s.lab_form_b_created_by, s.lab_prepare_form_b_date, s.lab_is_print_form_b, s.lab_report_remark, 
				s.is_lab_report_prepare_by_do, s.accredited_lab_report_date, s.accredited_lab_report_status, s.accredited_lab_report_remark, s.accredited_lab_report_image, 
				s.is_accredited_lab_report_prepare_by_do, s.referral_lab_report_date, s.referral_lab_report_status, s.referral_lab_report_remark, s.referral_lab_report_image, 
				s.is_referral_lab_report_prepare_by_do, s.adjudication_flag, s.courtcase_flag, s.is_allocated, s.status, s.del_flag, s.created_by, s.sample_created_date, s.created_date, 
				s.sync_date, s.updated_by, s.updated_date FROM sample AS s LEFT JOIN circles AS c ON s.circle_id = c.id LEFT JOIN lab AS l ON s.lab_id = l.id LEFT JOIN users AS u ON s.fa_id = u.id 
				WHERE s.status = 1 AND s.del_flag = 0 AND s.is_send = 1 AND s.fa_id != 0 AND l.lab_type = 0 ORDER BY s.send_date DESC";
	
		if(!empty($sql))
		{
			$query = $this->db->query($sql);
	
			if($query->num_rows() > 0)
			{
				return $query->result();
			}
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}
	}
	
	function getPendingFormbSamplesForJctListing($lab_id)
	{
		$sql="SELECT s.id, s.sample_no, s.unique_code, s.name, s.short_name, s.code, s.category_id, s.manufacturer_address, s.pack_id, s.quantity, s.price, s.payment_to_fbo, s.receipt_no, s.investigation_id,
				s.preservation_added, s.disclosure, s.is_demand_referral_lab, s.referral_lab_id, s.is_demand_accredited_lab, s.accredited_lab_id, s.witness_name, s.witness_address, s.is_seizzer,
				s.seizzer_quantity, s.seizzer_price, s.circle_id, c.name AS circle_name, s.lab_id, l.name AS lab_name, l.place AS lab_place, s.fa_id, u.full_name AS fa_name, s.is_app, s.edit_request_flag,
				s.is_print, s.transport_type_id, s.slip_no, s.slip_image, s.slip_date, s.is_send, s.send_date, s.is_slip_print, s.lab_received_date, s.lab_box_type_id, s.lab_received_sample_no,
				s.lab_suitable_sample_no, s.lab_unsuitable_sample_no, s.lab_is_form_vi_received, s.lab_is_sample_under_act, s.lab_second_form_vi_received_date, s.lab_is_rejected, s.lab_serial_no,
				s.lab_sample_conditions, s.lab_sample_other_conditions, s.lab_acknowledgement_date, s.lab_is_acknoledgement, s.lab_acknowledgement_created_by, s.lab_seal_type_id, s.lab_container_type_id,
				s.lab_seals_on_outer_cover, s.lab_seals_on_wrapper, s.lab_seals_on_container, s.lab_prepare_sample_details_date, s.lab_is_prepare_sample_details, s.lab_sample_details_created_by,
				s.lab_report_no, s.lab_entry_no, s.lab_report_status, s.lab_analysis_start_date, s.lab_analysis_end_date, s.lab_regulation_no, s.lab_sample_description, s.lab_appearance,
				s.lab_name_of_food, s.lab_label, s.lab_opinion, s.lab_is_prepare_form_b, s.lab_form_b_created_by, s.lab_prepare_form_b_date, s.lab_is_print_form_b, s.lab_report_remark,
				s.is_lab_report_prepare_by_do, s.accredited_lab_report_date, s.accredited_lab_report_status, s.accredited_lab_report_remark, s.accredited_lab_report_image,
				s.is_accredited_lab_report_prepare_by_do, s.referral_lab_report_date, s.referral_lab_report_status, s.referral_lab_report_remark, s.referral_lab_report_image,
				s.is_referral_lab_report_prepare_by_do, s.adjudication_flag, s.courtcase_flag, s.is_allocated, s.status, s.del_flag, s.created_by, s.sample_created_date, s.created_date,
				s.sync_date, s.updated_by, s.updated_date FROM sample AS s LEFT JOIN circles AS c ON s.circle_id = c.id LEFT JOIN lab AS l ON s.lab_id = l.id LEFT JOIN users AS u ON s.fa_id = u.id
				WHERE s.status = 1 AND s.del_flag = 0 AND s.is_send = 1";
		if($lab_id != "all")
		{
			$sql.=" AND s.lab_id = '".$lab_id."'";
		}
		$sql.=" AND s.lab_is_prepare_form_b = 0 AND l.lab_type = 0 ORDER BY s.send_date DESC";
		
		if(!empty($sql))
		{
			$query = $this->db->query($sql);
	
			if($query->num_rows() > 0)
			{
				return $query->result();
			}
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}
	}
	
	function getSampleAcknowledgementForJctListing($lab_id)
	{
		$sql="SELECT s.id, s.sample_no, s.unique_code, s.name, s.short_name, s.code, s.category_id, s.manufacturer_address, s.pack_id, s.quantity, s.price, s.payment_to_fbo, s.receipt_no, s.investigation_id,
				s.preservation_added, s.disclosure, s.is_demand_referral_lab, s.referral_lab_id, s.is_demand_accredited_lab, s.accredited_lab_id, s.witness_name, s.witness_address, s.is_seizzer,
				s.seizzer_quantity, s.seizzer_price, s.circle_id, c.name AS circle_name, s.lab_id, l.name AS lab_name, l.place AS lab_place, s.fa_id, u.full_name AS fa_name, s.is_app, s.edit_request_flag,
				s.is_print, s.transport_type_id, s.slip_no, s.slip_image, s.slip_date, s.is_send, s.send_date, s.is_slip_print, s.lab_received_date, s.lab_box_type_id, s.lab_received_sample_no,
				s.lab_suitable_sample_no, s.lab_unsuitable_sample_no, s.lab_is_form_vi_received, s.lab_is_sample_under_act, s.lab_second_form_vi_received_date, s.lab_is_rejected, s.lab_serial_no,
				s.lab_sample_conditions, s.lab_sample_other_conditions, s.lab_acknowledgement_date, s.lab_is_acknoledgement, s.lab_acknowledgement_created_by, s.lab_seal_type_id, s.lab_container_type_id,
				s.lab_seals_on_outer_cover, s.lab_seals_on_wrapper, s.lab_seals_on_container, s.lab_prepare_sample_details_date, s.lab_is_prepare_sample_details, s.lab_sample_details_created_by,
				s.lab_report_no, s.lab_entry_no, s.lab_report_status, s.lab_analysis_start_date, s.lab_analysis_end_date, s.lab_regulation_no, s.lab_sample_description, s.lab_appearance,
				s.lab_name_of_food, s.lab_label, s.lab_opinion, s.lab_is_prepare_form_b, s.lab_form_b_created_by, s.lab_prepare_form_b_date, s.lab_is_print_form_b, s.lab_report_remark,
				s.is_lab_report_prepare_by_do, s.accredited_lab_report_date, s.accredited_lab_report_status, s.accredited_lab_report_remark, s.accredited_lab_report_image,
				s.is_accredited_lab_report_prepare_by_do, s.referral_lab_report_date, s.referral_lab_report_status, s.referral_lab_report_remark, s.referral_lab_report_image,
				s.is_referral_lab_report_prepare_by_do, s.adjudication_flag, s.courtcase_flag, s.is_allocated, s.status, s.del_flag, s.created_by, s.sample_created_date, s.created_date,
				s.sync_date, s.updated_by, s.updated_date FROM sample AS s LEFT JOIN circles AS c ON s.circle_id = c.id LEFT JOIN lab AS l ON s.lab_id = l.id LEFT JOIN users AS u ON s.fa_id = u.id
				WHERE s.status = 1 AND s.del_flag = 0 AND s.is_send = 1";
		if($lab_id != "all")
		{
			$sql.=" AND s.lab_id = '".$lab_id."'";
		}
		$sql.=" AND s.lab_is_acknoledgement = 1 ORDER BY s.send_date DESC";
	
		if(!empty($sql))
		{
			$query = $this->db->query($sql);
	
			if($query->num_rows() > 0)
			{
				return $query->result();
			}
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}
	}
	// lab_is_prepare_sample_details
	// lab_is_prepare_form_b
	// lab_is_acknoledgement
	
	function getSampleDetailsForJctListing($lab_id)
	{
		$sql="SELECT s.id,s.unique_code, s.sample_no, s.name, s.short_name, s.code, s.category_id, s.manufacturer_address, s.pack_id, s.quantity, s.price, s.payment_to_fbo, s.receipt_no, s.investigation_id,
				s.preservation_added, s.disclosure, s.is_demand_referral_lab, s.referral_lab_id, s.is_demand_accredited_lab, s.accredited_lab_id, s.witness_name, s.witness_address, s.is_seizzer,
				s.seizzer_quantity, s.seizzer_price, s.circle_id, c.name AS circle_name, s.lab_id, l.name AS lab_name, l.place AS lab_place, s.fa_id, u.full_name AS fa_name, s.is_app, s.edit_request_flag,
				s.is_print, s.transport_type_id, s.slip_no, s.slip_image, s.slip_date, s.is_send, s.send_date, s.is_slip_print, s.lab_received_date, s.lab_box_type_id, s.lab_received_sample_no,
				s.lab_suitable_sample_no, s.lab_unsuitable_sample_no, s.lab_is_form_vi_received, s.lab_is_sample_under_act, s.lab_second_form_vi_received_date, s.lab_is_rejected, s.lab_serial_no,
				s.lab_sample_conditions, s.lab_sample_other_conditions, s.lab_acknowledgement_date, s.lab_is_acknoledgement, s.lab_acknowledgement_created_by, s.lab_seal_type_id, s.lab_container_type_id,
				s.lab_seals_on_outer_cover, s.lab_seals_on_wrapper, s.lab_seals_on_container, s.lab_prepare_sample_details_date, s.lab_is_prepare_sample_details, s.lab_sample_details_created_by,
				s.lab_report_no, s.lab_entry_no, s.lab_report_status, s.lab_analysis_start_date, s.lab_analysis_end_date, s.lab_regulation_no, s.lab_sample_description, s.lab_appearance,
				s.lab_name_of_food, s.lab_label, s.lab_opinion, s.lab_is_prepare_form_b, s.lab_form_b_created_by, s.lab_prepare_form_b_date, s.lab_is_print_form_b, s.lab_report_remark,
				s.is_lab_report_prepare_by_do, s.accredited_lab_report_date, s.accredited_lab_report_status, s.accredited_lab_report_remark, s.accredited_lab_report_image,
				s.is_accredited_lab_report_prepare_by_do, s.referral_lab_report_date, s.referral_lab_report_status, s.referral_lab_report_remark, s.referral_lab_report_image,
				s.is_referral_lab_report_prepare_by_do, s.adjudication_flag, s.courtcase_flag, s.is_allocated, s.status, s.del_flag, s.created_by, s.sample_created_date, s.created_date,
				s.sync_date, s.updated_by, s.updated_date FROM sample AS s LEFT JOIN circles AS c ON s.circle_id = c.id LEFT JOIN lab AS l ON s.lab_id = l.id LEFT JOIN users AS u ON s.fa_id = u.id
				WHERE s.status = 1 AND s.del_flag = 0 AND s.is_send = 1";
		if($lab_id != "all")
		{
			$sql.=" AND s.lab_id = '".$lab_id."'";
		}
		$sql.=" AND s.lab_is_prepare_sample_details = 1 ORDER BY s.send_date DESC";
	
		if(!empty($sql))
		{
			$query = $this->db->query($sql);
	
			if($query->num_rows() > 0)
			{
				return $query->result();
			}
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}
	}
	
	function getFormBDetailsForJctListing($lab_id)
	{
		$sql="SELECT s.id, s.sample_no, s.unique_code, s.name, s.short_name, s.code, s.category_id, s.manufacturer_address, s.pack_id, s.quantity, s.price, s.payment_to_fbo, s.receipt_no, s.investigation_id,
				s.preservation_added, s.disclosure, s.is_demand_referral_lab, s.referral_lab_id, s.is_demand_accredited_lab, s.accredited_lab_id, s.witness_name, s.witness_address, s.is_seizzer,
				s.seizzer_quantity, s.seizzer_price, s.circle_id, c.name AS circle_name, s.lab_id, l.name AS lab_name, l.place AS lab_place, s.fa_id, u.full_name AS fa_name, s.is_app, s.edit_request_flag,
				s.is_print, s.transport_type_id, s.slip_no, s.slip_image, s.slip_date, s.is_send, s.send_date, s.is_slip_print, s.lab_received_date, s.lab_box_type_id, s.lab_received_sample_no,
				s.lab_suitable_sample_no, s.lab_unsuitable_sample_no, s.lab_is_form_vi_received, s.lab_is_sample_under_act, s.lab_second_form_vi_received_date, s.lab_is_rejected, s.lab_serial_no,
				s.lab_sample_conditions, s.lab_sample_other_conditions, s.lab_acknowledgement_date, s.lab_is_acknoledgement, s.lab_acknowledgement_created_by, s.lab_seal_type_id, s.lab_container_type_id,
				s.lab_seals_on_outer_cover, s.lab_seals_on_wrapper, s.lab_seals_on_container, s.lab_prepare_sample_details_date, s.lab_is_prepare_sample_details, s.lab_sample_details_created_by,
				s.lab_report_no, s.lab_entry_no, s.lab_report_status, s.lab_analysis_start_date, s.lab_analysis_end_date, s.lab_regulation_no, s.lab_sample_description, s.lab_appearance,
				s.lab_name_of_food, s.lab_label, s.lab_opinion, s.lab_is_prepare_form_b, s.lab_form_b_created_by, s.lab_prepare_form_b_date, s.lab_is_print_form_b, s.lab_report_remark,
				s.is_lab_report_prepare_by_do, s.accredited_lab_report_date, s.accredited_lab_report_status, s.accredited_lab_report_remark, s.accredited_lab_report_image,
				s.is_accredited_lab_report_prepare_by_do, s.referral_lab_report_date, s.referral_lab_report_status, s.referral_lab_report_remark, s.referral_lab_report_image,
				s.is_referral_lab_report_prepare_by_do, s.adjudication_flag, s.courtcase_flag, s.is_allocated, s.status, s.del_flag, s.created_by, s.sample_created_date, s.created_date,
				s.sync_date, s.updated_by, s.updated_date FROM sample AS s LEFT JOIN circles AS c ON s.circle_id = c.id LEFT JOIN lab AS l ON s.lab_id = l.id LEFT JOIN users AS u ON s.fa_id = u.id
				WHERE s.status = 1 AND s.del_flag = 0 AND s.is_send = 1";
		if($lab_id != "all")
		{
			$sql.=" AND s.lab_id = '".$lab_id."'";
		}
		$sql.=" AND s.lab_is_prepare_form_b = 1 ORDER BY s.send_date DESC";
	
		if(!empty($sql))
		{
			$query = $this->db->query($sql);
	
			if($query->num_rows() > 0)
			{
				return $query->result();
			}
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}
	}
	
	function getSampleCount($where = NULL)
	{
		$this->db->select('*');
		$this->db->from('sample');
		if(!empty($where))
			$this->db->where($where);
		
		$query = $this->db->get();
		$count = $query->num_rows();
		return $count;
	}
	
	function insert($list){
		$list = $this->m_util->FirstCapital($list,array());
		$this->db->set('unique_code','UUID()',false);
		$this->db->insert('sample', $list);
		$insert_id = $this->db->insert_id();
		return $insert_id;
	}
	
	function edit($where = NULL, $list)
	{
		$this->db->where($where);
		return $this->db->update('sample',$list);
	}
	
	function delete($where = NULL)
	{
		$this->db->where($where);
		return $this->db->delete('sample');
	}
	function generate_unique_code(){
		$res = $this->getAllSamples();
		foreach ($res as $key => $value) {
			$where = "id = ". $value->id;
			$this->db->set('unique_code','UUID()',false);
			$this->db->where($where);
			$this->db->update('sample',[]);
		}
	}
}